This is the source for the textbook *Theory of Computation*
by Jim Hefferon.

This is for an undergraduate computer science theory class.
It covers models of computation such as Turing machines,
unsolvability and the Halting problem, Finite State machines
and regular languages, and ends with computational complexity
and P vs NP. As to pedagogy, it is a mathematics text so things are
proved.  But what sets it apart is that the presentation is liberal,
highlighting the ideas in the work and making connections with things
from the background that students bring to the class.

The presentation is for US undergraduate sophmores.  It
expects that the reader has some background in programming and in
Discrete Math, about one course in each.  It is suitable for
a course's main text, for a supplemental text, or for self-study.

There are about eight hundred and fifty exercises, with fully-worked
answers.  There are PDF slides for classroom use.  And, there are video
lectures on YouTube that are particularly useful for those working on
their own.



## Get the book

You can download [the book](https://joshua.smcvt.edu/computation/book.pdf)
and [the answers](https://joshua.smcvt.edu/computation/answers.pdf).

You can also get a paper copy from Amazon.  See the homepage.



## Status

This is version 1.10. I have used it in class from 2018 through 2022 and a
couple of other people have used it in their class.  I have uploaded a
version to Amazon.



## Home page

More information is at 
[https://hefferon.net/computation](https://hefferon.net/computation).


## Bug reports

As with any large project, there will be errors.  I greatly appreciate
bug reports.  Reach me via the contact link on the home page.


## Source

This is made with LaTeX and related programs.  To compile the source files
you can see INSTALL.  But the chance is 99.999% that you just want the
supplied PDF; see the home page.


## History

2016-May-31  JH Not yet at version 0.01  
2018-Jan-17  JH Version 0.40. Using in class.  No Scheme lab.  
2018-Nov-29  JH Developing for second in-class use.  
2019-Dec-31  JH Developing for third in-class use.  
2020-Nov-11  JH Major revision in preparation for 1.0.  Added
  many exercises, provide full answers for each.  
2021-Nov-15  JH Writing Jupyter notebooks.  
2022-Jan-21  JH Text is getting a final in-class use before being
  set to version 1.0.  
2022-May-20  JH Run-up to 1.0: got asy to work on recent OS install.  
2022-Jul-19  JH Version 1.0. 
2023-Jul-24  JH Version 1.10, incorporating feedback from other teachers
  and providing a paper copy.