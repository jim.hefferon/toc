\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage{baskervillef}
\usepackage[varqu,varl,var0]{inconsolata}
\usepackage[scale=.95,type1]{cabin}
\usepackage[baskerville,vvarbb]{newtxmath}
\usepackage[cal=boondoxo]{mathalfa}


\usepackage{hyperref}
\hypersetup{
    bookmarks=false,         % show bookmarks bar?
    unicode=true,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=true,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={My title},    % title
    pdfauthor={Author},     % author
    pdfsubject={Subject},   % subject of the document
    pdfcreator={Creator},   % creator of the document
    pdfproducer={Producer}, % producer of the document
    pdfkeywords={keyword1, key2, key3}, % list of keywords
    pdfnewwindow=true,      % links in new PDF window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=blue,          % color of internal links (change box color with linkbordercolor)
    citecolor=blue,        % color of links to bibliography
    filecolor=blue,         % color of file links
    urlcolor=blue        % color of external links
}


\usepackage[medium]{titlesec}
\titleformat*{\section}{\bfseries}
\titlelabel{}

\usepackage{enumitem}
\setlist[itemize,1]{label=$\bullet$,labelwidth=1em,labelindent=0em,leftmargin=1em}
\setlist[itemize,2]{label=$\circ$,labelwidth=1em,labelindent=0em,leftmargin=1em}
\setlist[enumerate,2]{labelwidth=1em,labelindent=0em,leftmargin=1em}

\usepackage[dvipsnames]{xcolor}

\newcommand{\coverage}[1]{\hspace{0.667em}{#1}}
\newcommand{\fullcoverage}{\coverage{\color{Green}\textit{Full coverage}}}
\newcommand{\partialcoverage}{\coverage{\color{TealBlue}\textit{Partial coverage}}}
\newcommand{\nocoverage}{\coverage{\textit{Not covered}}}

\pagestyle{empty}
\begin{document}\thispagestyle{empty}
\begin{center}\Large\bfseries
  Coverage in \textit{Theory of Computation} \\
  of the ACM recommendations 
\end{center}

This lists the appropriate items from the Association for Computing Machinery's
\href{https://www.acm.org/binaries/content/assets/education/cs2013_web_final.pdf}{\textit{Curriculum Guidelines for Undergraduate Programs in Computer Science}}
(from 2013), and describes the coverage in \textit{Theory of Computation}.
In brief, the text covers all of the points, except that it
omits the Chomsky hierarchy in favor of additional Computational
Complexity.
(The text also covers some material not listed by the ACM.)



\section{AL/Basic Automata Computability and Complexity}

\begin{description}
\item[Topics:]
\begin{itemize}
% \item[Core-Tier1]
\item Finite-state machines  \fullcoverage
\item Regular expressions     \fullcoverage
\item The halting problem      \fullcoverage
% \item[Core-Tier2]
\item Context-free grammars  \partialcoverage
\item Introduction to the P and NP classes and the P vs.\ NP problem  \fullcoverage
\item Introduction to the NP-complete class and exemplary NP-complete problems (e.g., SAT, Knapsack)  \fullcoverage
\end{itemize}
\item[Learning Outcomes:] 
\begin{enumerate}
% \item[Core-Tier1]
\item Discuss the concept of finite state machines. [Familiarity]  \fullcoverage
\item Design a deterministic finite state machine to accept a specified language. [Usage]  \fullcoverage
\item Generate a regular expression to represent a specified language. [Usage]  \fullcoverage
\item Explain why the halting problem has no algorithmic solution. [Familiarity]  \fullcoverage
% \item[Core-Tier2]
\item Design a context-free grammar to represent a specified language. [Usage]  \partialcoverage
\item Define the classes P and NP. [Familiarity]  \fullcoverage
\item Explain the significance of NP-completeness. [Familiarity]  \fullcoverage
\end{enumerate}
\end{description}



\section{AL/Advanced Computational Complexity}
\begin{description}
\item[Topics:]
\begin{itemize}
\item  Review of the classes P and NP; introduce P-space and EXP  \fullcoverage
\item Polynomial hierarchy   \partialcoverage
\item NP-completeness (Cook's theorem)   \fullcoverage
\item Classic NP-complete problems  \fullcoverage
\item Reduction Techniques   \fullcoverage
\end{itemize}
\item[Learning Outcomes:] 
\begin{enumerate}
\item Define the classes P and NP. 
[Familiarity]  \fullcoverage
\item Define the P-space class and its relation to the EXP class. [Familiarity]
   \partialcoverage
\item Explain the significance of NP-completeness. [Familiarity]   \fullcoverage
\item Provide examples of classic NP-complete problems. [Familiarity]   \fullcoverage
\item Prove that a problem is NP-complete by reducing a classic known NP-complete problem to it. [Usage]   \fullcoverage
\end{enumerate}
\end{description}




\section{AL/Advanced Automata Theory and Computability}
\begin{description}
\item[Topics:]
\begin{itemize}
\item Sets and languages
  \begin{itemize}
    \item Regular languages   \fullcoverage
    \item Review of deterministic finite automata (DFAs)   \fullcoverage
    \item Nondeterministic finite automata (NFAs)    \fullcoverage
    \item Equivalence of DFAs and NFAs    \fullcoverage
    \item Review of regular expressions; their equivalence to finite automata  \fullcoverage
    \item Closure properties   \fullcoverage
    \item Proving languages non-regular, via the pumping lemma or alternative means   \fullcoverage
  \end{itemize}
\item Context-free languages
  \begin{itemize}
    \item Push-down automata (PDAs)   \partialcoverage
    \item Relationship of PDAs and context-free grammars  \partialcoverage
    \item Properties of context-free languages   \partialcoverage
  \end{itemize}
\item Turing machines, or an equivalent formal model of universal computation  \fullcoverage
\item Nondeterministic Turing machines   \fullcoverage
\item Chomsky hierarchy   \nocoverage
\item The Church-Turing thesis   \fullcoverage
\item Computability      \fullcoverage
\item Rice's Theorem      \fullcoverage
\item Examples of uncomputable functions    \fullcoverage
\item Implications of uncomputability    \fullcoverage
\end{itemize}
\item[Learning Outcomes:]
\begin{enumerate}
\item Determine a language's place in the Chomsky hierarchy (regular, context-free, recursively enumerable).   \nocoverage
[Assessment]
\item Convert among equivalently powerful notations for a language, including among DFAs, NFAs, and regular
expressions, and between PDAs and CFGs. [Usage]  \partialcoverage
\item Explain the Church-Turing thesis and its significance. [Familiarity]  \fullcoverage
\item Explain Rice's Theorem and its significance. [Familiarity]   \fullcoverage
\item Provide examples of uncomputable functions. [Familiarity]   \fullcoverage
\item Prove that a problem is uncomputable by reducing a classic known uncomputable problem to it. [Usage]   \fullcoverage
\end{enumerate}
\end{description}

\end{document}
