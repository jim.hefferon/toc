\section{Functions}\label{sec:Functions}\index{function|(}
\thispagestyle{bookemptypage}

A function is an input-output relationship:~each
input is associated with a unique output.
An example is the association of each input natural number with 
the output number that is its square.
Another is the association of each string of characters
with the length of that string.
A third is the association of each polynomial 
$a_nx^n+\cdots+a_1x+a_0$ with a Boolean value \true{} or~\false,
depending on whether~$1$ is a root of that polynomial.

An important point is that, contrary to what is said in most
introductions, a function isn't a `rule'.
The function that associates a year with that year's winners 
of the US baseball World Series
isn't given by any rule simpler than an
exhaustive listing of all cases.  
Nor is the kind of association that a database might have, such as
linking the government ID of US citizens to their income in the most
recent tax year.
True, in science
many functions are described by a formula, such as $E(m)=mc^2\!$,
and as well many functions are computed by a program.
But what makes something a function is
that for each input 
there is exactly one associated output.
If we can go from an input to the associated output with a calculation 
then that's great
but even if we can't, it is still a function.

For a precise definition fix two sets, 
a \definend{domain}~$D$\index{domain}\index{function!domain} 
and a \definend{codomain}~$C$.\index{codomain}\index{function!codomain}
A \definend{function}\index{function}\index{function!definition} 
or \definend{map},\index{map|see {function}}
\definend{$\map{f}{D}{C}$}, is a set of pairs
$(x,y)\in D\times C$, subject to the restriction of being 
\definend{well-defined},\index{well-defined}\index{function!well-defined}
that every~$x\in D$ appears as the first entry 
in one and only one pair (more on well-definedness is below).
We write $f(x)=y$ 
or $x\mapsto y$ and say that `$x$ maps to~$y$'. 
Note the difference between the arrow symbols used in $\map{f}{D}{C}$ and 
$x\mapsto y$. 
We say that~$x$ is an \definend{input}\index{input, to a function}\index{function!index} 
or \definend{argument}\index{argument, to a function}\index{function!argument} 
to the function,
and that~$y$ is an 
\definend{output}\index{output, from a function}\index{function!output} 
or \definend{value} of the 
function.\index{value, of a function}\index{function!value}

Some functions take more than one input, such as
$\operatorname{dist}(x,y)=\smash{\sqrt{x^2+y^2}}$.
We say that this function is $2$-ary, while some other
functions are $3$-ary, etc.
The number of inputs is the function's \definend{arity}.
If the function takes only one input but that input is a tuple 
then we often drop the parentheses, so  we write $f(3,5)$
instead of $f((3,5))$.



\subsection*{Pictures}
We often illustrate functions using the familiar $xy$~axes.
% ;
% here are the graphs of $f(x)=x^3$ and $f(x)=\floor{x}$.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions000.pdf}}
  \hspace{2em plus 0.5fil}
  \vcenteredhbox{\includegraphics{appendix/asy/functions001.pdf}}
\end{center}
We also illustrate functions with a bean diagram, 
which separates the domain and the codomain sets.
Below on the left is the action of the exclusive~or operator
while on the right is a variant of the bean diagram, showing the
absolute value function mapping integers to integers.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions002.pdf}}
  \hspace{2em plus 0.5fil}
  \vcenteredhbox{\includegraphics{appendix/asy/functions011.pdf}}
\end{center}


\subsection*{Codomain and range}
Where  $S\subseteq D$ is a subset of the domain, its 
\definend{image}\index{image under a function}\index{function!image under} 
is the set $f(S)=\set{f(s)\suchthat s\in S}$.
Thus, under the squaring function the image of $S=\set{0,1,2}$ is 
$f(S)=\set{0,1,4}$.
Under the floor function $\map{g}{\R}{\R}$ given by $g(x)=\floor{x}$,
the image of the positive reals is
the set of natural numbers.

The image of the entire domain~$D$ is the function's
\definend{range},\index{range of a function}\index{function!range}
$\operatorname{ran}(f)=f(D)=\set{f(d)\suchthat d\in D}$.
For instance, the range of the function $\map{f}{\Z}{\Z}$ 
given by $x\mapsto 2x$
is the set of even integers.

Note the difference between a function's range and its 
codomain;~the\index{codomain versus range}
codomain is a convenient superset of the range.
An example is that 
for the function given by
$f(x)=\smash{\sqrt{2x^4+2x^2+15}}$, we will usually be content to note 
that the polynomial is always nonnegative and so the output is real, 
writing $\map{f}{\R}{\R}$ where the second $\R$ is the codomain,
rather than troubling to find its exact range. 



\subsection*{Domain}\index{domain}
Sometimes a function's domain requires attention.
Examples
are that $f(x)=1/x$ 
is undefined at $x=0$ and that the function defined by the infinite series
$g(r)=1+r+r^2+\cdots$ diverges when $r$ is outside the interval
$\open{-1}{1}$.
Formally, when we define the function 
we must specify the domain to eliminate such problems,
for instance by defining the domain of~$f$ as $\R-\set{0}$.
However, we are usually casual about this, expecting that a reader will
understand to omit inputs that are an issue.

We sometimes have a function $\map{f}{D}{C}$
and want to reduce the domain to some subset $S\subseteq D$.
The 
\definend{restriction}~$\restrictionmap{f}{S}$\index{restriction of a function}\index{function!restriction} 
is the function with
domain~$S$ and codomain~$C$ defined by $\restrictionmap{f}{S}(x)=f(x)$.   


\subsection*{In the Theory of Computation}\index{domain!in the Theory of Computation}
we sometimes uses these terms in a way that is at odds with the 
definitions above.
When we study a function, often we will
fix a convenient set of inputs~$D$, such as the set of all 
strings $\kleenestar{\Sigma}$ or all natural numbers~$\N$,
and refer to $D$ as the function's domain, although that function may be 
undefined on some of $D$'s elements.
In this case 
we say that~$f$ is a 
\definend{partial function}.\index{partial function}\index{function!partial}\label{def:PartialFunction} 
If $f$ is defined on all inputs then 
it is a \definend{total function}.\index{total function}\index{function!total}
Strictly speaking, `partial' is redundant
since any function is partial, including a total function,
but often `partial' is used to suggest the possibility 
that $f$ may be not defined on some $d\in D$.


\subsection*{Well-defined}\index{well-defined}\index{function!well-defined}
The definition of a function contains  
the condition that for 
each domain element there cannot be two associated codomain elements.
We say that functions are \definend{well-defined}.

When we are considering a relationship 
between $x$'s and $y$'s and
asking if it is a function, typically it is well-definedness that is at 
issue.\footnote{%
  Sometimes people say that they are, ``checking that the function is 
  well-defined.''
  In a strict sense this is confused, because   
  if it is a function then it is by definition well-defined.
  However, natural language is funny this way\Dash while all tigers have 
  stripes, we may well sometimes say ``striped tiger.''}
For instance, consider the set of ordered pairs
$(x,y)$ where $y^2=x$.
If~$x=9$ then both $y=3$ and~$y=-3$ are related to~$x$
so this is not a functional relationship\Dash it is not well-defined\Dash
because $x=9$ does not have only one associated~$y$.
% Similarly, if we try to go from an input cosine to an output angle,
% so that we relate each input $x\in\closed{-1}{1}$
% with output~$y$ so that~$\cos(y)=x$ then
% this is not a function, since there are many such angles.
Another example is that 
when setting up a company's email we may decide to use each person's
first initial and last name, but  
there could be more than one, say, \str{lwainwright},
making the relationship
$\text{email}\mapsto\text{person}$ be not 
well-defined.

For a function that is suitable for graphing on $xy$~axes, % $\map{f}{\R}{\R}$, 
visual proof of well-definedness is that for any $x$ in the domain, 
the vertical line
through~$x$ intercepts $f$'s graph in exactly one point.


\subsection*{One-to-one and onto}
The definition of function has an asymmetry:~among the ordered pairs
$(x,y)$, it 
requires that each domain element~$x$ be in one pair and only one pair,
but it does not require the same of the codomain elements.

A function is 
\definend{one-to-one}\index{one-to-one function}\index{function!one-to-one} 
(or \definend{$1$-$1$} or
an \definend{injection})\index{injection}\index{function!injection} 
if each codomain element~$y$ is in at most one pair.
The function below is one-to-one because for every element~$y$ in the 
codomain, the bean on the right, 
there is at most one arrow ending at~$y$.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions004.pdf}}
\end{center}
An important example is that when the 
domain is a subset of the codomain, $D\subseteq C$,
then the 
\definend{inclusion}\index{inclusion function@inclusion function $\iota$}\index{i@$\iota$|see{inclusion function}} function $\map{\iota}{D}{C}$ 
is defined by $\iota(x)=x$.
The most common way to prove that a function~$f$ is one-to-one is to 
assume that $f(x_0)=f(x_1)$ and then argue that therefore $x_0=x_1$.
If a function is suitable for graphing on $xy$~axes then 
visual proof that it is one-to-one 
is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in at most one point.

% Note also that the restriction of a one-to-one function is one-to-one.

A function is 
\definend{onto}\index{onto function}\index{function!onto} 
(or a 
\definend{surjection})\index{surjection}\index{function!surjection} 
if each codomain element~$y$ is in at least one pair.
Thus, a function is onto if its codomain equals its range.
The function below is onto because every element in the codomain bean
has at least one arrow ending at it.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions003.pdf}}
\end{center}
The most common way to verify that a function is onto is to 
start with a generic (that is, arbitrary) codomain element~$y$ and then 
exhibit a domain element~$x$ that maps to it.
If a function is suitable for graphing on $xy$~axes then 
visual proof that it is onto
is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in at least one point.

As the above pictures suggest, where
the domain and codomain are finite, 
when there is a function $\map{f}{D}{C}$ then 
we can conclude that the number of elements in the domain is less than or equal 
to the number in the codomain.
Further, if the function is onto then
the number of elements in the domain equals the number in the codomain
if and only if 
the function is one-to-one.


\subsection*{Correspondence}
A function is a 
\definend{correspondence}\index{correspondence}\index{function!correspondence} 
(or \definend{bijection})\index{bijection} 
if it is both one-to-one and onto.
The picture on the left shows a correspondence between two finite sets, 
both with four elements,
and the picture on the right shows a correspondence between the natural numbers
and the primes.
\begin{center}
  \vcenteredhbox{\includegraphics{appendix/asy/functions006.pdf}}
  \hspace{1em plus 1fil}
  \vcenteredhbox{\includegraphics{appendix/asy/functions007.pdf}}
\end{center}
The most common way to verify that a function is a correspondence is to 
separately verify that it is one-to-one and that it is onto.
Where the function is $\map{f}{\R}{\R}$, so it can be graphed on $xy$~axes,  
visual proof that it is a correspondence 
is that for any $y$ in the codomain, the horizontal line
at~$y$ intercepts the graph in exactly one point.

As the picture above on the left suggests, where
the domain and codomain are finite, if a function is a correspondence
then its domain has the same number of elements as its codomain.


\subsection*{Composition and inverse}
If $\map{f}{D}{C}$ and $\map{g}{C}{B}$ then their 
\definend{composition $\map{\composed{g}{f}}{D}{B}$}\index{composition}\index{function!composition} 
is defined by $\composed{g}{f}\,(d)= g(\,f(d)\,)$.
For instance, the real functions $f(x)=x^2$ and $g(x)=\sin(x)$
combine to give $\composed{g}{f}=\sin (x^2)$.

Composition does not commute.
Using the functions from the prior paragraph,
$\composed{f}{g}=\sin(x^2)$ and 
$\composed{f}{g}=(\sin x)^2$ are different; 
for instance they are unequal when $x=\pi$.
Composition can fail to commute more dramatically:~if
$\map{f}{\R^2}{\R}$ is given by $f(x_0,x_1)=x_0$, and 
$\map{g}{\R}{\R}$ is $g(x)=x$, then
$\composed{g}{f}(x_0,x_1)=x_0$ is perfectly sensible but
composition in the other order is not even defined.

The composition of one-to-one functions is one-to-one, and the 
composition of onto functions is onto; see the exercises.
It follows then that the composition of correspondences is a 
correspondence.

An \definend{identity function}\index{identity function}\index{function!identity} 
$\map{\identity}{D}{D}$ is given by
$\identity(d)=d$ for all $d\in D$.
It acts as the identity element in function composition,
so that if $\map{f}{D}{C}$ then $\composed{f}{\identity}=f$
and if $\map{g}{C}{D}$ then $\composed{\identity}{g}=g$.
As well, if $\map{h}{D}{D}$ then 
$\composed{h}{\identity}=\composed{\identity}{h}=h$.

Given $\map{f}{D}{C}$,
if $\composed{g}{f}$ is the identity function then 
$g$~is a \definend{left inverse}\index{left inverse}\index{function!left inverse}\index{inverse of a function!left} 
function of~$f\!$, or what is the
same thing, $f$ is a 
\definend{right inverse}\index{right inverse}\index{function!right inverse}\index{inverse of a function!right}  
of~$g$.
If $g$ is both a left and right inverse of~$f$ then
we simply say that it is an 
\definend{inverse}\index{inverse of a function}\index{inverse of a function!two-sided}\index{function!inverse}\index{two-sided inverse} 
(or \definend{two-sided inverse}) of~$f$ and denoted it 
as~\definend{$f^{-1}\!$}. 
If a function has an inverse then that inverse is unique.
A function has a two-sided inverse if and only if it is a 
correspondence.


\begin{exercises}
\begin{exercise} 
  Let $\map{f,g}{\R}{\R}$ be $f(x)=3x+1$ and 
  $g(x)=x^2+1$.
\begin{exparts*}
\item 
  Show that $f$ is one-to-one and onto.
\item
  Show that $g$ is not one-to-one and not onto.
\end{exparts*}
\begin{answer}\verified
\begin{exparts}
\item 
  For one-to-one, suppose that $f(x_0)=f(x_1)$ for $x_0,x_1\in\R$.
  Then $3x_0+1=3x_1+1$.
  Subtract the~$1$'s and divide by~$3$ to conclude that $x_0=x_1$.   

  For onto, fix a member of the codomain~$c\in\R$.
  Observe that $d=(c-1)/3$ is a member of the domain and that 
  $f(d)=3\cdot ((c-1)/3)+1=(c-1)+1=c$.
  Thus $f$ is onto.  
\item
  The function~$g$ is not one-to-one because $g(2)=g(-2)$.
  It is not onto because no element of the domain~$\R$ is mapped by~$g$
  to the element $0$ of the codomain.     
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} Show each of these.
\begin{exparts}
\item
Let $\map{g}{\R^3}{\R^2}$
be the projection map $(x,y,z)\mapsto(x,y)$
and let $\map{f}{\R^2}{\R^3}$ be
$(x,y)\mapsto(x,y,0)$.
Then $g$ is a left inverse of~$f$ but not a right inverse.
\item
  The function $\map{f}{\Z}{\Z}$ given by $f(n)=n^2$ has no left inverse.
\item
Where $D=\set{0,1,2,3}$ and $C=\set{10,11}$,
the function $\map{f}{D}{C}$ given by
$0\mapsto 10$, $1\mapsto 11$, $2\mapsto 10$, $3\mapsto 11$
has more than one right inverse.  
\end{exparts}
\begin{answer}\verified
\begin{exparts}
\item
It is a left inverse because $\composed{g}{f}$ has the action
$(x,y)\mapsto(x,y,0)\mapsto(x,y)$ for all $x$ and~$y$.
It is not a right inverse because there is an 
input on which the composition $\composed{f}{g}$ is not the identity, namely
the action is 
$(1,2,3)\mapsto(1,2)\mapsto(1,2,0)$.
\item 
Observe that $f(2)=f(-2)=4$.
Any left inverse would have to map $4$ to~$2$ and also to map
$4$ to~$-2$.
That would be not well-defined, so no function is the left inverse.
\item
One right inverse is $\map{g_0}{C}{D}$ given by $10\mapsto 0$, 
$11\mapsto 1$.
It is a right inverse because
$\composed{f}{g_0}\,(10)=f(g_0(10))=f(0)=10$
and
$\composed{f}{g_0}\,(11)=f(g_0(11))=f(1)=11$.
A second right inverse is  
$\map{g_1}{C}{D}$ given by $10\mapsto 2$, 
$11\mapsto 3$.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} 
\begin{exparts}
\item 
  Where $\map{f}{\Z}{\Z}$ is~$f(a)=a+3$ and 
  $\map{g}{\Z}{\Z}$ is $g(a)=a-3$,
  show that $g$ is inverse to~$f$.
\item
  Where $\map{h}{\Z}{\Z}$ is the function that returns
  $n+1$ if $n$ is even and returns $n-1$ if $n$ is odd,
  find a function inverse to~$h$.
\item
  If $\map{s}{\R^+}{\R^+}$ is~$s(x)=x^2$,
  find its inverse.
\end{exparts}
\begin{answer}\verified
\begin{exparts}
\item
  First, the domain of each equals the codomain of the other
  so both compositions $\composed{f}{g}$ and $\composed{g}{f}$ are defined.
  Next, $\composed{g}{f}\,(a)=g(f(a))=g(a+3)=(a+3)-3=a$
  is the identity map.
  Similarly, $\composed{f}{g}\,(a)=(a-3)+3$ is also the identity map.
  Thus they are inverse to each other.  
\item
  The inverse of~$h$ is itself.
  The domain and codomain are equal, as required. 
  In the case that~$n$ is odd, 
  $\composed{h}{h}\,(n)=h(n-1)=(n-1)+1=n$ (the second
  equality holds because $n-1$ is even in this case).
  Similarly if~$n$ is even then $\composed{h}{h}\,(n)=(n+1)-1=n$.  
\item
  The inverse of~$s$ is the map~$\map{r}{\R^+}{\R^+}$ given my 
  $r(x)=\sqrt{x}$ (that is, the positive root).
  The domain of each is the codomain of the other,
  and each of $\composed{s}{r}$ and~$\composed{r}{s}$ is the identity function.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
Fix $D=\set{0,1,2}$ and~$C=\set{10,11,12}$.
Let $\map{f,g}{D}{C}$ be
$f(0)=10$, $f(1)=11$, $f(2)=12$, and
$g(0)=10$, $g(1)=10$, $g(2)=12$.
Then:
\begin{exparts*}
\item verify that $f$ is a correspondence
\item construct an inverse for~$f$\!
\item verify that~$g$ is not a correspondence
\item show that~$g$ has no inverse. 
\end{exparts*}
\begin{answer}\verified
This is a bean diagram of the function~$f$
\begin{center}
  \includegraphics{appendix/asy/functions008.pdf}
\end{center}
and this is a diagram of~$g$.
\begin{center}
  \includegraphics{appendix/asy/functions009.pdf}
\end{center}
\begin{exparts}
\item By inspection $f$ is both one-to-one and onto.
\item Reverse the association made by $f$ so that $f^{-1}(10)=0$,
  $f^{-1}(11)=1$, and~$f^{-1}(12)=2$.
\item The map~$g$ is not one-to-one since $g(0)=g(1)$.
  (In addition, $g$ is not onto since no input is sent to $11\in C$.) 
\item If a $h$ were the inverse of~$g$
  then we would have both $h(10)=0$ and~$h(10)=1$, 
  which would violate the definition of a function.   
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} \label{InteractionOneToONeOntoWithComposition}
\begin{exparts}
\item
  Prove that a composition of one-to-one functions is one-to-one.
\item
  Prove that a composition of onto functions is onto.
  With the prior item, this gives that a
  composition of correspondences is a correspondence.
\item
  Prove that if $\composed{g}{f}$ is one-to-one then $f$ is one-to-one.
\item
  Prove that if $\composed{g}{f}$ is onto then $g$ is onto.
\item
  If $\composed{g}{f}$ is onto, must $f$ be onto?
  If it is one-to-one, must $g$ be one-to-one?
\end{exparts}
\begin{answer}\verified
\begin{exparts}
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be one-to-one.
  Suppose that $\composed{g}{f}\,(x_0)=\composed{g}{f}\,(x_1)$ for
  some $x_0,x_1\in D$.
  Then $g(f(x_0))=g(f(x_1))$.
  The function~$g$ is one-to-one and so, since the two values $g(f(x_0))$ 
  and~$g(f(x_1))$ are equal, the arguments $f(x_0)$ and~$f(x_1)$ are
  also equal.
  The function~$f$ is one-to-one and so $x_0=x_1$.
  Because $\composed{g}{f}\,(x_0)=\composed{g}{f}\,(x_1)$ implies that
  $x_0=x_1$, the composition is one-to-one.  
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be onto.
  If $B$ is the empty set then by the definition of function, 
  $C$ is empty also and, again by the definition of function, 
  in turn $D=\emptyset$, and so 
  the composition is onto, vacuously.
  Otherwise, fix an element of the codomain of the composition,~$b\in B$.
  The function~$g$ is onto so there is a $c\in C$ such that $g(c)=b$.
  The function~$f$ is onto so there is a $d\in D$ such that $f(d)=c$.
  Then $\composed{g}{f}(d)=b$, and so the composition is onto.
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be such that 
  the composition $\composed{g}{f}$ is one-to-one.
  Suppose that~$f$ is not one-to-one.
  Then there are $d_0,d_1\in D$ with $d_0\neq d_1$ such that $f(d_0)=f(d_1)$.
  But this implies that $g(f(d_0))=g(f(d_1))$, contradicting that 
  the composition is one-to-one.  
\item
  Let $\map{f}{D}{C}$ and~$\map{g}{C}{B}$ be such that 
  the composition $\composed{g}{f}$ is onto.
  Suppose for a contradiction that~$g$ is not onto.
  Then for some~$b\in B$ there is no~$c\in C$ with~$g(c)=b$.
  But this means that there is no~$d\in D$ such that 
  $b=g(f(d))$, contradicting that the composition is onto.
  So $g$ must be onto.  
\item
  The answer to both questions is ``no.''
  
  Let $D=\set{0,1}$, $C=\set{10,11,12}$, and $B=\set{20, 21}$.
  Let $\map{f}{D}{C}$ and $\map{g}{C}{B}$ be as shown below.
  Then $\composed{g}{f}$ is onto because $\composed{g}{f}\,(0)=20$ and
  $\composed{g}{f}\,(1)=21$.
  But $f$ is not onto because no element of its domain~$D$ is mapped to the
  element $10$ of its codomain~$C$. 
  \begin{center}
    \includegraphics{appendix/asy/functions010.pdf}
  \end{center}
  The same function is an example of a composition $\composed{g}{f}$ 
  that is one-to-one but where
  the function performed second,~$g$, is not one-to-one.
  In particular, $\composed{g}{f}$ is one-to-one because only one element
  of its domain~$D$ is mapped to it's codomain element $20$. 
  Similarly only one element of its domain is mapped to~$21$. 
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise} \label{PropertiesOfInverses}
Prove each.
\begin{exparts}
\item
  A function~$f$ has an inverse if and only if
  $f$ is a correspondence.
\item 
  If a function has an inverse then that inverse
  is unique.
\item
  The inverse of a correspondence is a correspondence.  
\item
  If $f$ and $g$ are each invertible then so is 
  $\composed{g}{f}$, and $(\composed{g}{f})^{-1}=\composed{f^{-1}}{g^{-1}}$.
\end{exparts}
\begin{answer}\verified
\begin{exparts}
\item
  We first prove that if the function $\map{f}{D}{C}$ has an
  inverse $\map{f^{-1}}{C}{D}$ then it must be  a correspondence.
  To verify that $f$ is one-to-one,
  suppose that~$d_0,d_1\in D$ are 
  such that $f(d_0)=f(d_1)=c$ for some $c\in C$.
  Consider~$f^{-1}(c)$.
  Because $\composed{f^{-1}}{f}$ is the identity map on~$D$ we have 
  both that $f^{-1}(c)=f^{-1}(f(d_0))=d_0$ 
  and that $f^{-1}(c)=f^{-1}(f(d_1))=d_1$.
  Thus $d_0=d_1$ and so $f$ is one-to-one.
   
  Next we verify that~$f$ is onto.
  Suppose otherwise, 
  so that there is a~$c\in C$ that is not associated with any~$d\in D$.
  But that gives the contradiction that the map~$\composed{f}{f^{-1}}$
  cannot give $c=\composed{f}{f^{-1}}\,(c)=f(f^{-1}(c))$ 
  because $c\notin\range(f)$.
  Therefore~$f$ must be onto.  

  To finish, we prove that if a map $\map{f}{D}{C}$
  is a correspondence then it has an inverse.
  We will consider an inverse association $\map{g}{D}{C}$ and show that
  it is well-defined.
  For each $c\in C$, let $g(c)$ be the element $d\in D$ 
  such that $f(d)=c$.
  Note that there is such an element because $f$ is onto, and also note that
  there this element is unique because $f$ is one-to-one.
  Therefore, $g$ is well-defined, that is, it is a function.
  That the compositions $\composed{g}{f}$ and $\composed{f}{g}$ are identity
  maps is clear.
\item
  Suppose that $\map{f}{D}{C}$ has two inverses $\map{g_0,g_1}{C}{D}$.
  We will show that the two same associate the same elements of the domain
  and codomain, that is,
  we will show that they have the same graph and are thus the same function.

  Let $c$ be any element of the domain~$C$ of the two $g_0$ and~$g_1$.
  The prior item of this exercise
  shows that~$f$ is onto so there exists a~$d\in D$ such that
  $f(d)=c$.
  The prior item also shows that~$f$ is one-to-one, so there is only 
  one~$d$ with that property:
  $d=g_0(c)$ and $d=g_1(c)$.
  Therefore $g_0(c)=g_1(c)$.  
\item
  Observe that $f^{-1}$ is a function that has an
  inverse, namely~$f\!$.
  This is simply because in the equations $\composed{g}{f^{-1}}=\identity$
  and $\composed{f^{-1}}{g}=\identity$ we can take $g$ to be~$f\!$.
  Then by the first item of this exercise, $f^{-1}$ is a correspondence.  
\item
  Here is the verification that composition in 
  one order gives the identity function. 
  \begin{equation*}
    \composed{(\composed{f^{-1}}{g^{-1}})}{(\composed{g}{f})}\,(x)
    =(\composed{f^{-1}}{g^{-1}})\,(g(f(x)))
    =f^{-1}(g^{-1}(g(f(x))))
    =f^{-1}(f(x))=x 
  \end{equation*}
  Verification of the other order is similar.  
\end{exparts}
\end{answer}
\end{exercise}


\begin{exercise}   \label{CorrespondingSetsHaveSameNumberOfElements}
  Prove these for a function~$f$ with a finite domain~$D$. 
  They imply that
  corresponding finite
  sets have the same size.
  \hint~for each, you can do induction on either $|\range(f)|$ or  $|D|$.
\begin{exparts}
\item
  $|\range(f)|\leq |D|$
\item
  If $f$ is one-to-one then $|\range(f)|= |D|$.
\end{exparts}
\begin{answer}
\begin{exparts}
\item
  We will do induction on the number of elements in the range.
  Before the proof we give an illustration.
  Fix $D=\set{0,1,2,3}$ and $C=\set{10,11,12}$.
  Consider the function $\map{f}{D}{C}$ given here,
  which is such that $\range(f)=C$.
  \begin{equation*}
    0\mapsto 10
    \quad 1\mapsto 10
    \quad 2\mapsto 11
    \quad 3\mapsto 12
  \end{equation*}
  To do the induction, we pick an element of the range
  and omit it; suppose that we omit~$10$.
  We then want to consider the restriction map that is onto $C-\set{10}$.
  For this map to be well-defined, we must omit from its domain 
  all of the elements that map to $10$,
  the set $f^{-1}(10)=\set{0,1}$.
  We then have the function~$\hat{f}$ with 
  domain
  $\hat{D}=\set{2,3}$ and codomain~$\hat{C}=\set{11,12}$ given here.
  \begin{equation*}
    2\mapsto 11
    \quad 3\mapsto 12
  \end{equation*}
  Note that the range of this restriction map~$\hat{f}$ is~$\hat{C}$.
  The inductive hypothesis therefore gives that $\hat{f}$'s 
  domain,~$\hat{D}$, has at least 
  as many elements as its range, $\hat{C}$. 
  To finish the argument, we will add back the omitted elements 
  to get~$f$, adding back two elements from $f$'s domain~$D$ and one from
  its range~$C$, and 
  conclude that its domain has at least as many elements as its 
  range.

  With that example in mind we can go to the induction proof.
  The base step is to consider a function~$f$ whose range is empty.
  The domain must also be empty since the definition of function
  requires that every element of the domain is mapped somewhere.
  Hence $|\range(f)|\leq |D|$ because both of them are zero.

  For the inductive step
  assume that the statement holds for all functions
  between finite sets where the range has $0$ elements, or $1$ element,
  \ldots{} or $k$~elements.
  Consider $\map{f}{D}{C}$ with $C=\range(f)$ and $|C|=k+1$.

  Fix 
  some $c_0\in C$ (the range is not empty because $k+1>0$).
  Let $\hat{C}=C-\set{c_0}$.
  Because $c_0$ is in the range of~$f\!$, the set 
  $f^{-1}(c_0)=\set{d\in D\suchthat f(d)=c_0}$ 
  has at least one element.
  Restated, where $\hat{D}=D-f^{-1}(c_0)$ we have $|\hat{D}| < |D|$,
  with a strict inequality.
  Now the key point:~the map
  $\map{\hat{f}}{\hat{D}}{\hat{C}}$
  is onto, as any element $c\in \range(f)$
  is the image of some $d\in D$ and so
  any such element with $c\neq c_0$
  must be the image of some $d\in \hat{D}$.
  Therefore the inductive hypothesis applies, giving that 
  $ |\hat{C}|\leq |\hat{D}|$.
  Finish by adding one, 
  $|\range(f)|=|\hat{C}|+1\leq |\hat{D}|+1\leq |D|$.
 
  \smallskip
  \noindent\textit{Alternate proof.}
  We can instead do
  induction on the number of elements in the domain~$D$.
  As above, we first illustrate the argument.
  Fix $D=\set{0,1,2,3}$ and codomain $C=\set{10,11,12,13}$. 
  To do induction, we will omit an element of the domain, 
  in this example~$0$, giving $\hat{D}=D-\set{0}$.
  There are two cases.
  The first is that the omitted element~$0$ maps to the same member of the 
  codomain as at least one other, not omitted, element of the domain.
  This happens with this 
  function~$f$.
  \begin{equation*}
    0\mapsto 10
    \quad 1\mapsto 10
    \quad 2\mapsto 11
    \quad 3\mapsto 12    
  \end{equation*}
  Consider the restriction,
  $\map{\hat{f}}{\hat{D}}{C}$ given by $\hat{f}(d)=f(d)$.
  The inductive hypothesis applies, giving
  $|\range(\hat{f})|\leq |\hat{D}|$.
  In this first case,
  $0$ is not the only element of~$D$ associated with~$10$,
  so while $|\hat{D}|< |D|$  we have $|\range(f)|=|\range(\hat{f})|$ and 
  the desired conclusion is immediate,
  $|\range(f)|=|\range(\hat{f})|\leq |\hat{D}|< |D|$.

  The other case is that 
  the omitted domain element~$0$ does not map to the same member of the 
  codomain as any other domain element.
  That happens for this function~$f$. 
  \begin{equation*}
    0\mapsto 10
    \quad 1\mapsto 11
    \quad 2\mapsto 12
    \quad 3\mapsto 13    
  \end{equation*}
  To do the induction, we omit~$0$ from the domain, giving 
  $\hat{D}=D-\set{0}$.
  Consider the restriction map 
  $\map{\hat{f}}{\hat{D}}{C}$ defined by $\hat{f}(d)=f(d)$.
  The inductive hypothesis applies,
  so 
  $|\range(\hat{f})|\leq |\hat{D}|$.
  Then adding~$1$ to both sides gives 
  $|\range(f)|=|\range(\hat{f})|+1\leq |\hat{D}|+1=|D|$.

  We are ready for the proof.
  The base step is to consider a function~$f$ whose domain~$D$ 
  has no elements at all.
  The only function with an empty domain is the empty function,
  whose range is empty, and so $0=|\range(f)|\leq |D|=0$.

  For the inductive step, assume that the statement is true for
  functions between finite sets when the domain of that function
  has $0$ elements, or $1$ element, \ldots{} or $k$~elements. 
  Consider~$\map{f}{D}{C}$ where $|D|=k+1$.

  Fix some $d_0\in D$
  (there must be such an element since the domain is not empty, 
  as $k+1>0$).
  Let $\hat{D}=D-\set{d_0}$ and let $\map{\hat{f}}{\hat{D}}{C}$ 
  be the restriction of~$f$ 
  to~$\hat{D}$, defined by $\hat{f}(d)=f(d)$.
  The inductive hypothesis gives 
  $|\range(\hat{f})|\leq |\hat{D}|$.

  Consider the codomain element $c_0=f(d_0)$. 
  There are two cases:~either 
  (1)~$c_0$ is also the 
  image of another domain element, 
  so that there is a 
  $d\in\hat{D}$ with $d\neq d_0$ and $f(d)=c_0$, 
  or else (2)~it is not the image of another such element.
  In the first case,
  $\range(f)=\range(\hat{f})$ and 
  we have  
  $|\range(f)|=|\range(\hat{f})|\leq |\hat{D}|<|D|$.
  In the second case, $\range(f)=\range(\hat{f})+1$ and 
  we have
  $|\range(f)|=|\range(\hat{f})|+1 \leq |\hat{D}|+1=|D|$.

\item
  We will will do induction on the number of elements in
  the range.
  The base step is that the range is empty.
  The only function with an empty range is the empty function,
  which has an empty domain, giving $0=|\range(f)|=|D|=0$. 

  For the inductive step, assume that the statement is true for any one-to-one
  function between finite sets with whose range has $0$~elements,
  or $1$~element, \ldots{} or $k$~elements. 
  Consider a one-to-one $\map{f}{D}{C}$ with $|\range(f)|=k+1$.

  Fix~$c_0\in \range(f)$
  (such an element exists because $k+1>0$).
  Let $\hat{C}=C-\set{c_0}$.  
  Because $c_0$ is an element of the range of~$f\!$, there is a 
  domain element that maps to it.
  Because~$f$ is one-to-one, there
  is only one such element. 
  Call it $d_0$ and let $\hat{D}=D-\set{d_0}$.

  The restriction function $\map{\hat{f}}{\hat{D}}{\hat{C}}$ is defined by 
  $\hat{f}(d)=f(d)$.
  This map is also one-to-one:~thinking of $f$ as a set of ordered pairs, 
  the assumption that it is one-to-one means that no two pairs 
  have the same second element, and the restriction~$\hat{f}$ is a subset
  of~$f$ so it also has no two pairs with the same second element.
  By the inductive hypothesis, 
  $|\range(\hat{f})|=|\hat{D}|$. 
  Adding~$1$ gives
  $|\range(f)|=|\range(\hat{f})|+1= |\hat{D}|+1=|D|$.
 
  \smallskip
  \noindent\textit{Alternate proof.}
  We can instead do induction on the number of elements in the domain~$D$.
  The base step is that the domain is empty,~$|D|=0$.
  The only function with an empty domain is the empty function
  and we have $0=|\range(f)|=|D|=0$.

  For the inductive step, assume that the statement is true
  for any one-to-one function between finite sets with whose domain has 
  $0$~elements, 
  \ldots{} $k$~elements. 
  Consider a one-to-one function~$\map{f}{D}{C}$ whose domain has $|D|=k+1$.

  Fix some $d_0\in D$
  (the set~$D$ is not empty because $k+1>0$) and
  let $\hat{D}=D-\set{d_0}$.
  Consider $c_0=f(d_0)$. % and also let $\hat{C}=C-\set{\hat{c}}$.
  Because $f$ is one-to-one, $c_0$~is 
  not the image of any element of the domain other than~$d_0$.
  So the restriction map $\map{\hat{f}}{\hat{D}}{C}$ 
  has one less element in its range,
  $\range(f)=\range(\hat{f})+1$.

  The key point is that because~$f$ is one-to-one,~$\hat{f}$ is also 
  one-to-one:~thinking 
  of $f$ as a set of ordered pairs, 
  one-to-one-ness means that no two pairs 
  have the same second element, and~$\hat{f}$ is a subset
  of~$f$ so it has the same property.
  Therefore the inductive hypothesis applies, giving 
  $|\range(\hat{f})|=|\hat{D}|$.
  Add~$1$ to get the desired conclusion, that
  $|\range(f)|=|\range(\hat{f})|+1= |\hat{D}|+1=|D|$.
\end{exparts}
\end{answer}
\end{exercise}


\end{exercises}
\index{function|)}



\endinput