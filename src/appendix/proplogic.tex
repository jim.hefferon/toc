\section{Propositional logic}\label{sec:PropLogic}\index{Propositional logic|(}\thispagestyle{empty}

A \definend{proposition} is a statement that has a Boolean value,
that is, it is either true or false, which we write $T$ or~$F\!$.
For instance, `$7$ is odd' and `$8^2-1=127$' are propositions,
with values $T$ and~$F\!$. 
In contrast, `$x$ is a perfect square' is not a proposition because for some 
$x$ it is~$T$ while for others it is not.

We can operate on propositions, including 
negating as with `it is not the case that $8$ is prime', or taking the
conjunction of two propositions as with `$5$ is prime and $7$ is prime'.
The \definend{truth tables}\index{truth table} 
below define the behavior of
\definend{not} (also called \definend{negation}),\index{logical operator!not}\index{negation}
\definend{and} (also called \definend{conjunction}),\index{logical operator!and}\index{conjunction}
and \definend{or} (also called \definend{disjunction}).\index{logical operator!or}\index{disjunction}
% The first is a unary, or one input, operator while the
% other two are binary operators.
% For both tables, inputs are on the left while 
% outputs are on the right. 
\begin{center}\small % \vspace{0.5ex}
  \begin{tabular}[t]{c|c}
    \multicolumn{1}{c}{\ }    
        &\multicolumn{1}{c}{\tabulartext{not} $P$}  \\[-0.25ex]
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$\neg P$}  \\
    \hline
       $\false$  &$\true$  \\
       $\true$   &$\false$
  \end{tabular}
  \qquad
  \begin{tabular}[t]{cc|cc}
    \multicolumn{1}{c}{\ }    
        &\multicolumn{1}{c}{\ }    
        &\multicolumn{1}{c}{$P$ \tabulartext{and} $Q$}  
        &\multicolumn{1}{c}{$P$ \tabulartext{or} $Q$}  \\[-0.25ex]
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$P\wedge Q$}  
        &\multicolumn{1}{c}{$P\vee Q$}  \\
    \hline
       $\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\false$  &$\true$  \\
       $\true$  &$\false$  &$\false$  &$\true$  \\
       $\true$  &$\true$  &$\true$  &$\true$  
  \end{tabular}
\end{center}
Thus where `$7$ is odd' is $P$, and `$8$ is prime' is $Q$,
get the value of `$7$ is odd and $8$ is prime' from the
right-hand table's third column, third row: $F\!$.
% The value of the disjunction `$7$ is odd or $8$ is prime' is $\true$.
Observe that $\vee$ accumulates truth, in that if any of its inputs are 
$\true$ then $P\vee Q$ is~$\true$.
Similarly, $\wedge$ accumulates~$\false$.

In some fields the practice is to write $0$ where we write~$F$ and 
$1$ in place of~$T\!$.
% (In an electronic device these might 
% stand for different voltage levels.)

The advantage of using symbols over writing the sentences out is
that we can express more things.
For instance, if $P$ stands for `7 is odd', 
$Q$ stands for `9 is a perfect square', and 
$R$ means `11 is prime' then 
$(P\vee Q)\wedge \neg (P\vee (R\wedge Q))$
is too complex to comfortably state in everyday language.
We call that a propositional logic \definend{expression}
and denote it with a capital Roman letter such as~$E$.

Truth tables help in working out the behavior of the complex 
statements
by building them up from their components.
The table below shows the input/output behavior of 
$(P\vee Q)\wedge \neg (P\vee (R\wedge Q))$.
\begin{center}\small
  \begin{tabular}{ccc|ccccc}
    \multicolumn{1}{c}{$P$}
    &\multicolumn{1}{c}{$Q$}
    &\multicolumn{1}{c}{$R$}
    &\multicolumn{1}{c}{$P\vee Q$}
    &\multicolumn{1}{c}{$R\wedge Q$}
    &\multicolumn{1}{c}{$P\vee (R\wedge Q)$}
    &\multicolumn{1}{c}{$\neg(P\vee (R\wedge Q))$}
    &\multicolumn{1}{c}{\tabulartext{expression}}     \\ \hline
    $\false$  &$\false$  &$\false$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\false$  &$\false$  &$\true$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\false$  &$\true$  &$\false$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\false$  &$\true$  &$\true$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\[.5ex]
    $\true$  &$\false$  &$\false$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\true$  &$\false$  &$\true$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\true$  &$\true$  &$\false$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    \\
    $\true$  &$\true$  &$\true$  &$\false$  &$\false$  &$\false$  &$\true$  &$\false$    
  \end{tabular}
\end{center}

The three `$\neg$', `$\wedge$', and `$\vee$' are 
\definend{operators}\index{operators!logicial}\index{logical operators}\index{Propositional logic!operators} 
(or \definend{connectives}).\index{connectives!logicial}\index{logical connectives}
There are other operators; here are two common ones.
\begin{center}\small % \vspace{0.5ex}
  \begin{tabular}[t]{cc|cc}
    \multicolumn{1}{c}{\ }    
        &\multicolumn{1}{c}{\ }    
        &\multicolumn{1}{c}{$P$ \tabulartext{implies} $Q$}  
        &\multicolumn{1}{c}{$P$ \tabulartext{if and only if} $Q$}  \\[-0.25ex]
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$P\rightarrow Q$}  
        &\multicolumn{1}{c}{$P\leftrightarrow Q$}  \\
    \hline
       $\false$  &$\false$  &$\true$   &$\true$  \\
       $\false$  &$\true$   &$\true$   &$\false$  \\
       $\true$   &$\false$  &$\false$  &$\false$  \\
       $\true$   &$\true$   &$\true$   &$\true$  
  \end{tabular}
\end{center}

% In a propositional logic expression, a single variable
% such as~$P$ is an \definend{atom}.
% An atom or its negation, $P$ or $\neg P$, is a \definend{literal}.
% A \definend{clause} is a number of literals held together with logical 
% connectives, such as
% $P\vee\neg Q\vee\neg R$.

Two statements are 
\definend{equivalent}\index{equivalent propositional logic statements}
(or \definend{logically equivalent}) 
if they 
have equal output values whenever we give the same values to the variables. 
For instance, $P\rightarrow Q$ is equivalent to $\neg P\vee Q$, because
if we assign $P=F, Q=F$ then they both give the value~$T$, 
if we assign $P=F, Q=T$ then they also give the same value, etc.
That is, the statements are equivalent when their truth tables have the
same final column. 
We denote equivalence using $\equiv$, as with $P\rightarrow Q\equiv\neg P\vee Q$

The set of formulas describing when statements are equivalent is 
\definend{Boolean algebra}.\index{Boolean algebra}\index{Propositional logic!Boolean algebra}
For instance, these are the 
distributive laws\index{distributive laws!for logic expressions}\index{Propositional logic!distributive laws}
\begin{equation*}
  P\wedge(Q\vee R)\equiv(P\wedge Q)\vee (P\wedge R) 
  \qquad
  P\wedge(Q\vee R)\equiv(P\wedge Q)\vee (P\wedge R)  
\end{equation*}
and these are
\definend{DeMorgan's laws}.\index{DeMorgan's laws!for logic expressions}\index{Propositional logic!DeMorgan's laws}
\begin{equation*}
  \neg(P\wedge Q)\equiv \neg P\vee \neg Q 
  \qquad
  \neg(P\vee Q)\equiv \neg P\wedge \neg Q  
\end{equation*}

The three operators `$\neg$', `$\wedge$', and `$\vee$' 
form a complete set in that we can reverse the activity above:~for any  
truth table we can use the three to produce an expression 
whose input/output behavior is that table. 
In short, we can produce expressions with any desired behavior.
Here are two examples.
\begin{center} \small
  \begin{tabular}[t]{cc|c}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$E_0$}  \\
    \hline
       $\false$  &$\false$  &$\true$  \\
       $\false$  &$\true$  &$\false$  \\
       $\true$  &$\false$  &$\false$  \\
       $\true$  &$\true$  &$\false$  
  \end{tabular}
  \hspace{3em}
  \begin{tabular}[t]{ccc|c}
    \multicolumn{1}{c}{$P$}
    &\multicolumn{1}{c}{$Q$}
    &\multicolumn{1}{c}{$R$}
    &\multicolumn{1}{c}{$E_1$}     \\ \hline
    $\false$  &$\false$  &$\false$  &$\false$    \\
    $\false$  &$\false$  &$\true$  &$\true$    \\
    $\false$  &$\true$  &$\false$  &$\true$    \\
    $\false$  &$\true$  &$\true$  &$\false$    \\[.5ex]
    $\true$  &$\false$  &$\false$  &$\true$    \\
    $\true$  &$\false$  &$\true$  &$\false$    \\
    $\true$  &$\true$  &$\false$  &$\false$    \\
    $\true$  &$\true$  &$\true$  &$\false$    
  \end{tabular}  
\end{center}
To produce $E_0$, on the left, 
focus on the $\true$~row. 
There, $P=\false$ and $Q=\false$ 
and so for $E_0$ we can use the expression $\neg P\wedge \neg Q$. 
For $E_1$, on the right, focus on all of the $\true$ rows.
Target the second row with $\neg P\wedge \neg Q\wedge R$.
Target the third row with $\neg P\wedge Q\wedge \neg R$.
For the fifth use~$P\wedge\neg Q\wedge\neg R$.
Then $E_1$ joins these clauses with $\vee$'s.
\begin{equation*}
(\neg P\wedge \neg Q\wedge R)
  \vee(\neg P\wedge Q\wedge \neg R)
  \vee(P\wedge\neg Q\wedge\neg R)
\tag{$*$}
\end{equation*}

In a propositional logic expression, a single variable
such as~$P$ or~$Q$ is an \definend{atom}.
An atom or its negation, such as $P$ or $\neg P$, is a \definend{literal}.
A \definend{clause} is a number of literals joined by a connective
(we stick to either $\wedge$ or $\vee$), 
so that $\neg P\wedge\neg Q\wedge R$ is a clause.

The form of ($*$) above is important.
A propositional logic expression is in 
\definend{Disjunctive Normal form}\index{Disjunctive Normal form, DNF}\index{DNF}\index{Propositional logic!Disjunctive Normal form}
or \definend{DNF}
if is a disjunction of clauses, where each clause is a conjunction of literals.

Intuition requires that there be a matching approach that starts 
with the $\false$ rows.
We illustrate with $E_0$.
The second, third, and fourth rows are $\false$.
So $E_0\equiv\neg\big((\neg P\wedge Q)\vee(P\wedge\neg Q)\vee(P\wedge Q)\big)$.
DeMorgan's second law gives
$\neg(\neg P\wedge \neg Q)\wedge\neg(P\wedge\neg Q)\wedge\neg(P\wedge Q)$.
Next DeMorgan's first law gives
$E_0\equiv (P\vee Q)\wedge(\neg P\vee Q)\wedge(\neg P\vee \neg Q)$.
\definend{Conjunctive Normal form}\index{Conjunctive Normal form}\index{CNF}\index{Propositional logic!Conjunctive Normal form}
or \definend{CNF}
is a conjunction of clauses, where each clause is a disjunction of literals.
We use CNF more than DNF.
With this form an expression evaluates to \true{} if and only if 
all of its clauses evaluate to~\true{}.
And each clause evaluates to \true{}  
if and only if at least one of its literals evaluates to~$T\!$.

A \definend{Boolean function}\index{Boolean function}\index{function!Boolean}\index{Propositional logic!Boolean function}
has Boolean inputs, that is, they are either $T$ or~$F$, and Boolean outputs.
By the prior paragraphs about DNF and CNF, 
each single-output Boolean function is
determined by some Boolean expression.

% The \definend{\probname{$3$-Satisfiability} problem}\index{3-Satisfiability problem@$3$-\probname{Satisfiability}}
% is to decide the satisfiability of propositional logic expression
% where every clause has at most three literals.


\begin{exercises}
\begin{exercise}
Make a truth table for each of these propositions.
\begin{exparts*}
\item $(P\wedge Q)\wedge R$
\item $P\wedge (Q\wedge R)$
\item $P\wedge (Q\vee R)$
\item $(P\wedge Q)\vee (P\wedge R)$
\end{exparts*}
\begin{answer}
\begin{exparts}
\item This is a table for $(P\wedge Q)\wedge R$.
\begin{display}
  \begin{tabular}[t]{ccc|cc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$R$}      
        &\multicolumn{1}{c}{$P\wedge Q$}  
        &\multicolumn{1}{c}{$(P\wedge Q) \wedge R$}  \\
    \hline
       $\false$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\false$  &$\true$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\true$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\true$  &$\false$  &$\false$  \\
       $\true$  &$\true$  &$\false$  &$\true$  &$\false$  \\
       $\true$  &$\true$  &$\true$  &$\true$  &$\true$  \\
  \end{tabular}
\end{display}
\item And this is a table for $P\wedge (Q\wedge R)$.
It has the same final column as the one from the prior item.
\begin{display}
  \begin{tabular}[t]{ccc|cc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$R$}      
        &\multicolumn{1}{c}{$Q\wedge R$}  
        &\multicolumn{1}{c}{$P\wedge (Q \wedge R)$}  \\
    \hline
       $\false$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\false$  &$\true$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\true$  &$\true$  &$\false$  \\
       $\true$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\true$  &$\false$  &$\false$  \\
       $\true$  &$\true$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\true$  &$\true$  &$\true$  &$\true$  \\
  \end{tabular}
\end{display}
\item This is a table for $P\wedge (Q\vee R)$.
\begin{display}
  \begin{tabular}[t]{ccc|cc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$R$}      
        &\multicolumn{1}{c}{$Q\vee R$}  
        &\multicolumn{1}{c}{$P\wedge (Q \vee R)$}  \\
    \hline
       $\false$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\false$  &$\true$  &$\true$  &$\false$  \\
       $\false$  &$\true$  &$\false$  &$\true$  &$\false$  \\
       $\false$  &$\true$  &$\true$  &$\true$  &$\false$  \\
       $\true$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\true$  &$\true$  &$\true$  \\
       $\true$  &$\true$  &$\false$  &$\true$  &$\true$  \\
       $\true$  &$\true$  &$\true$  &$\true$  &$\true$  \\
  \end{tabular}
\end{display}
\item And this is a table for $(P\wedge Q)\vee (P\wedge R)$.
It has the same final column as the prior item.
\begin{display}
  \begin{tabular}[t]{ccc|ccc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$R$}      
        &\multicolumn{1}{c}{$P\wedge Q$}  
        &\multicolumn{1}{c}{$P\wedge R$}  
        &\multicolumn{1}{c}{$(P\wedge Q) \vee (P\wedge R)$}  \\
    \hline
       $\false$  &$\false$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\false$  &$\true$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\false$  &$\true$  &$\true$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\false$  &$\false$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\true$  &$\false$  &$\true$  &$\true$  \\
       $\true$  &$\true$  &$\false$  &$\true$  &$\false$  &$\true$  \\
       $\true$  &$\true$  &$\true$  &$\true$  &$\true$  &$\true$  \\
  \end{tabular}
\end{display}
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
Make a truth table for these.
\begin{exparts*}
\item $\neg (P\vee Q)$
\item $\neg P\wedge \neg Q$
\item $\neg(P\wedge Q)$
\item $\neg P\vee \neg Q$
\end{exparts*}
\begin{answer}
\begin{exparts}
\item This is a table for $\neg(P\vee Q)$.
\begin{display}
  \begin{tabular}[t]{cc|cc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$P\vee Q$}      
        &\multicolumn{1}{c}{$\neg(P\vee Q)$}   \\
    \hline
       $\false$  &$\false$  &$\false$  &$\true$  \\
       $\false$  &$\true$  &$\true$  &$\false$  \\
       $\true$  &$\false$  &$\true$  &$\false$  \\
       $\true$  &$\true$  &$\true$  &$\false$   \\
  \end{tabular}
\end{display}
\item The same for $\neg P\wedge\neg Q$.
\begin{display}
  \begin{tabular}[t]{cc|ccc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$\neg P$}      
        &\multicolumn{1}{c}{$\neg Q$}      
        &\multicolumn{1}{c}{$\neg P\wedge \neg Q$}   \\
    \hline
       $\false$  &$\false$  &$\true$  &$\true$  &$\true$  \\
       $\false$  &$\true$  &$\true$  &$\false$  &$\false$  \\
       $\true$  &$\false$  &$\false$  &$\true$  &$\false$  \\
       $\true$  &$\true$  &$\false$  &$\false$  &$\false$   \\
  \end{tabular}
\end{display}
It has the same final column as the prior item.
\item This is a table for $\neg(P\wedge Q)$.
\begin{display}
  \begin{tabular}[t]{cc|cc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$P\wedge Q$}      
        &\multicolumn{1}{c}{$\neg(P\wedge Q)$}   \\
    \hline
       $\false$  &$\false$  &$\false$  &$\true$  \\
       $\false$  &$\true$  &$\false$  &$\true$  \\
       $\true$  &$\false$  &$\false$  &$\true$  \\
       $\true$  &$\true$  &$\true$  &$\false$   \\
  \end{tabular}
\end{display}
\item This is the one for $\neg P\vee\neg Q$.
\begin{display}
  \begin{tabular}[t]{cc|ccc}
    \multicolumn{1}{c}{$P$}      
        &\multicolumn{1}{c}{$Q$}      
        &\multicolumn{1}{c}{$\neg P$}      
        &\multicolumn{1}{c}{$\neg Q$}      
        &\multicolumn{1}{c}{$\neg P\vee \neg Q$}   \\
    \hline
       $\false$  &$\false$  &$\true$  &$\true$  &$\true$  \\
       $\false$  &$\true$  &$\true$  &$\false$  &$\true$  \\
       $\true$  &$\false$  &$\false$  &$\true$  &$\true$  \\
       $\true$  &$\true$  &$\false$  &$\false$  &$\false$   \\
  \end{tabular}
\end{display}
The final column is the same as the final column of the prior item.
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
For the tables below, construct a DNF
propositional logic expression:
\begin{exparts*}
\item the table on the left,
\item the one on the right.
\end{exparts*}
\begin{center} \small
  \begin{tabular}[t]{ccc|c}
    \multicolumn{1}{c}{$P$}
    &\multicolumn{1}{c}{$Q$}
    &\multicolumn{1}{c}{$R$}
    &\multicolumn{1}{c}{}     \\ \hline
    $\false$  &$\false$  &$\false$  &$\false$    \\
    $\false$  &$\false$  &$\true$  &$\true$    \\
    $\false$  &$\true$  &$\false$  &$\true$    \\
    $\false$  &$\true$  &$\true$  &$\false$    \\[.5ex]
    $\true$  &$\false$  &$\false$  &$\false$    \\
    $\true$  &$\false$  &$\true$  &$\true$    \\
    $\true$  &$\true$  &$\false$  &$\false$    \\
    $\true$  &$\true$  &$\true$  &$\false$    
  \end{tabular}  
  \qquad
  \begin{tabular}[t]{ccc|c}
    \multicolumn{1}{c}{$P$}
    &\multicolumn{1}{c}{$Q$}
    &\multicolumn{1}{c}{$R$}
    &\multicolumn{1}{c}{}     \\ \hline
    $\false$  &$\false$  &$\false$  &$\true$    \\
    $\false$  &$\false$  &$\true$  &$\false$    \\
    $\false$  &$\true$  &$\false$  &$\true$    \\
    $\false$  &$\true$  &$\true$  &$\false$    \\[.5ex]
    $\true$  &$\false$  &$\false$  &$\false$    \\
    $\true$  &$\false$  &$\true$  &$\false$    \\
    $\true$  &$\true$  &$\false$  &$\true$    \\
    $\true$  &$\true$  &$\true$  &$\true$    
  \end{tabular}  
\end{center}
\begin{answer}
\begin{exparts}
\item
The second, third and sixth lines show \true.
The associated clauses are $\neg P\wedge\neg Q\wedge R$, 
$\neg P\wedge Q\wedge \neg R$,
and
$P\wedge\neg Q\wedge R$.
The resulting CNF expression is this.
\begin{equation*}
  (\neg P\wedge\neg Q\wedge R)
  \vee
  (\neg P\wedge Q\wedge \neg R)
  \vee
  (P\wedge\neg Q\wedge R)
\end{equation*}
\item
The \true{} lines give four clauses.
\begin{equation*}
  (\neg P\wedge \neg Q\wedge \neg R)
  \vee (\neg P\wedge Q\wedge \neg R)
  \vee (P\wedge Q\wedge \neg R)
  \vee (P\wedge Q\wedge R)
\end{equation*}
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
For the tables in the prior exercise, construct a CNF
propositional logic expression:
\begin{exparts*}
\item the table on the left,
\item the one on the right.
\end{exparts*}
\begin{answer}
\begin{exparts}
\item
The \false{} lines are first, fourth, fifth, seventh, and eighth.
So we have this.
\begin{equation*}
  E_0\equiv\neg\bigg( (\neg P\wedge\neg Q\wedge \neg R)\vee (\neg P\wedge Q\wedge R)\vee(P\wedge\neg Q\wedge \neg R)\vee(P\wedge Q\wedge \neg R)\vee(P\wedge Q\wedge  R)  \bigg)
\end{equation*}
Apply DeMorgan's law to distribute the negation across the $\vee$'s.
\begin{equation*}
   \neg(\neg P\wedge\neg Q\wedge \neg R)
   \wedge \neg(\neg P\wedge Q\wedge R)
   \wedge\neg(P\wedge\neg Q\wedge \neg R)
   \wedge\neg(P\wedge Q\wedge \neg R)
   \wedge\neg(P\wedge Q\wedge  R)
\end{equation*}
Now distribute the $\neg$'s across the $\wedge$'s.
\begin{equation*}
   (P\vee Q\vee R)
   \wedge (P\vee \neg Q\vee \neg R)
   \wedge (\neg P\vee Q\vee R)
   \wedge (\neg P\vee \neg Q\vee R)
   \wedge (\neg P\vee \neg Q\vee  \neg R)
\end{equation*}
\item
Find the \false{} lines.
\begin{equation*}
  E_1\equiv\neg\bigg( (\neg P\wedge\neg Q\wedge R)\vee (\neg P\wedge Q\wedge R)\vee(P\wedge\neg Q\wedge \neg R)\vee(P\wedge \neg Q\wedge R) \bigg)
\end{equation*}
Distribute the negation $\neg$ across the $\vee$'s.
\begin{equation*}
  \neg(\neg P\wedge\neg Q\wedge R)\wedge \neg(\neg P\wedge Q\wedge R)\wedge\neg(P\wedge\neg Q\wedge \neg R)\wedge\neg(P\wedge \neg Q\wedge R) 
\end{equation*}
Distribute the four $\neg$'s across the $\wedge$'s.
\begin{equation*}
  (P\vee Q\vee \neg R)
  \wedge (P\vee \neg Q\vee \neg R)
  \wedge (\neg P\vee Q\vee R)
  \wedge (\neg P\vee Q\vee \neg R)
\end{equation*}
\end{exparts}
\end{answer}
\end{exercise}

\begin{exercise}
There are sixteen binary logical operators.
Give all sixteen truth tables, and give the operator's name,
such as `$P\rightarrow Q$' or `$Q\rightarrow P$'. 
\begin{answer}
Here are the sixteen.
\begin{display}
\begin{tabular}[t]{*{4}{c}}
  \begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_0$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{1}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{2}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{3}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}                        \\[12ex]
  \begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{4}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{5}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{6}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{7}$}  \\
    \hline
    $F$ &$F$ &$F$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}                        \\[12ex]
  \begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{8}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{9}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{10}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{11}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$F$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}                        \\[12ex]
  \begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{12}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{13}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$F$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{14}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$F$  \\
  \end{tabular}
  &\begin{tabular}[t]{*{2}{c}|c}
    \multicolumn{1}{c}{$P$} 
      &\multicolumn{1}{c}{$Q$} 
      &\multicolumn{1}{c}{$O_{15}$}  \\
    \hline
    $F$ &$F$ &$T$  \\
    $F$ &$T$ &$T$  \\
    $T$ &$F$ &$T$  \\
    $T$ &$T$ &$T$  \\
  \end{tabular}                        \\
\end{tabular}
\end{display}
The operators are: 
$O_{0}$ is $F$ (sometimes called falsum and represented $\bot$),
$O_{1}$ is $P\wedge Q$,
$O_{2}$ is $\neg(P\rightarrow Q)$,
$O_{3}$ is $P$,
$O_{4}$ is $\neg(Q\rightarrow P)$,
$O_{5}$ is $Q$,
$O_{6}$ is $\neg(P\leftrightarrow Q)$,
$O_{7}$ is $P\vee Q$,
$O_{8}$ is $\neg(P\vee Q)$,
$O_{9}$ is $P\leftrightarrow Q$,
$O_{10}$ is $\neg Q$,
$O_{11}$ is $Q\rightarrow P$,
$O_{12}$ is $\neg P$,
$O_{13}$ is $P\rightarrow Q$,
$O_{14}$ is $\neg(P\wedge Q)$,
and $O_{15}$ is $T$ (sometimes represented $\top$).
\end{answer}
\end{exercise}

\end{exercises}
\index{Propositional logic|)}

\endinput