\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

\usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 
\definecolor{alertcolor}{RGB}{203, 0 , 34}
\setbeamercolor{alerted text}{fg=alertcolor}

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../presentation,../presentationfonts}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Prologue: the definition of computation}

\author{Jim Hef{}feron}
\institute{
  University of Vermont\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Introduction to Computability}

\usepackage{catchfilebetweentags} % read text from background.tex
\newcommand{\catchfilefn}{../../prologue/prologue.tex}
% Keep from swallowing end-of-lines
%   https://tex.stackexchange.com/a/40704/121234
\usepackage{etoolbox}
\makeatletter
\patchcmd{\CatchFBT@Fin@l}{\endlinechar\m@ne}{}
  {}{\typeout{Unsuccessful patch!}}
\makeatother

\usepackage{xr}
  \externaldocument{../../book}
  \externaldocument{../../prologue/prologue}

% Directory of this chapter's asy material
\newcommand{\asydir}{../../prologue/asy/}
\renewcommand{\tmexercises}{\asydir tm_exercises/pdfs/}
% Directory of the src/asy material
\newcommand{\generalasydir}{../../asy/}

\usepackage{multirow,bigstrut}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}




\section{Turing machines}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Introduction: What can be done?}

Here is a prototypical mathematical job.
We get a linear system.
\begin{equation*}\setlength{\arraycolsep}{.1111em}
\begin{array}{@{}*{3}{rc}r@{}}
  2x &  &   &+ &z  &=  &3   \\
   x &- &y  &- &z  &=  &1   \\
  3x &- &y  &  &   &=  &4   \\
\end{array}
\end{equation*}
Finding a single solution 
that satisfies this system $(x,y,z)=(3/2,1/2,0)$  is okay
but we prefer to find them all, as here.
\begin{equation*}
  \set{(x,y,z)=(\frac{3}{2}-\frac{1}{2}z, \frac{1}{2}-\frac{3}{2}z, z)\suchthat z\in\R}
\end{equation*}
And, we are most charmed when we have a procedure that inputs any
linear system at all and returns its complete set of solutions.

\pause\medskip
In asking what can be done mathematically we are asking what can be done 
systematically, or uniformily.
This course studies what can and cannot be done with a computer.
\end{frame}


\begin{frame}{Some history}
In 1928, the famous mathematician
D~Hilbert and his student W~Ackermann proposed the
\textit{Entscheidungsproblem}.
It asks for a definite procedure
that decides, from an input mathematical
statement, whether that statement is true or false.

Wanting to know what is true or false requires no explanation.
But what is a `definite procedure'?

In mathematics we take Euclid's development as a paradigm.
From the axioms we prove theorems in a sequence of steps.
These steps are such that you can verify each one 
typographically, 
by pushing symbols around without any unexplainable leaps of
intuition.
Thus in our mathematical culture 
a definite procedure is a sequence of fully-specified
steps that go from some input to a result.
So Hilbert and Ackermann were asking for 
what we today call an algorithm, 
which you could cast into code and run on a computer.

Thus, a important question in the air was to 
define what can be mechanically computed.
(Remember that in 1928 there were no computers as we know them today.)
\end{frame}


\begin{frame}
  \frametitle{Turing machine}
The approach to the definition of mechanical computation 
that we will follow was given by A~Turing. 
He described a mechanism that does the computing.
% (An advantage of his approach is that we can give the complete description
% in one class period. 
% Another is that his model remains widely used today, and is the basis for 
% the work in the final chapter.
% We will say more in the next section.)

As a prototype he imagined a clerk doing by-hand multiplication
with a sheet of paper that gradually becomes covered with columns of
numbers.
With that model he gave a number of conditions that 
the computer must satisfy.

\pause
\begin{itemize}
\item
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachinei}
\pause\item
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachineii}
\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachineiii}
\pause\item
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachineiv}
\end{itemize}
\end{frame}


\begin{frame}
Turing pictured a box containing a
mechanism and fitted with a tape.
\begin{center}
   \includegraphics{\generalasydir /share/machine00.pdf}
\end{center}
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachinev}

\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachinevi}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachinevii}

\ExecuteMetaData[\catchfilefn]{discussion:DefTuringMachineviii}

Before the formal definition, we first see a couple of examples.
\end{frame}


\begin{frame}{First example computation: the predecessor function}
\ExecuteMetaData[\catchfilefn]{ex:PredecessorTMi}

This shows a stretch of tape 
along with the machine's state and the position of its read-write head.
\ExecuteMetaData[\catchfilefn]{ex:PredecessorTMii}
\end{frame}

\begin{frame}
Here is the machine again
\begin{equation*}
 \TM_{\text{pred}}=
  \set{ \tminstruction{0}{B}{L}{1},\,
        \tminstruction{0}{1}{R}{0},\,
        \tminstruction{1}{B}{L}{2},\,
        \tminstruction{1}{1}{B}{1},\,
        \tminstruction{2}{B}{R}{3},\,
        \tminstruction{2}{1}{L}{2} 
      }
\end{equation*}
and here are the later steps of the computation.
\ExecuteMetaData[\catchfilefn]{ex:PredecessorTMiii}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{ex:PredecessorTMiv}

\pause
In place of the set of four-tuples
\begin{equation*}
 \TM_{\text{pred}}=
  \set{ \tminstruction{0}{B}{L}{1},\,
        \tminstruction{0}{1}{R}{0},\,
        \tminstruction{1}{B}{L}{2},\,
        \tminstruction{1}{1}{B}{1},\,
        \tminstruction{2}{B}{R}{3},\,
        \tminstruction{2}{1}{L}{2} 
      }
\end{equation*}
we can also describe this machine with a table
or picture it with a graph.
\begin{center}
  \begin{transitionfunction}
           &$\blank$      &\str{1}        \\ \cline{2-3}
     $q_0$ &$\str{L}q_1$  &$\str{R}q_0$     \\ 
     $q_1$ &$\str{L}q_2$  &$\str{B}q_1$     \\ 
     $q_2$ &$\str{R}q_3$  &$\str{L}q_2$     \\  
     $q_3$ &--            &--     \\
  \end{transitionfunction}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{\asydir circlediagram/circlediagram00.pdf}}
\end{center}
\end{frame}



\begin{frame}{Second example computation: addition of two numbers}
\ExecuteMetaData[\catchfilefn]{ex:AdditionTMi}
Here is the table and graph for $\TM_{\text{add}}$. 
\begin{center}
  \begin{transitionfunction}
           &$\blank$      &\str{1}        \\ \cline{2-3}
     $q_0$ &$\str{B}q_1$  &$\str{R}q_0$     \\ 
     $q_1$ &$\str{1}q_1$  &$\str{1}q_2$     \\ 
     $q_2$ &$\str{B}q_3$  &$\str{L}q_2$     \\  
     $q_3$ &$\str{R}q_3$  &$\str{B}q_4$     \\  
     $q_4$ &$\str{R}q_5$  &$\str{1}q_5$     \\  
     $q_5$ &--            &--     \\
  \end{transitionfunction}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{\asydir circlediagram/circlediagram01.pdf}}
\end{center}
\ExecuteMetaData[\catchfilefn]{ex:AdditionTMii}
\end{frame}
\begin{frame}
Here are the other steps in the computation
(the initialization above is step~$0$).
\ExecuteMetaData[\catchfilefn]{ex:AdditionTMiii}
\end{frame}


\begin{frame}{Some Turing machines don't halt}
A crucial observation:~some Turing machines,
for at least some starting configurations, never halt.
\ExecuteMetaData[\catchfilefn]{ex:SomeTMsDontHalt}
% \begin{center}
%   \vcenteredhbox{\includegraphics{\asydir circlediagram/infloop.pdf}}
% \end{center}
\end{frame}


\begin{frame}{Definition of a Turing machine}
\ExecuteMetaData[\catchfilefn]{df:TM}

\medskip
We denote a Turing machine with a $\TM$ because although these 
machines are hardware, 
the things from everyday experience that they are most like are programs.
\end{frame}


\begin{frame}{The action of a machine}
We can give a mathematically precise description
of how Turing machines act.
Thus for instance we could rigorously prove that the first machine above
computes the predecessor of its input.

\ExecuteMetaData[\catchfilefn]{def:TMConfigurationi}

\ExecuteMetaData[\catchfilefn]{def:TMConfigurationii}

\ExecuteMetaData[\catchfilefn]{def:TMConfigurationiii}

More details are in the text.
The example below gives the idea.
\end{frame}
\begin{frame}
\begin{example}
This machine decides whether its input is an odd number.
\begin{equation*}
  \TM_{\text{odds}}=\set{
    \tminstruction{0}{B}{B}{4},\,
    \tminstruction{0}{1}{B}{1},\,
    \tminstruction{1}{B}{R}{2},\,
    \tminstruction{2}{B}{1}{4},\,
    \tminstruction{2}{1}{B}{3},\,
    \tminstruction{3}{B}{R}{0}
    }
\end{equation*}
In the computation steps below the machine starts with \str{111} on the tape 
and with the read/write head pointing
to the leftmost~\str{1}.
(Recall that
in a configuration 
$\mathcal{C}=\sequence{q,s,\tau_{L},\tau_{R}}$,
the first item is the state, the second is the symbol now being read on the 
tape, the third is the tape string to the left of the head and the
fourth is the string to the right of the head.)
\begin{align*}
  \sequence{q_0,\str{1},\emptystring,\str{11}}
  &\yields\sequence{q_1,\str{B},\emptystring,\str{11}}  \\
  &\yields\sequence{q_2,\str{1},\emptystring,\str{1}}  \\
  &\yields\sequence{q_2,\str{B},\emptystring,\str{1}}  \\
  &\yields\sequence{q_0,\str{1},\emptystring,\emptystring}  \\
  &\yields\sequence{q_1,\str{B},\emptystring,\emptystring}  \\
  &\yields\sequence{q_2,\str{B},\emptystring,\emptystring}  \\
  &\yields\sequence{q_4,\str{1},\emptystring,\emptystring}
\end{align*}
This is just a slightly different presentation than the tape views that 
we saw earlier.
The machine now halts because there is no instruction for $q_pT_p=q_4\str{1}$.

This machine always ends with the head pointing to either a \str{1}
or a \str{B}, and nothing else on the tape.
Above we got a \str{1} so we interpret it as 
saying that the original input, $3$, is odd.
If instead we got a blank then 
we would take that to mean that the input is even.
\end{example}
\end{frame}



\begin{frame}{Definition of a computable function}
\ExecuteMetaData[\catchfilefn]{df:FcnComputedByTMi}

\ExecuteMetaData[\catchfilefn]{df:FcnComputedByTMii}

\ExecuteMetaData[\catchfilefn]{df:FcnConverge}


\ExecuteMetaData[\catchfilefn]{discussion:FcnVsMachine}
\end{frame}


\begin{frame}{Discussion: representations}
\ExecuteMetaData[\catchfilefn]{discussion:FcnConverge}
\end{frame}


\begin{frame}
\ExecuteMetaData[\catchfilefn]{df:ComputableFcn}

\begin{example}
The computable functions form a very big collection.
All of these are computable.
\begin{equation*}
  f_0(x)=2x\qquad f_1(x)=x^2\qquad f_2(x)=2^x
\end{equation*}
So are these.
\begin{equation*}
  f_3(x)=
  \begin{cases}
    x/2 &\case{if $x$ is even}  \\
    3x+1 &\case{if $x$ is odd}
  \end{cases}
 \qquad
  f_4(x)=\text{the least prime number greater than x}
\end{equation*}
The computable sets are also a big collection.
These sets are computable.
\begin{equation*}
  S_0=\set{2n\suchthat n\in\N}
  \qquad
  S_1=\set{x\suchthat\text{the prime factorization has three distinct primes}}
\end{equation*}
\end{example}
\end{frame}


% ===========================================
\section{Church's Thesis}

\begin{frame}{Church's Thesis}
A Church asserted the following, which essentially says that Turing's 
characterization of what can be done by a machine is correct.
It explains our spending a whole course thinking about this model
of machines and related ones.
We will discuss evidence for it below.

% \ExecuteMetaData[\catchfilefn]{ChurchsThesis}
\begin{center}
\framebox{\alert{\parbox{0.9\textwidth}{The set of things that can be computed 
by a discrete and deterministic mechanism 
is the same as the set of things that can be computed by
a Turing machine.}}}
\end{center}
 
  (Some authors call this the Church-Turing Thesis.  
  Here we figure that because Turing has the machine, 
  we can give Church sole possesion of the thesis.)
\end{frame}

\begin{frame}{Evidence}
\begin{description}[xxx]  % optional argument sets width of indent
\item[Coverage]
Everything that people think of as intuitively computable has proven to
be computable on a Turing machine.
\pause\item[Convergence]
There have been many models of computation proposed by researchers and all 
compute the same set of things, namely the set of things computed by
Turing machines.
\pause\item[Consistency]
The details of the definition, including the number of tapes or tape
heads, the number of symbols available, the number of states, all
don't change the collection of things computed.
(This includes the condition of determinism.)
\pause\item[Clarity]
Turing's analysis is persuasive of itself.
\end{description}
\end{frame}

\begin{frame}{An empirical question?}
As yet, there is no persuasive evidence of physically possible
systems that allow you to compute things that a Turing machine
cannot compute.
(We shall take proposals involving wormholes, black hole event horizons,
etc., 
as too speculative to be as-yet persuasive.)

In particular, we expect that quantum computers, should they become practical,
will run faster on some problems. 
But they are not more capable in principle\Dash
the set of things computed by 
quantum computers is the same as the set of things computed by 
Turing machines.
\end{frame}

\begin{frame}{Church's Thesis in arguments}
Church's Thesis equates `computable' with `computable by a Turing machine'.
We will leverage this in two ways.
\begin{enumerate}
\item It gives our results a larger, philosophical, importance.
When we produce a result of the form 
\textit{No Turing machine can \ldots{}} then we can use
Church's Thesis to interpret it as \textit{No mechanism can \ldots{}}
\pause\item To prove results about Turing machines we often 
argue intuitively.
For instance, consider the problem of whether this function is computable:~it 
inputs a number~$n$ and
returns~$1$ when $n$~is a prime, and~$0$ otherwise.
To show that this function can be computed, we will not exhibit
a Turing machine set of four-tuple instructions.
Instead we will argue that the function is intuitively computable,
often by giving some pseudo-code and perhaps some actual code, 
and then invoke Church's Thesis.

Working intuitively is potentially hazardous. 
However, we have so much experience with computation
that our intuition is quite reliable.
For us, there is more of a danger
of being overwhelmed by the detail of four-tuples
than a danger of being too hand-wavy about algorithms. 
\end{enumerate}
\end{frame}



% ======================================================
\section{Primitive recursive functions}

\begin{frame}{Definition by recursion}
\ExecuteMetaData[\catchfilefn]{discussion:PrimReci}

\ExecuteMetaData[\catchfilefn]{discussion:PrimRecii}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:PrimReciii}

\pause
\ExecuteMetaData[\catchfilefn]{discussion:PrimReciv}

\begin{example}
\begin{align*}
  \power(3,2) &=\product(\power(3,1),3) \\
              &=\product(\product(\power(3,0),3),3)=9
\end{align*}
\end{example}
\end{frame}

\begin{frame}{Defining by primitive recursion}
\ExecuteMetaData[\catchfilefn]{def:PrimRec}

\ExecuteMetaData[\catchfilefn]{ex:PrimReci}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{ex:PrimRecii}
\end{frame}

\begin{frame}{Collection of Primitive Recursive Functions}
\ExecuteMetaData[\catchfilefn]{def:PrimRecFcns}

The collection of primitive recursive functions is very large.
It includes addition,
proper subtraction, multiplication, integer division, exponentiation,
the equality Boolean function, the remainder function, the sum of a 
bounded number of terms, the product of a bounded number of terms, and
just about every function that comes to mind.
\end{frame}



% ===================================================
\section{General Recursive Functions}

\begin{frame}{Ackermann's function}
\ExecuteMetaData[\catchfilefn]{discussion:Ackermanni}

\ExecuteMetaData[\catchfilefn]{discussion:Ackermannii}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:Ackermanniii}
\end{frame}



\begin{frame}
\ExecuteMetaData[\catchfilefn]{def:Hyperoperation}

The hyperoperation is an \alert{Ackermann's function}.

\ExecuteMetaData[\catchfilefn]{lem:Hyperoperation}
\end{frame}
\begin{frame}
Our interest in the 
$\hyperoperation$ operation is that it is intuitively computable
but not primitive recursive. 
\ExecuteMetaData[\catchfilefn]{thm:Hyperoperation}
(This is proved in an Extra section.)

\begin{remark}
At first glance it could appear that many of the arguments for Church's Thesis
apply to the set of primitive recursive
functions.
However this theorem, proved by Ackermann, shows that there are functions that
are intuitively mechnically computable but which are not primitive recursive.
So we need more than the schema of primitive recursion.  
\end{remark}
\end{frame}


\begin{frame}{$\mu$ recursion}
\ExecuteMetaData[\catchfilefn]{def:MuRecursioni}
\ExecuteMetaData[\catchfilefn]{def:MuRecursionii}

\pause
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:PrimeProducingPoly}
(Note that $f(1,1,41)=40$.)
\end{example}
\end{frame}
% sage: def f(x):
% ....:     return x*x+x+41
% ....: 
% sage: f(1)
% 43
% sage: for x in range(41):
% ....:     if f(x) in Primes():
% ....:         print(str(x)+" is prime")
% ....:     else:
% ....:         print(str(x)+" is not prime")
% ....: 
% 0 is prime
% 1 is prime
% 2 is prime
% 3 is prime
% 4 is prime
% 5 is prime
% 6 is prime
% 7 is prime
% 8 is prime
% 9 is prime
% 10 is prime
% 11 is prime
% 12 is prime
% 13 is prime
% 14 is prime
% 15 is prime
% 16 is prime
% 17 is prime
% 18 is prime
% 19 is prime
% 20 is prime
% 21 is prime
% 22 is prime
% 23 is prime
% 24 is prime
% 25 is prime
% 26 is prime
% 27 is prime
% 28 is prime
% 29 is prime
% 30 is prime
% 31 is prime
% 32 is prime
% 33 is prime
% 34 is prime
% 35 is prime
% 36 is prime
% 37 is prime
% 38 is prime
% 39 is prime
% 40 is not prime

\renewcommand{\prfiles}{../../scheme/prologue/}
\begin{frame}
\begin{example}
\ExecuteMetaData[\catchfilefn]{ex:GoldbachConjecturei}
\end{example}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{ex:GoldbachConjectureii}
\pause
\begin{example}
A composite number~$n$ with the property that $b^n\equiv b\pmod n$ for all~$b$
is a \alert{Carmichael number}.
The first one is~$561$.
This Boolean function
is primitive recursive.
\begin{equation*}
  C(n)=
  \begin{cases}
    1  &\case{if $n$ is a Carmichael number}  \\
    0  &\case{otherwise}
  \end{cases}
\end{equation*}
Here we define the $x$-th Carmichael number $\map{F}{\N}{\N}$
by $\mu$-recursion.
\begin{equation*}
  F(x)=
  \begin{cases}
    561  &\case{if $x=0$}           \\
    \least y\,\big[ \text{$y>F(x-1)$ and $1-C(y)=0$} \big]
         &\case{otherwise}
    \end{cases}
\end{equation*}
\end{example}
\end{frame}


% \begin{frame}
% \begin{example}
% \alert{Twin primes} are ones that differ by two, such as $3$ and~$5$, 
% or $17$ and~$19$, or $599$ and~$601$.
% We don't know if there are infinitely many such pairs.
% Let
% \begin{equation*}
%   g(y)=
%   \begin{cases}
%     0  &\case{if $x$ and $x+2$ are both prime}  \\
%     1  &\case{otherwise}
%   \end{cases}
% \end{equation*}
% then the function giving the $x$-th twin prime pair is this.
% \begin{equation*}
%   f(x)=
%   \begin{cases}
%     2  &\case{if $x=0$}  \\
%     \least y\,\big[ \text{$y>f(x-1)$ and $g(y)=0$} \big]  &\case{if $x>0$}
%   \end{cases}
% \end{equation*}
% \end{example}

% \begin{example}
% \alert{Goldbach's conjecture} is that every whole number greatere than two
% is the sum of two prime numbers.
% Consider this function.
% \begin{equation*}
%   g(y)=
%   \begin{cases}
%     1  &\case{if $y$ is the sum of two primes smaller than it}  \\
%     0  &\case{otherwise}
%   \end{cases}
% \end{equation*}
% It is primitive recursive.
% The function
% $f(x)=\least y\,\big[ g(y)=0 \big]$
% looks for a counterexample to the conjecture.
% That is, the conjecture is false if $f(x)$ is defined for any $x$.
% \end{example}

% \end{frame}


\begin{frame}
\ExecuteMetaData[\catchfilefn]{def:GeneralRecursive}

It is beyond our scope, but we could prove that 
the collection of general recursive functions is the same set as the
collection of functions computed by some Turing machine, which we
call the `computable functions'.

(Some authors define the computable functions as the recursive functions,
in contrast with our using Turing machines. 
Such a development
has the advantage of going straight to functions without having to
do representations.
But it has the disadvantage of losing the  
evident connection with what can be mechanically computed
that comes with Turing's approach.)  
\end{frame}

\end{document}






