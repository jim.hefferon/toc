\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

\usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 
\definecolor{alertcolor}{RGB}{203, 0 , 34}
\setbeamercolor{alerted text}{fg=alertcolor}

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../presentation,../presentationfonts}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Background}

\author{Jim Hef{}feron}
\institute{
  University of Vermont\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Cardinality}

\usepackage{catchfilebetweentags} % read text from background.tex
\newcommand{\catchfilefn}{../../background/background.tex}
% Keep from swallowing end-of-lines
%   https://tex.stackexchange.com/a/40704/121234
\usepackage{etoolbox}
\makeatletter
\patchcmd{\CatchFBT@Fin@l}{\endlinechar\m@ne}{}
  {}{\typeout{Unsuccessful patch!}}
\makeatother

\usepackage{xr}
  \externaldocument{../../book}
  \externaldocument{../../background/background}


% Directory of this chapter's asy material
\newcommand{\asydir}{../../background/asy/}
% \newcommand{\tmexercises}{\asydir tm_exercises/pdfs/}
% Directory of the src/asy material
\newcommand{\generalasydir}{../../asy/}

\usepackage{multirow,bigstrut}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}




\section{Cardinality}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Finite sets have the same size iff there is a correspondence}
\begin{center}\small\setlength{\bigstrutjot}{30pt}
  \hspace*{-4em}% table looks funny else
  \begin{tabular}{rr|c@{\hspace*{3em}}c}
    \multicolumn{2}{c}{} &\multicolumn{2}{c}{\textit{One-to-one?}}  \\
    \multicolumn{2}{c}{} &\textit{No}  &\textit{Yes}   \\ \cline{3-4}
   \multirow{2}[2]{*}{\it Onto?}
   &\textit{No}
    &\bigstrut\vcenteredhbox{\includegraphics{asy/background02.pdf}} 
    &\vcenteredhbox{\includegraphics{asy/background01.pdf}}     \\
   &\textit{Yes}
    &\bigstrut\vcenteredhbox{\includegraphics{asy/background00.pdf}} 
    &\vcenteredhbox{\includegraphics{asy/background03.pdf}}  \\
  \end{tabular}
\end{center}

\ExecuteMetaData[\catchfilefn]{lem:OneToOneOntoFiniteSets}
\end{frame}


\begin{frame}
  \frametitle{Cardinality definition}\vspace*{-1ex}
\ExecuteMetaData[\catchfilefn]{lem:EquinumerosityIsEquivalence}

\ExecuteMetaData[\catchfilefn]{def:Cardinality}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/background04.pdf}} 
\end{center}

\pause
\begin{example}
These have the same cardinality:
(1)~$|A|=|B|$ where 
$A=\set{x\in\R\suchthat 0\leq x<1}$
and $B=\set{x\in\R\suchthat 1\leq x<5}$
(2)~$|C|=|D|$ where 
$C=\set{x\in\N\suchthat 0\leq x<10}$
and $D=\set{x\in\N\suchthat 1\leq x<11}$.
Further, 
(3)~if $I=\set{x\in\R\suchthat 0<x<1}$ then $|I|=|\R|$
and 
(4)~if $S=\set{x\in\N\suchthat \text{$x$ is a perfect square}}$ then $|S|=|\N|$.
\end{example}

\pause
\ExecuteMetaData[\catchfilefn]{def:FiniteInfinite}
\ExecuteMetaData[\catchfilefn]{def:Countable}
\ExecuteMetaData[\catchfilefn]{Enumeration}
\end{frame}


\begin{frame}
\begin{example}
The set $\set{n^2\suchthat n\in\N}$ is countably infinite.
It is enumerated by the function $\map{f}{\N}{\N}$ given by $f(x)=x^2$.
Note that this enumeration is effective.
\end{example}

\begin{example}
The set $\N-\set{0,1,2}=\set{3,4,5,6,7,\ldots}$ is countably infinite.
The function $f(x)=x+3$ closes the gap.
\begin{equation*}
  \begin{array}{r|ccccccc@{\hspace{6pt}}c}
    n
      &0 &1 &2 &3 &4 &5 &6 &\ldots \\ \cline{2-9}
    f(n)
      &3
      &4
      &5
      &6
      &7
      &8
      &9
      &\ldots
  \end{array}
  \qquad    
\end{equation*}
This function is clearly one-to-one and onto, as well as computable.
\end{example}

\ExecuteMetaData[\catchfilefn]{ex:SetOfPrimesCountable}
\ExecuteMetaData[\catchfilefn]{ex:SetOfIntegersCountable}
\end{frame}


% ..............................
\begin{frame}
  \frametitle{Cross product}
\begin{example}
If $N_2=\set{0,1}\times\N$ then $|N_2|=|\N|$.
\begin{center}
  \includegraphics{../../background/asy/correspondences/correspondences1000.pdf}
\end{center}
The picture above shows $N_2$ as two $\N$'s.
Nonetheless we can enumerate it.
\pause
\ExecuteMetaData[\catchfilefn]{table:CrossProductCorrespondence}
\ExecuteMetaData[\catchfilefn]{def:PairingFcn}
\end{example}

\begin{example}
If $N_3=\set{0,1,2}\times\N$ then $|N_3|=|\N|$.
For any finite~$k$, 
if $N_k=\set{0,1,\ldots k-1}\times\N$ then $|N_k|=|\N|$.
\end{example}
\ExecuteMetaData[\catchfilefn]{lem:CrossProdFiniteCountableIsCountable}
\end{frame}



%========================================
\section{Cantor's correspondence}
\begin{frame}
  \frametitle{Cantor's correspondence}
Next is to enumerate an array that is unbounded in two dimensions.
% \ExecuteMetaData[\catchfilefn]{eqn:TwoByTwoArray}
\begin{center}
  \only<1>{\includegraphics{\asydir correspondences/correspondences2000.pdf}}%
  \only<2->{\includegraphics{\asydir correspondences/correspondences2010.pdf}}
\end{center}
\pause
\ExecuteMetaData[\catchfilefn]{table:CantorsCorrespondence}
\ExecuteMetaData[\catchfilefn]{def:CantorsCorrespondence}
\pause
\ExecuteMetaData[\catchfilefn]{lem:CantorsCorrespondenceIsCorrespondence}
\ExecuteMetaData[\catchfilefn]{cor:CroosProdFinitelyManyCountablyManyIsCountable}
\end{frame}





\begin{frame}
  \frametitle{Counting Turing machines}

\ExecuteMetaData[\catchfilefn]{discussion:EnumeratingTMsi}

\ExecuteMetaData[\catchfilefn]{discussion:EnumeratingTMsii}
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:EnumeratingTMsii}  
\end{frame}


\begin{frame}
\ExecuteMetaData[\catchfilefn]{def:NumberingTMs}
We fix some acceptable numbering that we will use for the rest of the course.
\pause
\ExecuteMetaData[\catchfilefn]{lem:EachTMHasInfManyIndices}
\ExecuteMetaData[\catchfilefn]{pf:EachTMHasInfManyIndices}
\end{frame}



\begin{frame}
  \frametitle{A way to informally think about numbering}
Turing machines are like programs.
Imagine that you write a program $\TM$ and save it to disc.
It lives on the hard drive as a bitstring. 
Think of that bitstring as a number,~$e$, written in binary.
In this analogy, the program and the number correspond.
In both directions the association is effective:~from the source code in your
editor the system derives its bit string representation, and from the 
bit string representation on the disc the system can recover the program's 
source.  
\begin{center}
  \includegraphics{asy/background05.pdf}
\end{center}
This is only an analogy and it isn't perfect\Dash one problem is that 
leading \str{0}'s
in the bit string cause ambiguity\Dash but it
can helpin thinking about numbering.
A Turing machine's index number~$e$ is a name, a way to refer to that machine.
That reference is effective, meaning that there is a program that goes from the 
Turing machine source 
to the index and a program that goes from the index 
to the source.
\end{frame}





\section{Diagonalization}

\begin{frame}
  \frametitle{There are sets that cannot be counted}
\ExecuteMetaData[\catchfilefn]{th:NoOntoMapNToR}

Before the proof, an example.
% \ExecuteMetaData[\catchfilefn]{table:EnumerateR}
\only<1>{\begin{equation*} \setlength{\arraycolsep}{0.75\arraycolsep}
  \begin{array}{c|r@{\hspace*{.35em}.\hspace*{.35em}}ccccccc@{\hspace*{1em}\ldots\;}}
    \multicolumn{1}{c}{\text{\small $n$\hspace*{0.25em}}} 
    &\multicolumn{8}{c}{\text{\hspace*{-1.00em}\small\tabulartext{Decimal expansion of $f(n)$}\ }}  \\ 
   \cline{1-9}
   \rule{0em}{2.15ex}% bit more vertical room 
   0  &42  &3  &1  &2  &7  &7  &0  &4  \\  
   1  &2   &0  &1  &0  &0  &0  &0  &0  \\  
   2  &1   &4  &1  &4  &1  &5  &9  &2  \\  
   3  &-20  &9  &1  &9  &5  &9  &1  &9  \\  
   4  &  0 &1  &0  &1  &0  &0  &1  &0  \\
   5  &-0  &6   &2  &5  &5  &4  &1  &8  \\[-.5ex]
   \multicolumn{1}{c}{\vdotswithin{$0$}} 
    &\multicolumn{4}{c}{\vdotswithin{$1$}}  
  \end{array}
  \label{ex:DiagonalizationTable}
\end{equation*}}%
\only<2->{\begin{equation*} \setlength{\arraycolsep}{0.75\arraycolsep}
  \begin{array}{c|r@{\hspace*{.35em}.\hspace*{.35em}}ccccccc@{\hspace*{1em}\ldots\;}}
    \multicolumn{1}{c}{\text{\small $n$\hspace*{0.25em}}} 
    &\multicolumn{8}{c}{\text{\hspace*{-1.00em}\small\tabulartext{Decimal expansion of $f(n)$}\ }}  \\ 
   \cline{1-9}
   \rule{0em}{2.15ex}% bit more vertical room 
   0  &42  &\definend{3}  &1  &2  &7  &7  &0  &4  \\  
   1  &2   &0  &\definend{1}  &0  &0  &0  &0  &0  \\  
   2  &1   &4  &1  &\definend{4}  &1  &5  &9  &2  \\  
   3  &-20  &9  &1  &9  &\definend{5}  &9  &1  &9  \\  
   4  &  0 &1  &0  &1  &0  &\definend{0}  &1  &0  \\
   5  &-0  &6   &2  &5  &5  &4  &\definend{1}  &8  \\[-.5ex]
   \multicolumn{1}{c}{\vdotswithin{$0$}} 
    &\multicolumn{4}{c}{\vdotswithin{$1$}}  
  \end{array}
  \label{ex:DiagonalizationTable}
\end{equation*}}%
We will produce a number $z\in\R$ that does not equal 
any of the $f(n)$'s.

\pause
\ExecuteMetaData[\catchfilefn]{disc:EnumerateR}
\end{frame}
\begin{frame}
\ExecuteMetaData[\catchfilefn]{th:NoOntoMapNToR}

\ExecuteMetaData[\catchfilefn]{pf:NoOntoMapNToR}
\end{frame}


\begin{frame}
  \frametitle{Uncountable sets}

\ExecuteMetaData[\catchfilefn]{def:Uncountable}

\begin{example}
There are many uncountable sets.
We can adjust the argument from the prior slide 
to show that there is no onto function 
from $\N$ to $S$ where
$S=\set{x\in\R\suchthat x>5}$, or 
$S=\leftclosed{\sqrt{3}}{10}=\set{x\in\R\suchthat \sqrt{3}\leq x<10}$. 
\end{example}

% \pause
% \begin{example}
% For each of these, we can decide whether it is a countable or uncountable
% set.
% For the uncountable ones, we can just sketch the argument by showing
% an example table modeled on the prior slide, and a sample $z$.
% \begin{enumerate}
% \item $A=\set{1,3,4}$
% \item $B=\set{2, 5, 8, 11, \ldots}$
% \item $C=\set{-3, -5, -7, -9, \ldots}$
% \item the set of prime numbers, $P$
% \item $E=\set{x\in\R\suchthat x<0}$
% \item $F=\leftclosed{0}{1}=\set{x\in\R\suchthat 0\leq x <1}$
% \end{enumerate}
% \end{example}
\end{frame}

\begin{frame}
  \frametitle{Hierarchy of infinities}
\ExecuteMetaData[\catchfilefn]{def:CardLessThanOrEqual}
\begin{example}
For finite sets it just means that the number of elements in~$S$
is less than or equal to the number of elements in~$T$.
An example is $S=\set{10,11,12}$ and $T=\set{0,1,2,3}$.
The function $\map{f}{S}{T}$ given by 
$10\mapsto 0$, and $11\mapsto 1$, and $12\mapsto 2$
is one-to-one and
so $\sizeof{S}\leq\sizeof{T}$.
\end{example}

\begin{example}
Let $S=\set{0,2,4,\ldots{}}$ be the even numbers.
The inclusion map $\map{f}{S}{\N}$ given by $s\mapsto s$ is one-to-one.
So $\sizeof{S}\leq\sizeof{\N}$.
\end{example}

\begin{example}
Similarly, $|\N|\leq |\R|$ via the inclusion map $f(x)=x$.
Since $\R$ is uncountable, this less-than relation between
cardinalities is strict.
\end{example}

\begin{example}
Let 
$S=\leftclosed{0}{2}$ and $T=\leftclosed{10}{16}$
be intervals of real numbers.
The function $\map{f}{\R}{\R}$ given by $f(x)=3x+10$
is one-to-one, so $\sizeof{S}\leq\sizeof{T}$.

(Also one-to-one is the function $\map{g}{\R}{\R}$ given by $g(y)=(y-10)/6$
and so we also have $\sizeof{T}\leq\sizeof{S}$.
A result that we cover in the exercises 
says that if there is a one-to-one
function both ways then the two sets correspond, so
$\sizeof{T}=\sizeof{S}$.)
\end{example}
\end{frame}



\begin{frame}
  \frametitle{Cantor's Theorem}\vspace*{-1ex}
\ExecuteMetaData[\catchfilefn]{recall:CharFcn}

\ExecuteMetaData[\catchfilefn]{th:CantorsThm}

Before the proof, we first see an example of its harder half.
\end{frame}
\begin{frame}
\ExecuteMetaData[\catchfilefn]{ex:CantorsThm}
\end{frame}


\begin{frame}
% \ExecuteMetaData[\catchfilefn]{th:CantorsThm}
\ExecuteMetaData[\catchfilefn]{pf:CantorsThm}

Diagonalization depends on changing the entries.
Before we leave this argument 
note where the changing happens, where the bits get flipped, namely in 
the definition of~$R$.

% \begin{example}
% The set $S=\set{\str{a},\str{b}}$ has a smaller cardinality than does
% its power set.
% \begin{equation*}
%   \powerset(S)=\set{
%      \emptyset,\,
%      \set{\str{a}},\, \set{\str{b}},\,
%      \set{\str{a},\str{b}}
%      }
% \end{equation*}
% \end{example}
\begin{example}
The collection containing all subsets of~$\N$
\begin{equation*}
  \mathcal{C}=\set{\emptyset,\,\ldots{}\set{1,2,6},\,\ldots{}\set{0,1,4,9,\ldots{}},\,\ldots{}}
\end{equation*}
has a larger cardinality than does the set~$\N$. 
\end{example}

\end{frame}



\begin{frame}
  \frametitle{The functions not computed by any machine}

\ExecuteMetaData[\catchfilefn]{cor:FcnsNToNNotCountable}
\ExecuteMetaData[\catchfilefn]{pf:FcnsNToNNotCountable}

\pause
Turing machines compute functions from $\N$ to $\N$.
There are uncountably many such functions, but only countably many 
Turing machines.
Consequently, there are function that are not computed by any Turing machine.

This is like the child's game of Musical Chairs.
In the game there are more children than chairs 
and so some child is left without a chair.
Here, there are more functions from $\N$ to~$\N$
than there are Turing machines and similarly
there must be a function without a machine to compute it.

\pause
\textcolor{highlightcolor}{In the light of Church's Thesis we interpret 
this to say that there are jobs 
that no computer can do.}

We started the course by asking ``What can be done?''
We now have part of the answer:~not everything.
\end{frame}

% ====================================================
\section{Universality}

\begin{frame}
  \frametitle{Universal Turing machine}
\ExecuteMetaData[\catchfilefn]{th:UniversalTM}

This machine will fail to halt if $\TM_e$ fails to halt on~$x$
and otherwise will halt and yield the matching output.

\pause
This machine is in a sense an all-powerful Turing machine,~$\UTM$. 
It is a \definend{Universal Turing Machine}
in that this one device can be made to have any desired computable behavior.
So we don't need infinitely many different Turing machines, 
we just need this one.

\begin{itemize}
\item A universal machine $\UTM$ is like an interpreter, 
  or like an operating system. 
\item This converts from doing jobs in hardware to doing them in software.
\item The universal machine does not get as input a Turing machine, it gets
  the index of a Turing machine, which is computationally equivalent to
  a representation of that machine.
\item Yes, we could feed $\UTM$ its own index.
\end{itemize}
\end{frame}




\begin{frame}[fragile]
  \frametitle{Parametrization}
\definend{Partial evaluation} is the freezing of some of a machine's inputs.
An example is to 
start with this two-input function.

\begin{lstlisting}
(define (power base exponent) ; Def is a bit silly because 
  (expt base exponent))       ; expt does the same and is built in
\end{lstlisting}

Get a family of functions by  
\alert{parametrizing} the exponent.

\begin{lstlisting}
(define (identity_fcn base) 
  (power base 1))

(define (square_fcn base)
  (power base 2))

(define (cube_fcn base)
  (power base 3))
\end{lstlisting}


% \pause
% \begin{remark}
% Python has a more systematic way to accomplish the same thing.

% \begin{lstlisting}
% from functools import partial
% power_1 = partial(power, exponent=1)
% power_2 = partial(power, exponent=2)
% power_3 = partial(power, exponent=3)
% \end{lstlisting}
% \end{remark}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Freezing variables}

On the left is a three input program.
By Church's Thesis there is a Turing machine with this behavior;
let the index of that machine be~$e$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/background06.pdf}}
  \hspace{3em}
  \vcenteredhbox{\includegraphics{asy/background07.pdf}}
\end{center}
On the right we have frozen the first two inputs.
The \smn~Theorem, which we are about to see,
says that we can do this uniformly, that there is a program which takes in
$e$, $5$, and~$7$ and outputs the index of the Turing machine 
on the right. 

Further, the \smn~Theorem says that a single program works in all
freeze-two-inputs-and-leave-one-as-is situations.
We can write $s_{2,1}$ for the computable function that does this.
We usually just call it~$s$.
  
In short, where $e$ is the index of the 
  Turing machine outlined on the left,
  the index of the machine on the right is $s(e,5,7)$.
\end{frame}




\begin{frame}
    \frametitle{The \protect\smn~Theorem}

Universality says that there is a computable
function $\map{U}{\N^2}{\N}$ such that
$U(e,x)=\phi_e(x)$.
There, the letter~$e$ travels from the function's argument to an index.
In the next result we go from the argument to being part of the index.

\ExecuteMetaData[\catchfilefn]{th:smn}

\pause
\textsc{Pf.}
\ExecuteMetaData[\catchfilefn]{pf:smni}
\end{frame}


\begin{frame}
\begin{center}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts00.pdf}}
  \hspace{3em}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts01.pdf}}
\end{center}
\ExecuteMetaData[\catchfilefn]{pf:smnii}\qedsymbol
\end{frame}



\begin{frame}{The \smn~Theorem gives a uniform family of functions}
Recall again the \lstinline{power} routine.

% \begin{lstlisting}
% def power(base, exponent):
%     return base ** exponent  
% \end{lstlisting}

\begin{center}
  \vcenteredhbox{\includegraphics{asy/background08.pdf}}
\end{center}

Suppose that sketch describes the Turing machine with index~$e$.
Looking at the different machines indexed by $s(e,x)$ 
gives a family of routines parametrized by the exponent.

% \begin{lstlisting}
% def identity(base):
%     return power(base, 1)
% def square(base):
%     return power(base, 2)
% def cube(base):
%     return power(base, 3)
% \end{lstlisting}

\begin{center}
  \vcenteredhbox{\includegraphics{asy/background09.pdf}}
  \hspace{1em}
  \vcenteredhbox{\includegraphics{asy/background10.pdf}}
  \hspace{1em}
  \vcenteredhbox{\includegraphics{asy/background11.pdf}}
  \hspace{1em}
  \ldots
  \hspace{1em}
  \vcenteredhbox{\includegraphics{asy/background12.pdf}}
  \hspace{1em}
  $\ldots$
\end{center}
\end{frame}




% ====================================================
\section{Unsolvability}


\begin{frame}
  \frametitle{The \HP: motivation}
We have shown that there are some functions that are not mechanically
computable.
But to do this we used Cantor's Theorem that 
says the function exists but doesn't construct it. 
In this subject we prefer to construct things.
So we will now go through the proof of Cantor's Theorem and effectivize it.

\pause
On the left is the table from the discussion leading to Cantor's Theorem.
\begin{center}
  \ExecuteMetaData[\catchfilefn]{table:CantorThmEffectivization}
  \hspace*{2em plus 0.75fil}
  \vcenteredhbox{\includegraphics{../../background/asy/hp/hp02.pdf}}
\end{center}
Consider the diagonalization flowchart.
All the boxes except the middle one are trivial.
For the middle, use universality to simulate Turing machine~$e$ 
running input~$e$.

\pause
We seem to have produced 
a program whose output isn't on the list
of all program outputs.
That's obviously impossible.
Where is the flaw in the reasoning?
\end{frame}


\begin{frame}
  \frametitle{The \HP: definition}

\ExecuteMetaData[\catchfilefn]{prob:HP}

\ExecuteMetaData[\catchfilefn]{def:K}

\pause
\ExecuteMetaData[\catchfilefn]{th:HPIsUnsolvable}

\textsc{Pf.} \ExecuteMetaData[\catchfilefn]{pf:HPIsUnsolvablei}
\begin{center}
\begin{minipage}{0.3\textwidth}
  \begin{equation*}
  f(e)=
    \begin{cases}
    42    &\case{if $\TMfcn_e(e)\diverges$}  \\
    \uparrow  &\case{if $\TMfcn_e(e)\converges$}
    \end{cases}
  \end{equation*}
\end{minipage}
\hspace{2.5em}
\vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts02.pdf}}
\end{center}
\end{frame}



\begin{frame}
\ExecuteMetaData[\catchfilefn]{pf:HPIsUnsolvableii}\qedsymbol
\end{frame}



\begin{frame}
  \frametitle{Showing other problems are unsolvable by relating them to the \HP{}}

\begin{example}
Consider this problem: given~$i\in\N$, determine whether Turing machine~$i$
halts on input~$1$.
We are asking whether this function is computable.
\begin{equation*}
  \computablefunction{halts\_on\_one\_decider}(i)=
  \begin{cases}
    1  &\case{if $\TMfcn_i(1)\converges$}  \\
    0  &\case{otherwise}
  \end{cases}
\end{equation*}
 We will show that if it were
mechanically computable then we could solve the \HP.

\pause
Consider the machines sketched here.
\begin{center}
  \includegraphics{\asydir hp/hp06.pdf}
  \hspace*{4em}
  \includegraphics{\asydir hp/hp07.pdf}
\end{center}
On the right, $x$ is hard-coded.
That machine halts on any input if and only if it gets through the middle box
(the output $42$ is not important; we just like to have both input and
output).
In particular, it halts on input~$y=1$ if and only if 
$\TMfcn_x(x)\converges$.
\end{example}
\end{frame}

\begin{frame}
So we can turn the `does $\TMfcn_x(x)\converges$?' questions
into questions that $\computablefunction{halts\_on\_one\_decider}$
can answer.
The proof involves recognizing that we can switch uniformly to questions 
of the second kind.

For that proof, here are the flowcharts again.
On the left is a sketch of a single machine.
We can write it as a program, using universality for the third box, so by 
Church's Thesis there is a Turing machine with this behavior.
Let that machine's index be~$e_0$.
\begin{center}
  \includegraphics{\asydir hp/hp06.pdf}
  \hspace*{4em}
  \includegraphics{\asydir hp/hp07.pdf}
\end{center}
Then the \smn~Theorem gives what's on the right, 
the family of machines, $\TM_{s(e_0,x)}$.
We have already observed that $\TMfcn_x(x)\converges$ if and only if 
$\computablefunction{halts\_on\_one\_decider}(s(e_0,x))=1$.
So if we could mechanically check whether a machine halts on~$1$
then we could mechanically solve the \HP.
We can't solve the \HP, so $\computablefunction{halts\_on\_one\_decider}$
is not a mechanically computable function. 
\end{frame}

\begin{frame}{Similar examples}
Each of these is unsolvable.
For each, show that by using the \HP{}.
\begin{enumerate}
\item The problem of determining whether a given Turing machine ever
  outputs a 19.
  \begin{equation*}
    \computablefunction{outputs\_nineteen\_decider}(i)=
    \begin{cases}
      1  &\case{if there is $n$ such that $\TMfcn_i(n)=19$}  \\
      0  &\case{otherwise}
    \end{cases}
  \end{equation*}
\item The problem of determining whether a given Turing machine 
  gives as output the square of its input.
  \begin{equation*}
    \computablefunction{square\_decider}(i)=
    \begin{cases}
      1  &\case{if $\TMfcn_i(n)=n^2$ for all n}  \\
      0  &\case{otherwise}
    \end{cases}
  \end{equation*}
\end{enumerate}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Discussion: the \HP{} is unsolvable}

\begin{itemize}
\item
Inherent in the nature of mechanical computation, necessary to avoid
contradiction, is that some computations fail to halt.

\pause
\item ``Unsolvable'' means unsolvable by a Turing machine.
  Below is a perfectly good function that solves the \HP{} 
  but it isn't mechanically computable\Dash no 
  Turing machine has this input/output behavior.
  \begin{equation*}
    \computablefunction{halt\_decider}(e)=
    \begin{cases}
      1  &\case{if $\TMfcn_e(e)\converges$}  \\
      0  &\case{if $\TMfcn_e(e)\diverges$}  \\
    \end{cases}
  \end{equation*}

\pause
\item
Unsolvability of the \HP{} 
does not mean that for no program can we tell if that program halts. 
The program on the left below halts, for all inputs.
Nor does it mean that for no program can we tell if the program does not halt.
The program on the right does not halt, for all inputs.
\begin{center}
% \vspace*{-4ex}
\vspace*{-\ht\strutbox}
\begin{minipage}[t]{0.4\linewidth}%
\begin{lstlisting}
(define (print-zero)
  (read)
  (display 0))
\end{lstlisting}%
\end{minipage}%
\hspace*{2em plus 0.5fil}
\begin{minipage}[t]{0.4\linewidth}%
\begin{lstlisting}
(define (unbounded-growth)
  (define (grow x)
    (grow (+ 1 x)))
  
  (read)  ; wait for user input
  (grow 0))
\end{lstlisting}%
\end{minipage}%
\end{center}
Instead, the unsolvability of the Halting Problem 
says that there is no single program that, for all input~$e$, 
correctly computes in a finite time whether $\TM_e$ halts on~$e$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Discussion continued}
\begin{itemize}
\item Unsolvability of the Halting Problem 
  says that there is no single program that, for all~$e$, correctly computes 
  in a finite time whether $\TM_e$ halts on input~$e$.
  The ``finite time'' qualifier is there because we could think 
  to use a universal
  Turing machine to simulate $\TM_e$ on~$e$, but if that machine failed to halt
  then we would not find out in a finite time.
 
  The ``single program'' qualifier is a little subtler.
  For any index~$e$,
  either $\TM_e$ halts on~$e$ or it does not.
  That is, for any~$e$ one of these two programs
  gives the right answer.
\begin{center}
% \vspace*{-4ex}
\vspace*{-\ht\strutbox}
\begin{minipage}[t]{0.4\linewidth}%
\begin{lstlisting}
(define (print-no)
  (display 0))
\end{lstlisting}%
\end{minipage}%
\hspace*{2em plus 0.5fil}
\begin{minipage}[t]{0.4\linewidth}%
\begin{lstlisting}
(define (print-yes)
  (display 1))
\end{lstlisting}%
\end{minipage}%
\end{center}

Thus, the unsolvability of the Halting Problem is about 
the non-existence of a single program that works across all
indices.
It is about uniformity,
or rather, the impossibility of uniformity.
\end{itemize}
\end{frame}




% ================================================
\section{Rice's Theorem}
\begin{frame}
  \frametitle{Rice's Theorem}
\ExecuteMetaData[\catchfilefn]{def:SameBehavior}
\ExecuteMetaData[\catchfilefn]{def:IndexSet}

\begin{example}
These are index sets:
$\set{e\in\N\suchthat
      \text{$\phi_e(x)=9$}}$,
and $\set{e\in\N\suchthat
      \text{$\phi_e(x)=2x$ or $\phi_e(x)=x+6$}}$,
and (3)~$\set{e\in\N\suchthat
      \text{$\phi_e(1)\converges$}}$.
\end{example}
\ExecuteMetaData[\catchfilefn]{th:RicesTheorem}

\end{frame}
\begin{frame}
\textsc{Pf.}
\ExecuteMetaData[\catchfilefn]{pf:RicesTheoremi}
\begin{center}
  \vcenteredhbox{\includegraphics{../../background/asy/hp/hp12.pdf}}
  \hspace{2em plus 0.5fil}
  \vcenteredhbox{\includegraphics{../../background/asy/hp/hp13.pdf}}
\end{center}
\ExecuteMetaData[\catchfilefn]{pf:RicesTheoremii}\qedsymbol
\end{frame}

\begin{frame}{Similar examples}
Show each of these is unsolvable using Rice's Theorem.

\begin{enumerate}
\item The problem of determining whether a given Turing machine
  halts on input~$1$.
  \begin{equation*}
    \mathcal{I}_0=\set{i\suchthat\text{$\TM_i$ halts on input~$1$}}
  \end{equation*}
\item The problem of determining whether a given Turing machine ever
  outputs a $19$.
  \begin{equation*}
    \mathcal{I}_1=\set{i\suchthat\text{there is an~$x$ so that $\TM_i$ halts on $x$ and outputs~$19$}}
  \end{equation*}
\item The problem of determining whether a given Turing machine 
  gives as output the square of its input.
\end{enumerate}
\end{frame}








% ====================================================
\section{Computably enumerable sets}

\begin{frame}[fragile]
\ExecuteMetaData[\catchfilefn]{def:ComputableSet}
\ExecuteMetaData[\catchfilefn]{def:ComputablyEnumerable}

The `enumerable' comes from the 
image of a computable function generating the set 
elements as a stream of numbers.
\begin{equation*}
\TMfcn_e(0),\,\TMfcn_e(1),\,\TMfcn_e(2),\, \ldots{}
\end{equation*}

\begin{example}
Some computably enumerable sets are
$\set{1,3,5}$,
and $\set{2n\suchthat n\in\N}$,
and $\set{y\suchthat\text{there is an $x$ so that $y=\TMfcn_{19}(x)$}}$,
and $\set{x\suchthat\TMfcn_{10}(x)\converges}$.
\end{example}
% \end{frame}

% \begin{frame}
\ExecuteMetaData[\catchfilefn]{description:DecidableAndSemidecidable}
\end{frame}

\begin{frame}[fragile]
\ExecuteMetaData[\catchfilefn]{lem:CEIffDomainOfComputableFcn}
\ExecuteMetaData[\catchfilefn]{def:WSubE}

\pause
\begin{lemma}
\ExecuteMetaData[\catchfilefn]{lem:ComputableAndCEi}
\ExecuteMetaData[\catchfilefn]{lem:ComputableAndCEii}
\end{lemma}

\ExecuteMetaData[\catchfilefn]{lem:HPIsCE}

\pause
Part of our interest in these sets is philosophical. 
By Church's Thesis we can think that, in a sense, 
the collection of computable sets consists of the only sets that we will
ever fully know.
With that thinking, sets that are semidecidable but not decidable 
are at the limits of our knowledge,  where 
we can determine membership but not nonmembership.
\end{frame}






% =====================================================
\section{Fixed point theorem}

\begin{frame}
  \frametitle{Sequences of computable functions}

So far we have studied individual computable functions~$\TMfcn$.
We next consider sequences of them.
\begin{equation*}
  \TMfcn_{i_0}, \,
  \TMfcn_{i_1}, \,
  \TMfcn_{i_2}, \,
  \TMfcn_{i_3}, \, \ldots
\end{equation*}

There, the indices seem to be any old sequence of numbers.
But in this subject it is most natural for this
sequence to be effectively computed, 
for the sequence 
to be the output of some computable function,
$i_0=\TMfcn_e(0), i_1=\TMfcn_e(1), \ldots$\sentencespace
That gives us this sequence, for some~$e$.
\begin{equation*}
  \TMfcn_{\TMfcn_e(0)}, \,
  \TMfcn_{\TMfcn_e(1)}, \,
  \TMfcn_{\TMfcn_e(2)}, \,
  \TMfcn_{\TMfcn_e(3)}, \, \ldots
\end{equation*}
To compute $\TMfcn_{\TMfcn_e(i)}(x)$, first compute 
$\TMfcn_e(i)=w$ and 
then use that value~$w$ to compute $\TMfcn_w(x)$.  
Thus, if $\TMfcn_e(i)$ diverges then $\TMfcn_{\TMfcn_e(i)}(x)$ also diverges.

\begin{example}
The squaring function $\TMfcn_{e_0}(x)=x^2$ gives this sequence
of computable functions
\begin{equation*}
  \TMfcn_{0}, \,
  \TMfcn_{1}, \,
  \TMfcn_{4}, \,
  \TMfcn_{9}, \, \ldots
\end{equation*}
and the doubler function $\TMfcn_{e_1}(x)=2x$ gives this sequence.
\begin{equation*}
  \TMfcn_{0}, \,
  \TMfcn_{2}, \,
  \TMfcn_{4}, \,
  \TMfcn_{6}, \, \ldots
\end{equation*}
\end{example}
\end{frame}
\begin{frame}
There are countably many computable functions $\TMfcn_e$ so 
this table that lists every computable sequence 
$\sequence{\,\TMfcn_{\TMfcn_e(n)}\suchthat n\in\N\,}$
of computable functions.

\ExecuteMetaData[\catchfilefn]{discussion:WhenDiagonalizationFailsiii}

What happens when we diagonalize?
\end{frame}

\begin{frame}
  \frametitle{When diagonalization fails}\vspace*{-1.5ex}
Recall the proof that the
set of real numbers is not countable.
\ExecuteMetaData[\catchfilefn]{discussion:RecallCantorThm}

\pause
\ExecuteMetaData[\catchfilefn]{discussion:WhenDiagonalizationFails}
\end{frame}

\begin{frame}
We can apply that insight to the table of all computable sequences
of computable functions.

\ExecuteMetaData[\catchfilefn]{discussion:WhenDiagonalizationFailsiii}
\ExecuteMetaData[\catchfilefn]{discussion:WhenDiagonalizationFailsv}
\end{frame}


\begin{frame}
\begin{theorem}
\ExecuteMetaData[\catchfilefn]{th:FixedPointThm}
\end{theorem}

\pause
\textsc{Pf.}
\ExecuteMetaData[\catchfilefn]{pf:FixedPointThmi}
\begin{equation*}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts06.pdf}}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts07.pdf}}
  \hspace{2em}
  d_n(x)=
  \begin{cases}
    \TMfcn_{\TMfcn_n(n)}(x)  &\case{if $\TMfcn_n(n)\converges$} \\
    \uparrow                &\case{otherwise}
  \end{cases}
\end{equation*}
\end{frame}



\begin{frame}
\ExecuteMetaData[\catchfilefn]{pf:FixedPointThmii}
\begin{equation*}
  t_f(d_n)\;(x)=
  \begin{cases}
    \TMfcn_{f\TMfcn_n(n)}(x)   &\case{if $\TMfcn_n(n)\converges$}     \\
    \uparrow                 &\case{otherwise}
  \end{cases}
  \hspace{2em}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts08.pdf}}
\end{equation*}%
\ExecuteMetaData[\catchfilefn]{pf:FixedPointThmiii}\qedsymbol
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{cor:SuccessorFcnSame}

This does not say that $\TM_e=\TM_{e+1}$.
It says that the two Turing machines have the same input/output behavior,
that $\TMfcn_e=\TMfcn_{e+1}$.

\pause\medskip
The Fixed Point theorem is very strange.
Our numbering of Turing machines does not seem to have anything to do with
the behavior of the machines.
Nonetheless, under the mild assumption that our numbering 
is acceptable there
must be adjacent machines with the same behavior. 
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{cor:WeEqualsSete}

\textsc{Pf.}
\ExecuteMetaData[\catchfilefn]{pf:WeEqualsSetei}
\begin{equation*}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts09.pdf}}
  \hspace{1em}
  \vcenteredhbox{\includegraphics{../../background/asy/flowcharts/flowcharts10.pdf}}
  \hspace{1em}
  \TMfcn_{s(e_o,m)}(x)=
  \begin{cases}
  42         &\case{if $x=m$}  \\
  \uparrow  &\case{otherwise}
  \end{cases}
\end{equation*}%

\ExecuteMetaData[\catchfilefn]{pf:WeEqualsSeteii}\qedsymbol

\pause
Again, this interaction between numbering and behavior
is very strange. 
It is also quite interesting.
Think of the index number as the computable function's name, so that
this is an interaction between its name and how it acts.
In particular, 
the machine $\TM_e$ of this example halts only on its name,~$e$.
\end{frame}

\end{document}




