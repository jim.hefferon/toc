\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

\usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 
\definecolor{alertcolor}{RGB}{203, 0 , 34}
\setbeamercolor{alerted text}{fg=alertcolor}

\usepackage{../presentation,../presentationfonts}
\usepackage{../../grammar}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Languages, Grammars, and Graphs}

\author{Jim Hef{}feron}
\institute{
  University of Vermont\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Languages}

\usepackage{catchfilebetweentags} % read text from background.tex
\newcommand{\catchfilefn}{../../languages/languages.tex}
% Keep from swallowing end-of-lines
%   https://tex.stackexchange.com/a/40704/121234
\usepackage{etoolbox}
\makeatletter
\patchcmd{\CatchFBT@Fin@l}{\endlinechar\m@ne}{}
  {}{\typeout{Unsuccessful patch!}}
\makeatother

\usepackage{xr}
  \externaldocument{../../book}
  \externaldocument{../../languages/languages}
\usepackage{cleveref}

% Directory of this chapter's asy material
\newcommand{\asydir}{../../languages/asy/}
% \newcommand{\tmexercises}{\asydir tm_exercises/pdfs/}
% Directory of the src/asy material
\newcommand{\generalasydir}{../../asy/}

% \usepackage{multirow,bigstrut}
\begin{document}
\begin{frame}
  \titlepage
\end{frame}



\renewcommand{\catchfilefn}{../../appendix/strings.tex}

% \section{This chapter}
% \begin{frame}

% This chapter contains material that may be new to some students, and may
% be a review for others.
% \end{frame}


\section{Languages}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Strings}
Recall that a \definend{symbol}, or \definend{token}, 
is something that a machine can read and write,
and an \definend{alphabet} is a nonempty and finite set of symbols.

\ExecuteMetaData[\catchfilefn]{def:String}

We write symbols in a distinct typeface, as in \str{1} or \str{a},
because the alternative of quoting them would be 
clunky.

\ExecuteMetaData[\catchfilefn]{discussion:AlphSymbol}%
\ExecuteMetaData[\catchfilefn]{def:StringLength}%
\ExecuteMetaData[\catchfilefn]{dis:OmitCommas}

\begin{example}
Natural numbers are represented by strings of digits.
The alphabet is $\Sigma=\set{\str{0},\ldots \str{9}}$.
The string $\sigma=\str{1066}$ has length~$|\sigma|=4$. 
\end{example}

\ExecuteMetaData[\catchfilefn]{def:Bitstrings}
\end{frame}



\begin{frame}[fragile]
  \frametitle{String operations}
\ExecuteMetaData[\catchfilefn]{def:StringOps}

\ExecuteMetaData[\catchfilefn]{def:Palindrome}

\ExecuteMetaData[\catchfilefn]{def:KleeneStar}

\begin{example}
The set of bitstrings of length~$3$ is
$\B^3=\set{\str{000},\str{001}, \ldots \str{111}}$.
The set of all bitstrings is 
$\kleenestar{\B}=\set{\emptystring,\str{0},\str{1},\str{00},\ldots}$.
\end{example}
\end{frame}


% Switch from taking things from the appendix to the Languages chapter
\renewcommand{\catchfilefn}{../../languages/languages.tex}


\begin{frame}
  \frametitle{Languages}
\ExecuteMetaData[\catchfilefn]{def:Lang}

\begin{example}
Let $\Sigma=\set{\str{a},\str{b}}$.
One language is
$\lang_0=\set{\sigma\in\kleenestar{\Sigma}\suchthat\text{$\sigma=\str{a}^n\str{b}\str{a}^n$ for some $n\in\N$}}$; some members are 
\str{b}, \str{aba}, \str{aabaa}, and \str{aaabaaa}.
Another is the language of \definend{palindromes},
$\lang_1=\set{\sigma\in\kleenestar{\Sigma}\suchthat \sigma=\reversal{\sigma}}$.
\end{example}

\begin{example}
For the alphabet of parentheses, $\Sigma=\set{\str{)}, \str{(}}$,
the set of strings of balanced 
parentheses is a language.
Three members are the strings \str{(())}, \str{(()())}, and \str{(()(()))}.
Nonmembers are the strings \str{)(}, \str{(()}, and \str{())(()}. 
\end{example}

\ExecuteMetaData[\catchfilefn]{def:LangOps}

% \ExecuteMetaData[\catchfilefn]{def:class}
\end{frame}


\begin{frame}{Deciding a language vs recognizing a language}
\ExecuteMetaData[\catchfilefn]{discussion:DecidingVsRecognizing}

\begin{example}
A Turing machine can decide the language of perfect squares,
$S=\set{\str{1}^{(n^2)}\suchthat n\in\N}$.
(Technically, it decides the language of strings representing
squares, but we will not stick to the distinction.)
\end{example}

\begin{example}
Consider the set~$S$ of indices~$e$
such that $\TM_e$ halts on input~$3$.
A Turing machine can recognize~$S$ just by running the $\TM_e$ on input~$3$
and waiting for it to halt.
But no Turing machine decides that set, because
we have shown that $K$ reduces to~$S$ and so if we had a Turing machine
to decide membership in~$S$ then we could use it to decide membership in~$K$.)
\end{example}
\end{frame}




% ==============================================
\section{Grammars}
\begin{frame}{Introduction}
The definition of language allows $\lang$ to be 
any subset of $\kleenestar{\Sigma}$ at all.
But languages in practice usually are constructed according to patterns, 
to lists of rules.

An example showing that there are rules is that,
``When nine hundred years old you reach, look as good you will not''
may fit the rules wherever Yoda was raised, but it does not fit those for
English.
Another example is that the C~language consists of programs that follow
rules decreed by the standardization committee.

\pause
This is a subset of the rules for English:
\begin{itemize}
\ExecuteMetaData[\catchfilefn]{ex:RulesForEnglish} 
\end{itemize}
\end{frame}


\begin{frame}
We will write the above rules in this form.
\ExecuteMetaData[\catchfilefn]{ex:GrammarRulesForEnglish} 

\ExecuteMetaData[\catchfilefn]{discussion:GrammarVocabulary} 
\end{frame}



\begin{frame}
  \frametitle{Derivation}
\ExecuteMetaData[\catchfilefn]{discussion:DerivationVocabularyi} 
\end{frame}
\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:DerivationVocabularyii} 
\end{frame}


\begin{frame}
  \frametitle{Derivation tree}
\ExecuteMetaData[\catchfilefn]{discussion:DerivationTree} 
\begin{center}
  \includegraphics{\asydir parsetree/parsetree000.pdf}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Grammar}

In this course we will use the definition of \alert{grammar} below.
It is not the most general definition possible; 
for instance, we could allow the head to have more than just a single 
nonterminal.
Our sticking to this more restrictive form is nonstandard so that 
you may well see other authors using other definitions, 
but this is the most general form that we shall need.

\ExecuteMetaData[\catchfilefn]{def:Grammar} 

\begin{example}
Let the alphabet be $\Sigma=\set{\trm{a},\trm{b}}$,
let the set of nonterminals be $N=\set{\ntrm{start},\ntrm{left},\ntrm{right}}$.
Here are five production rules making the set~$P$.
\begin{grammar}
\ntrm{start} \produces \trm{a}\ntrm{left}

\ntrm{left} \produces \trm{a}\ntrm{left} 
  \syntaxor \trm{b}\ntrm{right}

\ntrm{right} \produces \trm{b}\ntrm{right} \syntaxor \emptystring
\end{grammar}
\end{example}

As to the start symbol, we take the convention that it 
is the head of the first rule.
Here is a derivation using the production rules.
\begin{center}
\ntrm{start}\derives \trm{a}\ntrm{left} \derives \trm{aa}\ntrm{left}
  \derives \trm{aab}\ntrm{right} \derives \trm{aabb}\ntrm{right} 
  \derives \trm{aabb}\emptystring = \trm{aabb}
\end{center}
\end{frame}

\begin{frame}
\begin{example}
This is part of the grammar of C.

\begin{grammar}
\ntrm{statement\_list}
	\produces{} \ntrm{statement}
	\syntaxor \ntrm{statement\_list} \ntrm{statement}

\ntrm{expression\_statement}
	\produces{} \trm{;}
	\syntaxor \ntrm{expression} \ntrm{;}

\ntrm{selection\_statement}
	\produces \trm{IF} \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}
	\othersyntax \trm{IF} \trm{(} \ntrm{expression} \trm{)} \ntrm{statement} \trm{ELSE} \ntrm{statement}
	\othersyntax \trm{SWITCH} \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}

\ntrm{iteration\_statement}
	\produces \trm{WHILE} \trm{(} \ntrm{expression} \trm{)} \ntrm{statement}
	\othersyntax \trm{DO} \ntrm{statement} \trm{WHILE} \trm{(} \ntrm{expression} \trm{)} \trm{;}
	\othersyntax \trm{FOR} \trm{(} \ntrm{expression\_statement} \ntrm{expression\_statement} \trm{)} \ntrm{statement}
	\othersyntax \trm{FOR} \trm{(} \ntrm{expression\_statement} \ntrm{expression\_statement} \ntrm{expression} \trm{)} \ntrm{statement}
\end{grammar}
(Here the capitalized terminals
mean any string that capitalizes to that, so that \trm{IF} means
\trm{If}, or \trm{iF}, or \trm{if}, or \trm{IF}.)
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Recursive grammars}

The prior two example grammars are recursive. 
For instance in this rule,
\begin{grammar}
\ntrm{left} \produces \trm{a}\ntrm{left} 
  \syntaxor \trm{b}\ntrm{right}
\end{grammar}
\ntrm{left} can expand to
an expression involving \ntrm{left}.
Similarly, in the C grammar \ntrm{statement\_list}'s expansion 
involves itself.
\begin{grammar}
\ntrm{statement\_list}
	\produces{} \ntrm{statement}
	\syntaxor \ntrm{statement\_list} \ntrm{statement}
\end{grammar}
But we don't get stuck in an infinite regress. 
The question is not whether you could perversely 
keep expanding \ntrm{left} or \ntrm{statement\_list} forever.
Instead, you are given a string such as \trm{aabb} and the question is 
whether you can find a terminating derivation.
\end{frame}


\begin{frame}
  \frametitle{An abbreviation convention}

For examples and exercises, often for nonterminals we use uppercase letters
and for terminals we use other characters 
such as lowercase letters or digits.

\begin{example}
This grammar has two nonterminals, S and~I.
Its terminals are the parentheses, \trm{(} and~\trm{)},
the arithmetic operators, \trm{+} and~\trm{*}, 
and the digits.
\begin{grammar}
S \produces \trm{(} S \trm{)} \syntaxor S \trm{+} S \syntaxor S \trm{*} S \syntaxor I

I \produces{} \trm{0} \syntaxor \trm{1} \syntaxor \ldots{} \trm{9}
\end{grammar}
The derived strings are arithmetic expressions
such as \str{1+2*(3+4)}.
\end{example}
\end{frame}

\begin{frame}
  \frametitle{Language of a grammar}
\ExecuteMetaData[\catchfilefn]{def:LanguageOfGrammar} 

\begin{example}
This grammar~$G$
\begin{grammar}
S \produces AB

A \produces aA \syntaxor a

B\produces bB \syntaxor b
\end{grammar}
lets you derive strings consisting of some \str{a}'s followed by some \str{b}'s.
\begin{equation*}
\lang(G)=
\set{\str{a}^n\str{b}^m\suchthat n,m\in\N^+}
\end{equation*}
\end{example}

\begin{example}
This grammar $\hat{G}$
\begin{grammar}
S \produces AB

A \produces a

B\produces bbB \syntaxor \emptystring
\end{grammar}
lets you derive strings consisting of a single \str{a},
and then an even number of \str{b}'s,
including possibly zero many \str{b}'s.
\begin{equation*}
\lang(\hat{G})=
\set{\str{a}\str{b}^{2m}\suchthat m\in\N}
\end{equation*}
\end{example}
\end{frame}


\begin{frame}
  \frametitle{This is a kind of computation}
Derivations involve pushing symbols around according to formal rules.
So it fits with our idea of a mechanical computation.
 
For instance, this grammar
\begin{grammar}
S \produces \str{11}S \syntaxor \emptystring
\end{grammar}
produces the language $\set{\str{1}^{2n}\suchthat n\in\N}$
of even numbers in unary notation.

In a similar way we can compute such things as the set of perfect squares.
(As we remarked earlier, there are grammar types that 
are more general than the ones we've defined.
With more general grammars we can compute more sets.
An example is that the set of primes requires such a grammar.
But we won't compute these; having made the point that
derivation is a form of computation we will stop.) 
\end{frame}



\begin{frame}[fragile]
  \frametitle{A grammar can be ambiguious}
% \ExecuteMetaData[\catchfilefn]{rem:AmbiguousGrammar} 
This bit of C-like code shows nested
\codeinline{if} statements. 
\begin{lstlisting}
  if ... then ... if ... then ... else ...
\end{lstlisting}
The \codeinline{else} could be associated
with either the first \codeinline{if} or the second.
\begin{center}
  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}
 if ... 
 then ...
     if ...
     then ... 
 else ...
\end{lstlisting}
  \end{minipage}
  \quad
  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}
 if ... 
 then ...
     if ...
     then ... 
     else ...
\end{lstlisting}
  \end{minipage}
\end{center}
This is a \definend{dangling else}.
An \definend{ambiguous grammar}
is one where some strings
have two different leftmost derivations.
The example below illustrates. 
\end{frame}
\begin{frame}
This grammar
\begin{grammar}
\ntrm{expr} \produces{} \ntrm{expr} \trm{+} \ntrm{expr}
        \othersyntax  \ntrm{expr} \trm{*} \ntrm{expr}
        \othersyntax  \trm{(} \ntrm{expr} \trm{)} \
               \syntaxor  \trm{0}  \syntaxor  \trm{1}  \syntaxor \ldots{} 
               \trm{9}
\end{grammar}
is ambiguous because \str{5+2*3} has two
leftmost derivations.
\begin{center}
\hspace{1em}\begin{tabular}{l}
  \ntrm{expr}\derives\ntrm{expr}\trm{+}\ntrm{expr}
  \derives\trm{5} \trm{+} \ntrm{expr}                 \\ \quad
  \derives\trm{5} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}          
  \derives\trm{5} \trm{+} \trm{2} \trm{*} \ntrm{expr}          
  \derives\trm{5} \trm{+} \trm{2} \trm{*} \trm{3}          
\end{tabular}        \\[1ex]
\hspace{1em}\begin{tabular}{l}
  \ntrm{expr}\derives\ntrm{expr} \trm{*} \ntrm{expr}
  \derives\ntrm{expr} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}    \\ \quad 
  \derives\trm{5} \trm{+} \ntrm{expr} \trm{*} \ntrm{expr}  
  \derives\trm{5} \trm{+} \trm{2} \trm{*} \ntrm{expr}  
  \derives\trm{5} \trm{+} \trm{2} \trm{*} \trm{3}          
\end{tabular}
\end{center}
Here are the two associated parse trees.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/grammar000.pdf}}
  \hspace{4em}
  \vcenteredhbox{\includegraphics{asy/grammar001.pdf}}
\end{center}
The left one evaluates to $5+6=11$,
while the right one gives $7\cdot 3=21$.
\end{frame}





% =================================================================
\section{BNF}
\begin{frame}[fragile]{Backus-Naur notation}
Many programming languages, protocols, or formats are specified
using Backus-Naur form.
Every rule has the structure:
\mbox{\ntrm{name} \bnfproduces \ntrm{expansion}}.

For instance, this is part of a BNF grammar of arithmetic expressions.
% \begin{grammar}
% \ntrm{expr} \bnfproduces \ntrm{term} \trm{+} \ntrm{expr}
%          \othersyntax  \ntrm{term}

% \ntrm{term} \bnfproduces \ntrm{factor} \trm{*} \ntrm{term}
%          \othersyntax  \ntrm{factor}

% \ntrm{factor} \bnfproduces \trm{(} \ntrm{expr} \trm{)}
%            \othersyntax  \ntrm{const}

% \ntrm{const} \bnfproduces \trm{0} \syntaxor \trm{1} \syntaxor \ldots{} \trm{9} \syntaxor \ntrm{constant}\ntrm{constant}
% \end{grammar}

% \pause
% In a specification it may look like this.
\begin{lstlisting}
<expr> ::= <term> "+" <expr>
         |  <term>

<term> ::= <factor> "*" <term>
         |  <factor>

<factor> ::= "(" <expr> ")"
           |  <const>

<const> ::= integer
\end{lstlisting}

As a first point, it is a more type-able version of this.
\begin{grammar}
\ntrm{expr} \produces \ntrm{term} \trm{+} \ntrm{expr}
  \syntaxor \ntrm{term}

\ntrm{term} \produces \ntrm{factor} \trm{*} \ntrm{term}
  \syntaxor \ntrm{factor}

\ntrm{factor} \produces \trm{(} \ntrm{expr} \trm{)} 
  \syntaxor \ntrm{const}

\ntrm{const} \produces \ntrm{integer} 
\end{grammar}
But there are also a number of extensions.
\end{frame}

\begin{frame}{Common extensions to BNF}
\begin{description}[xxx]
\item[Repetition] To show that an element may be repeated zero or more times,
use curly braces.
An example is that this describes a comma-separated argument list.
\begin{grammar}
\ntrm{args} \bnfproduces \ntrm{arg} \{ \trm{,} \ntrm{arg} \trm{} \}
\end{grammar}
To denote zero or more times you may also see a Kleene star,~`$*$'. 
\begin{grammar}
\ntrm{trailing-space} \bnfproduces \ntrm{whitespace}*
\end{grammar}
To denote one or more times you may see a plus,~`$+$'.
\begin{grammar}
\ntrm{natural} \bnfproduces \ntrm{digit}+
\end{grammar}

For zero or one repetitions, you may see square brackets.
\begin{grammar}
\ntrm{int} \bnfproduces [ \trm{+} \syntaxor \trm{-} ] \ntrm{natural}
\end{grammar}

\pause\item[Grouping] Use parentheses. 
This shows the form of addition or subtraction.
\begin{grammar}
\ntrm{expr} \bnfproduces \ntrm{term} ( \trm{+} | \trm{-} ) \ntrm{expr}
\end{grammar}
The vertical bar for `or' and 
the parentheses are metacharacters.

\pause\item[Concatenation]
Juxtaposition is invisible so occasionally you see a comma~`\trm{,}' 
to denote concatenation.
\end{description}
\end{frame}


\begin{frame}[fragile]
Here is part of a grammar
taken from RFC~5322.
Note the quoted terminals.
\begin{lstlisting}
date-time       =   [ day-of-week "," ] date time [CFWS]

day-of-week     =   ([FWS] day-name) / obs-day-of-week

day-name        =   "Mon" / "Tue" / "Wed" / "Thu" /
              "Fri" / "Sat" / "Sun"

date            =   day month year

day             =   ([FWS] 1*2DIGIT FWS) / obs-day

month           =   "Jan" / "Feb" / "Mar" / "Apr" /
         "May" / "Jun" / "Jul" / "Aug" /
         "Sep" / "Oct" / "Nov" / "Dec"

year            =   (FWS 4*DIGIT FWS) / obs-year
 
time            =   time-of-day zone

time-of-day     =   hour ":" minute [ ":" second ]

hour            =   2DIGIT / obs-hour

minute          =   2DIGIT / obs-minute

second          =   2DIGIT / obs-second

zone            =   (FWS ( "+" / "-" ) 4DIGIT) / obs-zone
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Python's floating point}
This is the grammar that the Python documentation uses
for floating point numbers.
\begin{lstlisting}
floatnumber   ::=  pointfloat | exponentfloat

pointfloat    ::=  [intpart] fraction | intpart "."

exponentfloat ::=  (intpart | pointfloat) exponent

intpart       ::=  digit+

fraction      ::=  "." digit+

exponent      ::=  ("e" | "E") ["+" | "-"] digit+
\end{lstlisting}
\end{frame}




\section{Graphs}

\begin{frame}
  \frametitle{Definition}

\ExecuteMetaData[\catchfilefn]{def:Graph} 

\begin{example}
This simple graph has five vertices $\mathcal{V}=\set{v_0,\ldots,v_4}$ 
and eight edges.
\begin{center}
  \vcenteredhbox{\includegraphics{../../languages/asy/graphs/graphs00.pdf}}
  \qquad
  $\mathcal{E}=\set{\set{v_0,v_1},\set{v_0,v_2},\,\ldots\,\set{v_3,v_4}}$
\end{center}
\end{example}

\ExecuteMetaData[\catchfilefn]{discussion:EdgeNotation} 

\ExecuteMetaData[\catchfilefn]{discussion:GraphVariantsi} 
\end{frame}

\begin{frame}
\ExecuteMetaData[\catchfilefn]{discussion:GraphVariantsii} 

\ExecuteMetaData[\catchfilefn]{discussion:GraphVariantsiii} 
\pause
\ExecuteMetaData[\catchfilefn]{def:Subgraph} 
\end{frame}


\begin{frame}
  \frametitle{Graph traversal}

\ExecuteMetaData[\catchfilefn]{def:Adjacent} 

\ExecuteMetaData[\catchfilefn]{def:GraphCircuit} 
\end{frame}


\begin{frame}
  \frametitle{Graph representation}
We can represent a graph in a computer with reasonable efficiency.

\ExecuteMetaData[\catchfilefn]{def:AdjacencyMatrix} 

\begin{example}
This is the matrix for the graph.
\begin{equation*}
  \vcenteredhbox{\includegraphics{asy/grammar004.pdf}}
  \hspace{5em}
  \mathcal{M}=\begin{pmatrix}
    0 &2 &0 &0 \\
    0 &0 &1 &1 \\
    1 &0 &0 &0 \\
    0 &1 &0 &0
  \end{pmatrix}
\end{equation*}
\end{example}

\ExecuteMetaData[\catchfilefn]{lem:MatrixPowersAdjacencyMatrix} 

\begin{example}
We can go from $v_0$ to $v_1$ in three steps in two different ways,
via $e_0e_4e_5$ and via $e_1e_4e_5$. 
\begin{equation*}
  \mathcal{M}^3=\begin{pmatrix}
    2 &2 &0 &0 \\
    0 &2 &1 &1 \\
    0 &0 &2 &2 \\
    1 &1 &0 &0
  \end{pmatrix}
\end{equation*}
% There is only one way from $v_3$ to $v_1$ in three steps, $e_5e_4e_5$.
There is no way from $v_3$ to $v_2$ in three steps.
\end{example}
\end{frame}



\begin{frame}{Graph coloring}
\ExecuteMetaData[\catchfilefn]{def:GraphColoring} 

\begin{example}
We want to schedule final exams for the CS courses
1007, 3137, 3157, 3203, 3261, 4115, 4118, 4156.
The classes 1007 and 3137 have students in common and so do these others:
% 1007-3137,
1007-3157, 3137-3157,
1007-3203,
1007-3261, 3137-3261, 3203-3261,
1007-4115, 3137-4115, 3203-4115, 3261-4115,
1007-4118, 3137-4118,
1007-4156, 3137-4156, 3157-4156.
What is the minimum number of time slots that we must use?
\pause
\begin{center}
  \only<2>{\includegraphics{asy/grammar002.pdf}}%
  \only<3>{\includegraphics{asy/grammar003.pdf}}
\end{center}
\end{example}
\end{frame}


\begin{frame}{Graph isomorphism}
We have a way to express that two graphs are, 
while not equal, essentially the same.

\ExecuteMetaData[\catchfilefn]{def:GraphIsomorphism} 

\ExecuteMetaData[\catchfilefn]{discussion:GraphIsomorphism}

If graphs are isomorphic then
associated vertices have the same degree.
Thus graphs with different degree sequences are not isomorphic.
If the the degree sequences are equal then they
help us construct an isomorphisms, if there is one,
however there are graphs with the same degree sequence that are not isomorphic.
\end{frame}





% ==========================================
% \section{Problems}
% \begin{frame}{Problems, Algorithms, Programs}

% \begin{itemize}
% \item
% A problem is a job, a task that we want done.
% Often a problem encompasses a family of tasks.

% \pause\item
% An algorithm describes at a high level a way to solve the problem
% that is effective, that is computable on a mechanism.
% An algorithm
% should be described in a way that is detailed enough that implementing it is
% routine for an experienced professional.

% \pause\item
% A program is an implementation of an algorithm,
% typically expressed in a formal computer language.
% It is designed to be executed on a particular computing platform.
% \end{itemize}

% \begin{example}
% One problem is, given a natural number, to find its prime factorization.
% One algorithm to solve that problem is to use brute force, that is, to
% check every number less than the input.
% We could implement that with a program written in Scheme.
% \end{example}
% \end{frame}


% \begin{frame}{Points about algorithms}
% While an algorithm is an abstraction, it is nonetheless tied to
% an underlying computing model.
% We have seen the model of Turing machines.
% The most common model is the RAM machine, consisting of a CPU with access
% to memory, where each cell tholds a single atom.
% Another model is distributted computation such as SETI at home.

% \pause
% Strictly speaking, to specify the algorithm,
% a complete description of a problem should include the form of the
% inputs and outputs.
% For instance, if we state a problem as, `to input a sequence of natural numbers
% and return their product'
% then we have not fully specified the input.
% We might take the input to be 
% a list of strings representing decimal numbers
% or
% we might take it as a single bit string
% where each number $n$ is represented with $n$-many \str{1}'s
% and numbers are separated by \str{0}'s.
% This matters because the form of the input and output can change the algorithm
% or our analysis of its behavior.

% Nevertheless, for our purposes specifying the input in great
% detail is going into the weeds.
% In particular at this time
% we only want to know in principle whether we can solve the problems with
% mechanical computation and
% by Church's Thesis that is not 
% affected by the exact form of the input and output representations.
% In the fifth chapter we will worry more about the timing.
% \end{frame}


% \begin{frame}{Problem types}
% \begin{description}[xxx]
% \item[Function problem]
% Produce an algorithm that has a single output for each input.

% \pause\item[Optimization problem]
% Produce an algorithm that find solutions that are best in some way.

% \pause\item[Search problem]
% Produce an algorithm that finds any value matching some criteria.

% \pause\item[Decision problem]
% Produce an algorithm that answers `yes' or `no'.

% A common case is a \alert{language recognition problem}, 
% where we are given a language and asked for an algorithm that 
% will decide membership in that language.
% \end{description}

% \pause
% Search problems and decision problems are related.
% For one, if you can solve a search problem then you can solve the
% associated decision problem quickly.
% For instance, consider 
% the search problem that takes in a string~$\sigma$ and a substring~$\tau$,
% returns the starting index of $\tau$ in~$\sigma$ if it is there, 
% and returns $0$ if not.
% The associated decision problem just asks if $\tau$ is a substring of~$\sigma$,
% yes or no.

% In the other direction,
% if you could quickly solve the decision problem
% then to solve the search problem you could look at the initial segments
% of~$\sigma$: the initial substring of length~$0$, the initial substring
% of length~$1$, etc., and apply the decision problem to each in turn.
% That is, if we can solve the decision problem quickly then
% we can use it to solve the search problem, but with some overhead.
% \end{frame}
\end{document}




