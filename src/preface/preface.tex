\newcommand{\aphorism}[3]{%
  \vspace{3ex plus 1ex minus .2ex}%
  \noindent\parbox{.85\textwidth}{\raggedright\itshape #1 \\ 
    \hspace*{\fill}{\upshape -- #2, \MakeUppercase{#3}}}}

\newcommand{\header}[1]{\vspace{1ex}\par\noindent\textbf{#1}\hspace{0.55em plus 0.1em minus 0.05em}}

% prolegomenon
\par\noindent{\Large\bfseries Preface}
\vspace{3ex}

The Theory of Computation is a wonderful thing.
It is beautiful.
It has deep connections with other areas in
mathematics and computer science, as well as with the wider intellectual world.
It is full of ideas, exciting and arresting ideas, many of which 
apply directly to practical computing.
And, looking forward into this century, 
clearly a theme will be the power and limits of computation.
So it is timely, too. 

It makes a delightful course.
Its organizing question\Dash what can be done?\Dash is both natural
and compelling.
Students see the contrast between computation's capabilities 
and limits.
There are well understood principles 
and within reach are as-yet unknown areas.

This text aims to reflect all of that:~to be precise,
topical, insightful, stimulating, and perhaps sometimes even delightful.



% ------------------------
\header{For students}
Have you ever wondered, while you were learning to instruct 
computers to do your bidding,
what cannot be done?
And what can be done in principle but not in practice?
In this course you will see the signpost results in the study of these 
questions and you will learn to use the tools to address
these issues as they come up in your work. 

We will consider the very nature of computation.
This has been intensively studied for a century so you 
will not see all that is known, but you will see 
enough to end with
key insights and with a better understanding of where the profession
that you are entering stands.

% Unifying it all is the computational perspective,
% that studying both the concepts underlying computation
% and the details of how information flows in computing
% specific things
% gives a deeper understanding of
% the problems that we consider, and of their solutions.

We do not stint on precision\Dash why would we want to?\Dash 
but we approach the ideas liberally; in a way that, 
\citetext{in addition to technical detail, also attends to a breadth of knowledge}{%
  S~Pinker emphasizes that a liberal approach involves making connections and
  understanding in a context \autocite{Pinker}.
  ``It seems to me that educated people should know something about the 
  13-billion-year prehistory of our species and the basic laws governing 
  the physical and living world, including our bodies and brains. 
  They should grasp the timeline of human history from the dawn of agriculture 
  to the present. 
  They should be exposed to the diversity of human cultures, and the major 
  systems of belief and value with which they have made sense of their lives. 
  They should know about the formative events in human history, including the 
  blunders we can hope not to repeat. 
  They should understand the principles behind democratic governance and the 
  rule of law. 
  They should know how to appreciate works of fiction and art as sources of 
  aesthetic pleasure and as impetuses to reflect on the human condition.  
  On top of this knowledge, a liberal education should make certain habits 
  of rationality second nature. 
  Educated people should be able to express complex ideas in clear writing 
  and speech. 
  They should appreciate that objective knowledge is a precious commodity, 
  and know how to distinguish vetted fact from superstition, rumor, 
  and unexamined conventional wisdom. 
  They should know how to reason logically and statistically, avoiding the 
  fallacies and biases to which the untutored human mind is vulnerable. 
  They should think causally rather than magically, and know what it takes 
  to distinguish causation from correlation and coincidence. 
  They should be acutely aware of human fallibility, most notably their own, 
  and appreciate that people who disagree with them are not stupid or evil. 
  Accordingly, they should appreciate the value of trying to change minds 
  by persuasion rather than intimidation or demagoguery.''
  See also \protect\url{https://www.aacu.org/leap/what-is-a-liberal-education}.}.
We will be eager to
make connections with other fields, with things that you have previously
studied, and with a variety of modes of thinking,
most importantly
\citetext{computational thinking}{%
  \url{http://www.cs.cmu.edu/afs/cs/usr/wing/www/publications/Wing06.pdf}}.
People learn best when the topic fits into a whole,
as several of the quotes below express.

The presentation here encourages you be an active learner:~to
explore and reflect on the motivation, development, and future of those ideas.
It gives you the chance to follow things that intrigue you,
including that in the
back of the book are lots of notes to the main text,
many of which contain links that will take you even deeper.
There are also Extra sections at the end of each chapter 
to help you explore further.
Whether or not your instructor covers them formally in class,
they can further your understanding of the material
and where it leads.

The subject is big and a challenge.
It will change the
way that you see the world.
It is also a great deal of fun.
Enjoy!


% ------------------------
\header{For instructors}
We cover the definition of
computability, unsolvability, 
languages and grammars, automata, and complexity.
The audience is undergraduate majors in Computer Science, Mathematics, and
nearby areas.

The prerequisite, besides introductory programming, is
Discrete Mathematics.
We rely on propositional logic,
proof methods including induction,
graphs,
basic number theory, 
sets, functions, and relations.
For non-Computer Science students, 
appendices establish notation and terminology for strings, functions,
and propositional logic, and there are brief sections on graphs
and Big-$\bigOh$
(this section requires derivatives). 

A text does its readers a disservice if it is not precise.
Details matter.
But students can also fail to understand a subject because they have not had
a chance to reflect on the underlying ideas.
The presentation here stresses motivation and naturalness
and, where practical, sets the results in a network of connections.

The first example comes on the first page where we begin with Turing machines.
The alternative of first doing Finite State machines
may be mathematically slicker but for
a fresh learner it is more natural to instead
start by asking what can be computed at all.
We follow the definition of computable function with  
an extensive discussion of Church's Thesis,
relying on the intuition that students have
from their programming experience.
This discussion also justifies
giving algorithms
in outline or as code in the Racket language,
which better communicates the ideas than code for a computation model
or intricate recursions.

A second example of choosing naturalness and
making connections happens with  nondeterminism.
We introduce it in the context
of Finite State machines along with a
discussion promoting intuition, so that 
% giving students a chance to reflect on this important but tricky concept.
when it appears again in Complexity we can rely on this understanding
to develop the standard definition that a language is in
$\NP$ if it has a polytime verifier. 

A third example is the inclusion
of a section introducing the kinds of problems that drive
the work in Complexity today.
Still another
is the discussion of the current state of
$\P$ versus~$\NP$.
These and many more, 
taken together,
encourage students to develop the habit of inquiry.
They should expect that stuff makes sense.



% ------------------------
% \header{Speaking in code}
% As the quote below by A~Downey expresses, for communicating
% effective procedures, a
% modern programming language is hard to beat as a
% blend of precision and concision.
% We use Scheme as a single, uniform, practical language for this. 
% One reason is that it is very easy to learn.
% Another is that it
% \citetext{suits the topics}{John McCarthy, 
% the inventor of Scheme's ancestor Lisp, suggested the language
% as well-suited for the Theory of Computation.
% Two arguments in that direction 
% are the language's elegance and expressiveness.
% This is captured  an online obituary of
% McCarthy written by Bertrand Meyer for the 
% \textit{Communications of the Association for Computing Machinery}:
% ``The Lisp 1.5 manual, published in 1962, was another masterpiece; 
% as early as page 13 it introduces\Dash an unbelievable feat, especially 
% considering that the program takes hardly more than half a page\Dash an 
% interpreter for the language being defined, written in that very language! 
% The more recent reader can only experience here the kind of visceral, 
% poignant and inextinguishable jealously that overwhelms us the first time we 
% realize that we will never be able to attend the premi\`ere of Don Giovanni at 
% the Estates Theater in Prague on 29 October, 1787 \ldots\,. 
% What may have been the reaction of someone in `Data Processing,' 
% such as it was in 1962, suddenly coming across such a language manual?'' 
% \autocite{MeyerB}}.
% But the main reason is that, as stated 
% in the E~Dijkstra quote below, it is a delight.
% https://hardmath123.github.io/perchance-to-scheme.html


% -----------------------------
\header{Exploration and Enrichment}
The Theory of Computation is fascinating.
This book aims to showcase that,
to draw readers in, to be absorbing.
It uses lively language and many illustrations.

One way to stimulate readers is to make the material explorable.
Where practical, references are clickable.
For example, each picture of a founder of the subject is
a link to their Wikipedia page.
This makes them very much more likely to be
the subject of further reading than is the same content in a physical library.

Another example of encouraging engagement is 
the many notes in the back that fill out, and add a spark to, the 
core discussion.
Still another example is the inclusion of
informal discussions.
Informality has the potential to be a problem,
which is why it is carefully differentiated,
but it can also be very valuable.
Who has not had an Ah-ha!\@\ moment
in a hand-wavy hallway conversation? 

Finally, chapters end in a number of additional topics.
They are suitable as one-day lectures, or for assigning for group presentations
or extra credit or honors credit, or just for students to read for pleasure.




% ----------------------
\header{Schedule}
Below is a semester schedule that I have used.
The classes were four credits and met three times a week.
I used the slides available from the home page.

Chapter~I defines models of computation, Chapter~II covers unsolvability,
Chapter~III does languages and graphs, Chapter~IV is automata, and Chapter~V
is computational complexity.
I assign the readings as homework and quiz on them.
For those working independently I have marked a selection of exercises
with \recommendationmark\!.
\begin{center}\small
\begin{tabular}{r|lll}
  \multicolumn{1}{c}{\ }
             &\textit{Sections} &\textit{Reading} &\textit{Notes} \\ \cline{2-4}
  \textit{Week 1} &I.1, I.3     &I.2                     \\
  \textit{     2} &I.4, II.1    &II.A                    \\
  \textit{     3} &II.2, II.3   &                        \\
  \textit{     4} &II.4, II.5   &II.B                    \\
  \textit{     5} &II.6, II.7   &II.C                     \\
  \textit{     6} &II.9         &III.A      &\textsc{Exam} \\
  \textit{     7} &III.1--III.3 &                       \\
  \textit{     8} &IV.1, IV.2                             \\
  \textit{     9} &IV.3, IV.3   &IV.A                        \\
  \textit{    10} &IV.4, IV.5   &                            \\
  \textit{    11} &IV.7         &IV.1.4    &\textsc{Exam} \\
  \textit{    12} &V.1, V.2     &V.A                      \\
  \textit{    13} &V.4, V.5     &V.3                      \\
  \textit{    14} &V.6, V.6     &V.B       &        \\
  \textit{    15} &V.7          &\textsc{Discussion}   &\textsc{Review for final} \\
\end{tabular}
\end{center}
(Some instructors will do the Myhill-Nerode theorem instead
of section IV.5.)

%---------------------------------------
\header{License}
This book is Free.
You can download and use it without cost.
If you are a teacher then
you can post it on the Learning Management System for
your course.
Or, you can get bound book if you like that better.
% You can also modify the \LaTeX{} source.
For the full details,
see the home page \url{https://hefferon.net/computation}. 

One reason that the book is Free is that it is written in 
\LaTeX{}, which is Free,
as is Asymptote which drew the illustrations,
along with Emacs and all of GNU software, and
the entire Linux platform on which this book was
developed.
And anyway, the research that this text
presents was all made freely available by scholars.

Beyond those reasons, there is a long tradition of making educational
work open.
I believe that the synthesis here adds
value\Dash I hope so, indeed\Dash but the masters have left a well-marked
trail and following it seems right.


%---------------------------------------
\header{Acknowledgments}
I owe a great debt to my wife, whose patience with this
project has gone beyond all reasonable bounds.
Thank you, Lynne.

My students have made the book better in so many ways.
Thank you all for those contributions.

And, I must honor my teachers.
First among them is M~Lerman.
Thank you, Manny.
They also include H~Abelson, GJ~Sussmann, and J~Sussmann, whose
\textit{Structure  and Interpretation of Computer Programs} dared to show
students how mind-blowing it all is.
When I see a computer text
with examples about managing
a used car dealership inventory, I can only say:~Thank you,
for believing in me.

% \newpage
\vspace{1ex plus .25ex minus .1ex}
\aphorism{Memory works far better when you learn networks of facts rather than facts in isolation.}{T~Gowers}{WHAT MATHS A-LEVEL DOESN'T NECESSARILY GIVE YOU} 
% https://gowers.wordpress.com/2012/11/20/what-maths-a-level-doesnt-necessarily-give-you/

\vspace{1ex plus .25ex minus .1ex}
% \aphorism{Lisp has jokingly been called ``the most intelligent way to misuse a computer.'' I think that description is a great compliment because it transmits the full flavor of liberation: it has assisted a number of our most gifted fellow humans in thinking previously impossible thoughts.}{E Dijkstra}{CACM, 15:10}
\aphorism{Research into learning shows that content is
best learned within context \ldots{}\,, when the learner is active, and that above all, when the learner can actively construct knowledge by developing meaning and `layered' understanding.}{A W  (Tony) Bates}{Teaching in a Digital Age} % 2nd edition, p 587

\vspace{1ex plus .25ex minus .1ex}
\aphorism{Teach concepts, not tricks.}{G Rota}{TEN LESSONS I WISH I HAD LEARNED BEFORE I STARTED TEACHING
DIFFERENTIAL EQUATIONS}
% from an MAA talk

\vspace{1ex plus .25ex minus .1ex}
\aphorism{[W]hile many distinguished scholars have embraced
[the Jane Austen Society]
and its delights since the founding meeting, ready to don period dress,
eager to explore antiquarian minutiae, and happy to stand up at the
Saturday-evening ball, 
others, in their studies of Jane Austen's works,
\ldots{} have described how, as professional scholars, they are
rendered uneasy by this performance of \emph{pleasure} at
[the meetings].
\ldots\@
I am not going to be one of those scholars.
}{E Bander}{Persuasions, 2017}
% from p 151


\vspace{1ex plus .25ex minus .1ex}
\aphorism{The power of modern programming languages is that they are expressive, readable, concise, precise, and executable. That means we can eliminate middleman languages and use one language to explore, learn, teach, and think.}{A Downey}{Programming as a Way of Thinking}
% from https://blogs.scientificamerican.com/guest-blog/programming-as-a-way-of-thinking/nn, April 26, 2017



\vspace{1ex plus .25ex minus .1ex}
\aphorism{Of what use are computers?  They can only give answers.}{P Picasso}{The Paris Review, Summer-Fall 1964}

\vspace{2ex plus 1\fill}
\begin{flushright}
  \begin{tabular}{l@{}}
    Jim Hef{}feron \\ 
    Jericho, VT USA \\
    University of Vermont \\
    \texttt{hefferon.net} \\
    Version 1.90, \ymd{2025}{03}{04}
  \end{tabular}
\end{flushright}
