computing/src/scheme/prologue/life/README

2024-Aug-0 Jim Hefferon  Created.  Public Domain.


Scripts for Conway's Game of Life.

1) life.rkt and life-test.rkt

Given an input file with an initial board, run the game for a specified number
of generations.

Typical invocation:

  jim@millstone:~/Documents/computing/src/scheme/prologue/life$ ./life.rkt -s 1 -v blinker.init

This creates two files blinker000.life and blinker001.life.  The 000 file is
the initial grid recreated, while the 001 file is the first generation.

This is blinker.init:

# Simple blinker for Conway's Game of Life
.*.
.*.
.*.

Of course, `.' means dead cell and `*' means live cell.  Comments are
full-line only and begin with a `#'.

2) run_life.sh

This fetches a list of <fn>.init files from

  computing/src/prologue/asy/life

and does two things to each <fn>.init file.
(a) It runs life.rkt on it, producing a sequence of .life game boards.
<fn>000.life, <fn>001.life, ...  It then copies these .life files back to
/computing/src/prologue/asy/life/gameboards.  
(b) It produces <fn>_all.asy, which it copies to
/computing/src/prologue/asy/life.  When you run Asymptote on this file, it
reads the sequence of <fn>xxx.life files and outputs <fn>xxx.pdf for
inclusion directly in the book.

Comments:

* I like having the .life files because I can include them in the .git repo,
and a person does not have to worry that their Racket version no longer
compiles the .init file to the .life files.

* The life.rkt program allows the gameboard to grow if needed.