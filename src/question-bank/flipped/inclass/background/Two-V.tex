% \documentclass[noanswers, nolegalese, 11pt]{examjh}
\documentclass[answers, nolegalese, 11pt]{examjh}
\usepackage{../../../../computingfonts}
\usepackage{../../../../contentmacros}

\usepackage{graphicx}

\usepackage{listings}
\lstset{basicstyle=\small\ttfamily,
  keepspaces=true,
  columns=fullflexible}

\setlength{\parindent}{0em}\setlength{\parskip}{0.5ex}
\pagestyle{empty}
\begin{document}\thispagestyle{empty}
\makebox[\textwidth]{Questions for Two.V\hfill  From \textit{Theory of Computation}, by Hef{}feron}\vspace{-1ex}
\makebox[\textwidth]{\hbox{}\hrulefill\hbox{}}

\begin{questions}
\question 
Show that each of these problems is unsolvable,
by reducing it to the \HP.
\begin{parts}
\part Given $e$, does $\TM_e$ halt on $5$?
\begin{solution}
We will show that there is no Turing machine to compute this function.
\begin{equation*}
  \computablefunction{halts\_on\_five\_decider}(x)=
  \begin{cases}
    1 &\case{if $\TMfcn_x(5)\converges$}  \\
    0 &\case{otherwise} 
  \end{cases}
\end{equation*}
To do that, we will show 
that if $\computablefunction{halts\_on\_five\_decider}$ 
were a computable function then we could compute the solution of the \HP.

For contradiction, assume that 
$\computablefunction{halts\_on\_five\_decider}$ is mechanically
computable.
Consider this function.
\begin{equation*}
  \psi(x,y)=
  \begin{cases}
    42 &\case{if $\phi_x(x)\converges$} \\
    \uparrow &\case{otherwise}
  \end{cases}
\end{equation*}
Observe that~$\psi$ is mechanically computable, because we 
could easily write a computer program for it.
\begin{center}
  \includegraphics{asy/flowcharts000.pdf}
\end{center}
So by Church's Thesis
there is a Turing machine whose input-output behavior is $\psi$.
That Turing machine has some index.
Let the index be~$e$, meaning that $\psi=\phi_e$.

Use the \smn{} theorem to parametrize~$x$, giving $\phi_{s(e,x)}(y)$.
This is a family of functions, one for~$x=0$, one for~$x=1$, etc.\sentencespace
\begin{center}
  \vcenteredhbox{\includegraphics{asy/flowcharts001.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts001.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts001.pdf}}
  \quad
  \ldots
\end{center}
Observe that for each such function $\phi_{s(e,x)}$ in the family, 
it converges on all inputs~$y$
if and only if $\phi_x(x)\converges$.
In particular, it converges on $y=5$ 
if and only if $\phi_x(x)\converges$.
Therefore, for all $x\in\N$ we have this.
\begin{equation*}
 \phi_x(x)\converges 
  \quad\text{if and only if}\quad
 \computablefunction{halts\_on\_five\_decider}(\,s(e,x)\,)=1
\end{equation*}
The function~$s$ is effectively computable so 
the supposition that $\computablefunction{halts\_on\_five\_decider}$ is 
also effectively computable gives that the 
entire right side is effectively computable.
In turn, that implies that the \HP{} is effectively solvable, 
which is a contradiction.
Therefore, $\computablefunction{halts\_on\_five\_decider}$
is not mechanically computable.
\end{solution}

\part Given $e$, does $\TMfcn_e(4)$ converge and equal $7$?
\begin{solution}
We will show that no Turing machine computes this function.
\begin{equation*}
  \computablefunction{returns\_seven\_on\_four\_decider}(x)=
  \begin{cases}
    1 &\case{if $\TMfcn_x(4)=7$}  \\
    0 &\case{otherwise} 
  \end{cases}
\end{equation*}
To do that, we will show 
that if $\computablefunction{returns\_seven\_on\_four\_decider}$ 
were a computable function then we could compute the solution of the \HP.

For contradiction, assume that 
$\computablefunction{returns\_seven\_on\_four\_decider}$ is mechanically
computable.
Consider this function.
\begin{equation*}
  \psi(x,y)=
  \begin{cases}
    7 &\case{if $\phi_x(x)\converges$} \\
    \uparrow &\case{otherwise}
  \end{cases}
\end{equation*}
Observe that~$\psi$ is mechanically computable, because we 
could easily write a computer program for it.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/flowcharts004.pdf}}
\end{center}
So by Church's Thesis
there is a Turing machine whose input-output behavior is $\psi$.
That Turing machine has some index.
Let the index be~$e$, meaning that $\psi=\phi_e$.

Use the \smn{} theorem to parametrize~$x$, giving $\phi_{s(e,x)}(y)$.
This is a family of functions, one for~$x=0$, one for~$x=1$, etc.\sentencespace
\begin{center}
  \vcenteredhbox{\includegraphics{asy/flowcharts005.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts006.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts007.pdf}}
  \quad
  \ldots
\end{center}
For all $x\in\N$ we have this.
\begin{equation*}
 \phi_x(x)\converges 
  \quad\text{if and only if}\quad
 \computablefunction{returns\_seven\_on\_four\_decider}(\,s(e,x)\,)=1
\end{equation*}
The function~$s$ is computable so 
the supposition that $\computablefunction{returns\_seven\_on\_four\_decider}$ is 
also computable gives that the right side is effectively computable.
In turn, that implies that the \HP{} is effectively solvable, 
which is a contradiction.
Therefore, $\computablefunction{returns\_seven\_on\_four\_decider}$
is not mechanically computable.
\end{solution}

\part Given $e$, does $\TMfcn_e(a)$ converge and equal $a^2$?
\begin{solution}
We will show that no Turing machine computes this function.
\begin{equation*}
  \computablefunction{squarer\_decider}(x)=
  \begin{cases}
    1 &\case{if $\TMfcn_x(a)=a^2$}  \\
    0 &\case{otherwise} 
  \end{cases}
\end{equation*}
To do that, we will show 
that if $\computablefunction{squarer\_decider}$ 
were a computable function then we could compute the solution of the \HP.

For contradiction, assume that 
$\computablefunction{squarer\_decider}$ is mechanically
computable.
Consider this function.
\begin{equation*}
  \psi(x,y)=
  \begin{cases}
    y^2 &\case{if $\phi_x(x)\converges$} \\
    \uparrow &\case{otherwise}
  \end{cases}
\end{equation*}
Observe that~$\psi$ is mechanically computable, because we 
could easily write a computer program for it.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/flowcharts008.pdf}}
\end{center}
So by Church's Thesis
there is a Turing machine whose input-output behavior is $\psi$.
That Turing machine has some index.
Let the index be~$e$, meaning that $\psi=\phi_e$.

Use the \smn{} theorem to parametrize~$x$, giving $\phi_{s(e,x)}$.
This is a family of functions, one for~$x=0$, one for~$x=1$, etc.\sentencespace
\begin{center}
  \vcenteredhbox{\includegraphics{asy/flowcharts009.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts010.pdf}}
  \quad
  \vcenteredhbox{\includegraphics{asy/flowcharts011.pdf}}
  \quad
  \ldots
\end{center}
For all $x\in\N$ we have this.
\begin{equation*}
 \phi_x(x)\converges 
  \quad\text{if and only if}\quad
 \computablefunction{squarer\_decider}(\,s(e,x)\,)=1
\end{equation*}
The function~$s$ is computable so 
the supposition that $\computablefunction{squarer\_decider}$ is 
also computable gives that the right side is effectively computable.
In turn, that implies that the \HP{} is effectively solvable, 
which is a contradiction.
Therefore, $\computablefunction{squarer\_decider}$
is not mechanically computable.
\end{solution}
\end{parts}
\end{questions}
\end{document}
