% \documentclass[noanswers, nolegalese, 11pt]{examjh}
\documentclass[answers, nolegalese, 11pt]{examjh}
\usepackage{../../../../computingfonts}
\usepackage{../../../../contentmacros}
\usepackage{../../../../grammar}
\usepackage{graphicx}

\setlength{\parindent}{0em}\setlength{\parskip}{0.5ex}
\pagestyle{empty}
\begin{document}\thispagestyle{empty}
\makebox[\textwidth]{Questions for Five.V\hfill  From \textit{Theory of Computation}, by Hef{}feron}\vspace{-1ex}
\makebox[\textwidth]{\hbox{}\hrulefill\hbox{}}



\begin{questions}
\question
There are $n$ radio stations and we must assign each a frequency.
There are $B$~many frequencies available.  
To avoid interference, stations closer than $100$~km cannot use the
same frequency, so we have a list of distances $D(i,j)$
and want to know if there is a set of assignments that meets the restrictions.
We will restate this as a satisfiability problem.
\begin{parts}
\part
Represent this as 
a propositional logic statement for the case of $n=3$ and $B=2$. 
\begin{solution}
A condition such as $D(0,1)\geq 100$ is either True or False; call it $P$.
Similarly, let $Q$ be the condition $D(0,2)\geq 100$,
and let $R$ be the condition $D(1,2)\geq 100$.
We can assign frequencies if there are two pairs of stations that satisfy
the distance criteria.
Thus this statement is satisfiable if and only if there is a frequency 
assignment that meets the restrictions.
\begin{equation*}
  (P\wedge Q)\vee(P\wedge R)\vee(Q\wedge R)
\end{equation*}
\end{solution}

\part For the propositional logic statement in the $n,B$ general case, 
how many $\wedge$-clauses
are there?
\begin{solution}
$\binom{n}{B}$
\end{solution}
\end{parts}

\question
A judge must form a jury of twelve people. 
The law requires that no one on the jury know any other member. 
There are $n$ candidates and the judge has a Boolean function
$\operatorname{knows}(i,j)$.
We will restate this as a satisfiability problem.
\begin{parts}
\part
Give a propositional logic statement for the case of 
forming juries of three people chosen from a pool of five.
\begin{solution}
The judge has a statement made of clauses.
The clauses are joined by $\vee$'s, and each clause looks as here.
\begin{align*}
  &\bigl[(\neg\operatorname{knows}(0,1)\wedge\neg\operatorname{knows}(0,2)\wedge\neg\operatorname{knows}(1,2)\bigr]  \\
  &\quad\vee\,
  \bigl[(\neg\operatorname{knows}(0,1)\wedge\neg\operatorname{knows}(0,3)\wedge\neg\operatorname{knows}(1,3)\bigr]  \\
  &\quad\smash{\;\vdots{}}           \\
  &\quad\vee\,\bigl[(\neg\operatorname{knows}(3,4)\wedge\neg\operatorname{knows}(3,5)\wedge\neg\operatorname{knows}(4,5)\bigr]
\end{align*}
\end{solution}
\part How many $\wedge$-clauses are there?
\begin{solution}
There is a clause for each triple of $1$'s
\begin{center}
\begin{tabular}{*{5}{c}}
  \textit{candidate $0$}
    &\textit{candidate $1$}
    &\textit{candidate $2$}
    &\textit{candidate $3$}
    &\textit{candidate $4$}  \\
  \hline
  $1$ &$1$ &$1$ &$0$ &$0$ \\
  $1$ &$1$ &$0$ &$1$ &$0$ \\
  $1$ &$1$ &$0$ &$0$ &$1$ \\
  $1$ &$0$ &$1$ &$1$ &$0$ \\
  $1$ &$0$ &$1$ &$0$ &$1$ \\
  $1$ &$0$ &$0$ &$1$ &$1$ \\
  $0$ &$1$ &$1$ &$1$ &$0$ \\
  $0$ &$1$ &$1$ &$0$ &$1$ \\
  $0$ &$1$ &$0$ &$1$ &$1$ \\
  $0$ &$0$ &$1$ &$1$ &$1$ \\
\end{tabular}
\end{center}
There are $\binom{5}{3}=10$ many clauses.
\end{solution}

\part In the general $n$-candidate and twelve person jury case, 
how many clauses are there?
\begin{solution}
There are $\binom{n}{12}$~many clauses.
\end{solution}
\end{parts}

\question
Recall that we say a graph 
is \textit{$k$-colored} if the vertices are
broken into $k$-many disjoint sets so that no two vertices in a 
set are connected
(these sets are called \textit{colors} because of the problem's history).
The \textit{$k$-colorability problem} is, given a graph, to determine if it
can be $k$-colored.
We will restate this as a satisfiability problem.
\begin{parts}
\part
Consider a graph $\mathcal{G}$ with five vertices, 
$\mathcal{N}=\set{v_0,\ldots v_4}$.
How many ways are there to break the vertices from the set~$\mathcal{N}$ into 
$k=2$ colors
(paying no attention to whether they are connected)?
\begin{solution}
Take a subset $C_0\subseteq\mathcal{N}$, and then all of the other vertices,
the complement, makes up the other color, $C_1=\mathcal{N}-C_0$.
Because $\sizeof{\mathcal{N}}=5$, there are $2^5=32$ many possible such subsets
(although there is a redundancy such as with 
$C_0=\set{v_0,v_1},C_1=\set{v_2,v_3,v_4}$ and
$C_0=\set{v_2,v_3,v_4},C_1=\set{v_0,v_1}$, so in fact the number of different
splits is $2^{\sizeof{\mathcal{N}}}/2$).
\end{solution}

\part Assume that you have a Boolean function $e(i,j)$ that returns~$T$ 
if and only if $v_i$ is connected to~$v_j$.
Give a propositional logic statement that is satisfiable if
and only if this graph is $2$-colorable.
For illustration, you can simply produce the clause for the 
$C_0=\set{v_0,v_1}$ and $C_1=\set{v_2,v_3,v_4}$ potential coloring.
\begin{solution}
For the 
$C_0=\set{v_0,v_1}$ and $C_1=\set{v_2,v_3,v_4}$ potential coloring
we get this clause.
\begin{equation*}
  \neg e(0,1)
  \,\wedge\, 
  \neg e(2,3) 
  \,\wedge\,
  \neg e(2,4) 
  \,\wedge\,
  \neg e(3,4) 
\end{equation*}
\end{solution}

\part
Outline how to get a suitable propositional logic statement for the
$k$-colorability problem.
\begin{solution}
The overall propositional logic statement consists of $\wedge$~clauses,
joined with $\vee$'s.
There is a $\wedge$~clause for each way to partition the set of vertices 
into $k$~many disjoint sets.
Each such clause consists of the collection of atoms $\neg e(i,j)$, where 
$v_i$ and~$v_j$ are together in a color.
\end{solution}
\end{parts}

\question
State  the frequency assignment problem as a language decision problem.
Give an algorithm for this problem that is
suitable for a nondeterministic Turing machine.
\begin{solution}
Write the set of stations as $S={s_0, s_1, \ldots{} s_{k-1}}$.
Write the distance function as $\map{D}{S\times S}{\R}$.
And, the number of frequencies is~$B\in \N$. 
Here is a language decision problem.
\begin{equation*}
  \lang=\set{\sequence{S,D,B}\suchthat 
           \text{there is a way to dole out frequencies meeting the restrictions}}
\end{equation*}

For the algorithm, the machine is given input $\sigma=\sequence{S,D,B}$
(of course, it is actually a bitstring representing the triple).
Say that there is an \textit{assignment}~$\omega$ 
if it is a string representing a partition of 
the stations $\omega=\set{C_0,\ldots{} C_{B-1}}$ 
such that within any subset~$C_i$ 
no two stations are less than $100$\,km apart.

The algorithm is that the machine guesses an assignment
$\omega=\set{C_0,\ldots{} C_{B-1}}$:~if 
there is a solution, if $\sigma\in \lang_B$, 
then there is a way for the machine to guess right,
and by definition that means the nondeterministic machine accepts~$\sigma$.
If there is no solution then that means there is no way for the machine
to guess right, and by definition the nondeterministic machine does
not accept~$\sigma$.

If you are more comfortable this way, then you can think that a demon
gives the machine a hint, a bitstring~$\omega$ representing
$\set{C_0,\ldots{} C_{B-1}}$.  
Then the machine just has to check that it meets the requirements.
\end{solution}

\question
State the  jury selection problem as a language decision problem.
Give an algorithm for this problem that is 
suitable for a nondeterministic Turing machine.
\begin{solution}
Here is a language decision problem.
\begin{equation*}
  \lang=\set{\sequence{J,K}\suchthat 
           \text{$S$ is a set of jurors, $K$ gives who knows who, and there is a way to pick~$12$}}
\end{equation*}

For the algorithm, the machine is given input $\sigma=\sequence{J,K}$.
Say that there is an \textit{assignment} $\omega=\set{j_0,\ldots{} j_{11}}$
if it represents a list of twelve members of~$J$ who are pairwise strangers, 
according to~$K$.

The algorithm is that the machine guesses a witness 
$\omega=\set{j_0,\ldots{} j_{11}}$.
If there is a solution, if $\sigma\in \lang$, 
then there is a way for the machine to guess right,
and by definition that means the nondeterministic machine accepts~$\sigma$.
If there is no solution then that means there is no way for the machine
to guess right, and by definition the nondeterministic machine does
not accept~$\sigma$.

As earlier, if you are more comfortable then
then you can think that a demon
gives the machine a hint, a bitstring~$\omega$ representing
the $\set{j_0,\ldots{} j_{11}}$.  
Then the machine checks that it meets the requirements.
\end{solution}

\question
State the $k$-colorable problem as a language decision problem.
Give an algorithm for this problem
suitable for a nondeterministic Turing machine.
\begin{solution}
Here is the language decision problem.
\begin{equation*}
  \lang=\set{\sequence{\mathcal{G},k}\suchthat 
           \text{$\mathcal{G}$ is a graph that is $k$ colorable}}
\end{equation*}

For the algorithm, the machine is given input $\sigma=\sequence{\mathcal{G},k}$.
Say that there is an \textit{assignment}~$\omega$
if it names a coloring 
$\omega=\set{C_0,C_1,\ldots C_{k-1}}$.

The algorithm is that the machine guesses an assignment
$\omega=\set{C_0,C_1,\ldots C_{k-1}}$.
If there is a solution, if $\sigma\in \lang$, 
then there is a way for the machine to guess right,
and by definition that means the nondeterministic machine accepts~$\sigma$.
If there is no solution then that means there is no way for the machine
to guess right, and by definition the nondeterministic machine does
not accept~$\sigma$.
\end{solution}


\end{questions}
\end{document}
