{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Regular expressions in practice\n",
    "\n",
    "As part of the theory of Finite State machines, we introduced regular expressions.  There, they are small and elegant.  That is, our theory development focuses on things that are easy to understand, and to prove things about. But an expanded version of regular expressions are also widely used in practice. The languages used today include many additions and extensions.  To disinguish the two kinds, we will describe the ones used in practice as *regexes*. \n",
    "\n",
    "We will not cover every possiblilty; see [the reference](https://docs.racket-lang.org/reference/regexp.html).\n",
    "But this list gives some sense of the possibilities, and of the widespread applicability of regexes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The basics\n",
    "\n",
    "We've defined a regular expression, and now a regex, as a pattern that describes a set of strings.\n",
    "We say that strings in the set *match* the regex.\n",
    "An example is that if we are looking through a file of source code for looping constructs we might give the editor the regex\n",
    "`(for)|(while)|(unless)`.\n",
    "Recall that the vertical bars mean alternation, that is, \"or\", and the parentheses group.\n",
    "\n",
    "Another example is that if we are looking for strings of at least one minus sign, we could give the \n",
    "editor the expression `--*`.\n",
    "The Kleene star `*` matches repetition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "1. Name the three strings that match `(for)|(while)|(unless)`.\n",
    "\n",
    "2. Give a regular expression matching strings that begin with a lower-case ASCII letter, then include\n",
    "at least two digits, and then end in an upper case letter.\n",
    "\n",
    "3. Which binds more tightly: repetition or concatenation? \n",
    "Which binds more tightly: concatenation or alternation?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Racket syntax for regexes\n",
    "\n",
    "A Racket regular expression is a string prefixed with `#rx` or `#px`.\n",
    "The second includes some extras so we will use it, as in `#px\"abc\"`.\n",
    "\n",
    "Test for matches with `regexp-match`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"acd\" \"c\")</code>"
      ],
      "text/plain": [
       "'(\"acd\" \"c\")"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"a(b|c)d\" \"acd\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"a(b|c)d\" \"xyz\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the string does not match then Racket returns `#f`.  If the string does match then Racket returns a list. \n",
    "(Note that a list is not `#f`, so we can use the construct `(if (regexp-match #px\"a(b|)d\" \"acd\") ...)`.)\n",
    "\n",
    "In a success list, the first string is the part of the input matching the regex. If there are two or more matching substrings then it shows the earlier one.\n",
    "The other items in the list are strings matching parenthesized sub-expressions. \n",
    "(These come in order of the open parenthesis. Matches for the sub-expressions are provided in the order of the opening parentheses in the regex. If the overall regex can succeed without a match for the sub-expression, then instead of a substring, you see a `#f`.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"adef\" \"de\" \"de\")</code>"
      ],
      "text/plain": [
       "'(\"adef\" \"de\" \"de\")"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"a(b|(c|de))f\" \"adef\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the `de` is what matches for both the first and second open parenthesis.\n",
    "\n",
    "The empty string is perfectly legal here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"c\" \"\")</code>"
      ],
      "text/plain": [
       "'(\"c\" \"\")"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"(a||b)c\" \"c\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Anchors\n",
    "\n",
    "A very important practial point about regexes, in contrast with the theoretical regular expressions,\n",
    "is that they are not anchored at the start of the string.\n",
    "Instead, the string matches if the regex pattern is anywhere in that string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"ab\")</code>"
      ],
      "text/plain": [
       "'(\"ab\")"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ab\" \"cabd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is what, in practice, users usually want.\n",
    "If you want to instead anchor to the string start, put a caret, `^`, at the start of the regex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"^ab\" \"cabd\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"ca\")</code>"
      ],
      "text/plain": [
       "'(\"ca\")"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"^ca\" \"cabd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a similar way to make the search extend all the way to the end of the string.\n",
    "Put a dollar sign at the end of the regex. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ca$\" \"bcad\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"cbd\" \"b\")</code>"
      ],
      "text/plain": [
       "'(\"cbd\" \"b\")"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"^c(a|b)d$\" \"cbd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sets\n",
    "\n",
    "In the book's development of theoretical regular expressions, the alphabets had two\n",
    "or three characters.\n",
    "In practice, an alphabet usually includes at least\n",
    "ASCII’s printable characters such as a-z, A-Z, 0-9, space, etc. \n",
    "And often the alphabet contains\n",
    "all of Unicode’s more than one hundred thousand\n",
    "characters.\n",
    "We need manageable ways to describe\n",
    "these sets of characters.\n",
    "\n",
    "The first tool is square brackets.\n",
    "Put a list of characters between the brackets and the string will match if any of those characters works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"mix\")</code>"
      ],
      "text/plain": [
       "'(\"mix\")"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m[aiu]x\" \"mix\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can call for a sequence (the interval includes all characters with Unicode code points in that range)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"mar\")</code>"
      ],
      "text/plain": [
       "'(\"mar\")"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m[a-z]r\" \"mar\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"mAr\")</code>"
      ],
      "text/plain": [
       "'(\"mAr\")"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m[a-zA-z0-9]r\" \"mAr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To invert the set of matched characters, put a caret, `^ `, as the first thing inside\n",
    "the bracket.\n",
    "Thus, `[^0-9]` matches a non-digit\n",
    "and `[^A-Za-z]` matches a character that is not an ASCII letter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m[^a-z]r\" \"mar\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"mAr\")</code>"
      ],
      "text/plain": [
       "'(\"mAr\")"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m[^a-z]r\" \"mAr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Match the text inside a single set of matched diamond brackets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"&lt;b&gt;\")</code>"
      ],
      "text/plain": [
       "'(\"<b>\")"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"<[^<>]*>\" \"<b>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Abbreviations\n",
    "\n",
    "You can also use lists of abbreviations built into the system.\n",
    "The next regex uses backslash-w, `\\w`, to abbreviate the characters that form ordinary words, `[a-zA-Z0-9_]`\n",
    "(here, \"word\" means that in many older programming languages, identifiers used\n",
    "letters, digits, and underscores)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"msr\")</code>"
      ],
      "text/plain": [
       "'(\"msr\")"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m\\\\wr\" \"msr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the double backslash, as with `\\\\w`.\n",
    "This does not have to do with regexes, it is about the syntax of Racket strings - that's just how we include a backslash character in a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "m\\wr"
     ]
    }
   ],
   "source": [
    "(display \"m\\\\wr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A C language identifier begins with an ASCII letter or underscore and\n",
    "then can have arbitrarily many more letters, digits, or underscores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"firstBlock\")</code>"
      ],
      "text/plain": [
       "'(\"firstBlock\")"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"[a-zA-z_]\\\\w*\" \"firstBlock\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All these abbreviations are convenient:\n",
    "\n",
    "1. `\\d` for the digits, and `\\D` for anything that is not a digit\n",
    "2. `\\w` for a word character, and `\\W` for anything not in that list\n",
    "3. `\\s` for a whitespace character, meaning space, tab, newline, formfeed, or line return, and\n",
    "`\\W` for any non-whitespace character.\n",
    "\n",
    "For example, in the twelve hour time format some typical times strings are \"8:05 am\"\n",
    "or \"10:15 pm\".\n",
    "This is a reasonable regex (note the empty string at the start, after the opening parenthesis).\n",
    "(|0|1)\\d:\\d\\d\\s(am|pm)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"2\" \"\")</code>"
      ],
      "text/plain": [
       "'(\"2\" \"\")"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"(|0|1)2\" \"2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"1:15 am\" \"\" \"am\")</code>"
      ],
      "text/plain": [
       "'(\"1:15 am\" \"\" \"am\")"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"(|0|1)\\\\d:\\\\d\\\\d\\\\s(am|pm)\" \"1:15 am\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dot\n",
    "\n",
    "Besides matching limited sets of characters, another natural need is to match any character.\n",
    "As with regular expressions, for regexes the dot `.`\n",
    "matches any member of the alphabet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"met\")</code>"
      ],
      "text/plain": [
       "'(\"met\")"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"m.t\" \"met\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practice, programming languages by default have the dot match any character except newline.\n",
    "(They also have a way to make it match newline.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Metacharacters\n",
    "\n",
    "Recall that in a regular expression such as `a(b|c)d`, the parentheses and the pipe\n",
    "are not there to be matched.\n",
    "They are *metacharacters*, part of the syntax of the\n",
    "regular expression.\n",
    "To match a metacharacter, prefix it with a backlash, `\\`. \n",
    "Thus, to look for the string \"(Note\" put a backslash before the open parentheses `\\(Note`. \n",
    "(Again, here the backslash is doubled because that is how Racket makes a backslash in a string.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(regexp-match #px\"\\\\(Note\" \"text.  (Note; we must\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, `\\|` matches a pipe and `\\[` matches an open square bracket. \n",
    "Match backslash itself with `\\\\`.\n",
    "This is called *escaping* the metacharacter. \n",
    "The method described above for abbreviating sets with `\\d`, `\\D`, etc. is an extension of escaping."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Quantifiers\n",
    "\n",
    "In the theoretical expressions that we saw earlier, to match \"at most one a\" we\n",
    "used \"$\\varepsilon$|a\". \n",
    "In practice we can write something like `(|a)`, as we did above for the example involving time. \n",
    "But depicting the empty string by just putting nothing there can be confusing. \n",
    "Modern languages allow `a?` for \"at most one a\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"cadr\")</code>"
      ],
      "text/plain": [
       "'(\"cadr\")"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ca?dr\" \"cadr\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ca?dr\" \"caadr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those examples show that `?` is a metacharacter in regexes. \n",
    "As earlier, to match a literal question mark, you must escape it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"To be or not to be\\\\?\" \"These are the times that try mens souls\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"To be or not to be?\")</code>"
      ],
      "text/plain": [
       "'(\"To be or not to be?\")"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"To be or not to be\\\\?\" \"To be or not to be?\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For \"at least one a\" modern languages use `a+`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(regexp-match #px\"ca+dr\" \"caaadr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This matches an integer, positive or negative.\n",
    "(Note that the literal `+` is escaped.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"42\" #f)</code>"
      ],
      "text/plain": [
       "'(\"42\" #f)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"(-|\\\\+)?\\\\d+\" \"42\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"-1082\" \"-\")</code>"
      ],
      "text/plain": [
       "'(\"-1082\" \"-\")"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"(-|\\\\+)?\\\\d+\" \"-1082\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For email addresses, \\S+@\\S+ is an often used extended expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"ftpmaint@localhost\")</code>"
      ],
      "text/plain": [
       "'(\"ftpmaint@localhost\")"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"\\\\S+@\\\\S+\" \"ftpmaint@localhost\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More generally, we often want to specify quantities. For instance, to\n",
    "match five a ’s extended regular expressions use the curly braces as metacharacters,\n",
    "with a{5} . Match between two and five of them with a{2,5} and match at least\n",
    "two with a{2,} . Thus, a+ is shorthand for a{1,} ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"caaadr\")</code>"
      ],
      "text/plain": [
       "'(\"caaadr\")"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ca{2,5}dr\" \"caaadr\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"ca{2,5}dr\" \"caaaaaadr\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "US postal codes, called ZIP codes, are five digits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"05439\")</code>"
      ],
      "text/plain": [
       "'(\"05439\")"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"\\\\d{5}\" \"05439\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "North American phone numbers match have a three digit area code, then a three digit local exchange, and finally a four digit identifier.\\d{3} \\d{3}-\\d{4}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"202 456-1414\")</code>"
      ],
      "text/plain": [
       "'(\"202 456-1414\")"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"\\\\d{3} \\\\d{3}-\\\\d{4}\" \"202 456-1414\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Match a user name of between three and twelve letters, digits, under-\n",
    "scores, or periods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"user_id.num\")</code>"
      ],
      "text/plain": [
       "'(\"user_id.num\")"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"[\\\\w\\\\.]{3,12}\" \"user_id.num\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Match a valid username on Reddit: `[\\w-]{3,20}`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"JimH10\")</code>"
      ],
      "text/plain": [
       "'(\"JimH10\")"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"[\\\\w-]{3,20}\" \"JimH10\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the hyphen comes last in the square brackets and so matches itself, as distinguished from the\n",
    "hyphen in `[a-z]`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercises\n",
    "\n",
    "1. In North American phone numbers, often the area code is written in parentheses.  Extend the pattern above to allow such strings to match.\n",
    "2. Give a regex to match a password that is at least eight characters long."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Beyond automata\n",
    "\n",
    "In the text, we saw that a set of strings matches a regular expression if and only if it is the set of strings\n",
    "accepted by some Finite State machine.\n",
    "In the examples above, that suited us just fine.\n",
    "We added constructs for large alphabets and some other conveniences, but we stuck within the Finite State world.\n",
    "\n",
    "However, in practice we sometimes wish we could do more.\n",
    "For instance, consider matching text inside HTML markup.\n",
    "These regexes pick out the string that is put in italics, and in boldface.\n",
    "\n",
    "Matching any one tag is straightforward."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"&lt;i&gt;Ahoy there!&lt;/i&gt;\" \"Ahoy there!\")</code>"
      ],
      "text/plain": [
       "'(\"<i>Ahoy there!</i>\" \"Ahoy there!\")"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"<i>([^<]*)</i>\" \"<i>Ahoy there!</i>\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"&lt;b&gt;Thar she blows!&lt;/b&gt;\" \"Thar she blows!\")</code>"
      ],
      "text/plain": [
       "'(\"<b>Thar she blows!</b>\" \"Thar she blows!\")"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"<b>([^<]*)</b>\" \"<b>Thar she blows!</b>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But for a single expression that matches them all, you would seem\n",
    "to have to do each as a separate case and then combine cases with a pipe.\n",
    "The alternative we might wish for is to \n",
    "have the system match any tag at the start, and any tag at the end, and just grab the middle.\n",
    "But that won't work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"&lt;i&gt;My name is &lt;b&gt;\" \" \" \"b\")</code>"
      ],
      "text/plain": [
       "'(\"<i>My name is <b>\" \" \" \"b\")"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"<[^>*]>([^<])*<([^>])*>\" \"<i>My name is <b>Captain</b> Aubrey</i>.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is, the more sophisticated thing that we might wish for\n",
    "have the system match the tag at the start with something like `<[^>]*>`, and then remember what it found there\n",
    "(whether it was \"b\" or \"i\", or \"a\", or any other HTML tag),\n",
    "to look that exact thing again at the end."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(\"&lt;i&gt;My name is &lt;b&gt;Captain&lt;/b&gt; Aubrey&lt;/i&gt;\" \"i\")</code>"
      ],
      "text/plain": [
       "'(\"<i>My name is <b>Captain</b> Aubrey</i>\" \"i\")"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(regexp-match #px\"<([^>]+)>.*</\\\\1>\" \"<i>My name is <b>Captain</b> Aubrey</i>.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is a *back reference*. It is very convenient. However, it gives extended\n",
    "regular expressions more power than the theoretical regular expressions that we\n",
    "studied earlier.  Because of that, beyond mentioning them, we won't consider them more."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
