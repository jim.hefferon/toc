{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lexical variables\n",
    "\n",
    "Racket’s variables have *lexical scope*.  That means they are visible only to forms within a certain contiguous stretch of program text. \n",
    "\n",
    "The global variables we have seen thus far satisfy this condition trivially because their scope is all program text, which is certainly contiguous.\n",
    "We have also seen some examples of local variables. These were the `lambda` parameters, which get bound each time the procedure is called, and whose scope is that procedure’s body.\n",
    "\n",
    "Below, there is a global `x` and there is also a local `x`, the latter introduced by procedure `add2`. The global `x` is always `9`. \n",
    "In the call to `add2`, the local `x` gets bound to `3`. However, when the procedure calls return, the global `x` continues to be `9`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>9</code>"
      ],
      "text/plain": [
       "9"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x 9)\n",
    "(define add2 (lambda (x) (+ x 2)))\n",
    "(add2 3)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Racket does not get confused by the two uses of `x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define x 9)\n",
    "(define add2 (lambda (x) (+ x 2)))\n",
    "(add2 x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Modify the lexical binding of a variable with the form `set!`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define x 9)\n",
    "(set! x 20)\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above, the `set!` modifies the global binding of `x` from `9` to `20`, because that is the binding of `x` that is visible to `set!`. If the `set!` is inside `add2`’s body, it modifies the local `x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>7</code>"
      ],
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define add2\n",
    "  (lambda (x)\n",
    "    (set! x (+ x 2))\n",
    "    x))\n",
    "\n",
    "(add2 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `set!` here adds 2 to the local variable `x`, and the procedure returns this new value of the local `x`. (In terms of external effect, this is indistinguishable from the previous version of `add2`.) We can call `add2` on the global `x`, as before.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>22</code>"
      ],
      "text/plain": [
       "22"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x 9)\n",
    "(set! x 20)\n",
    "(define add2\n",
    "  (lambda (x)\n",
    "    (set! x (+ x 2))\n",
    "    x))\n",
    "\n",
    "(add2 x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, Racket does not get confused by the two uses of `x`; the `set!` inside `add2` affects only the local variable used by `add2`. Although as part of the call of the procedure `add2` the local variable `x` got its binding from the global `x`, the latter is unaffected by the `set!` to the local `x`.\n",
    "\n",
    "The point of all this discussion is that we used the same identifier for a local variable and a global variable, and Racket needs a rule for which `x` we mean. The rule is: an identifier named `x` refers to the lexically closest variable named `x`. This will *shadow* any outer or global `x`’s. E.g., in `add2`, the parameter `x` shadows the global `x`.\n",
    "\n",
    "A procedure’s body can access and modify variables in its surrounding scope provided the procedure’s parameters don’t shadow them. This can give some interesting programs.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>45</code>"
      ],
      "text/plain": [
       "45"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define counter 42)\n",
    "\n",
    "(define bump-counter\n",
    "  (lambda ()\n",
    "    (set! counter (+ counter 1))\n",
    "    counter))\n",
    "\n",
    "(bump-counter)\n",
    "(bump-counter)\n",
    "(bump-counter)\n",
    "counter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, `bump‑counter` is a zero-argument procedure (called a *thunk*). It introduces no local variables, and thus cannot shadow anything. Each time it is called, it modifies the global variable counter — it increments it by 1 — and returns its current value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## let and let\\*\n",
    "\n",
    "The special form `let` introduces a list of local variables for use within its body."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>105</code>"
      ],
      "text/plain": [
       "105"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define f\n",
    "  (lambda (x)\n",
    "    (let ([y (+ x 2)]\n",
    "          [z (+ x 4)])\n",
    "      (* x y z))))\n",
    "\n",
    "(f 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, `y` and `z` are local variables.\n",
    "Note the square brackets, as in `[y (+ x 2)]`, which make the procedure more readable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(1 2 3)</code>"
      ],
      "text/plain": [
       "'(1 2 3)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x 20)\n",
    "\n",
    "(let ([x 1]\n",
    "      [y 2]\n",
    "      [z 3])\n",
    "  (list x y z))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with the `lambda` form, within the body of the `let`, the local `x` (bound to 1) shadows the global `x` (which is bound to 20).\n",
    "\n",
    "Very important: for the `let` form\n",
    "the local variable initializations — `x` to 1, `y` to 2, and `z` to 3 — are not considered part of the `let` body. Therefore, a reference to `x` in the initialization will refer to the global, not the local `x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>21</code>"
      ],
      "text/plain": [
       "21"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x 20)\n",
    "\n",
    "(let ([x 1]\n",
    "      [y x])\n",
    "  (+ x y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There, where `y` is set to the value of `x`, it gets the value of the global variable, not the value of the local one.\n",
    "\n",
    "Sometimes you want the opposite, you want to have `let`’s list of lexical variables be introduced in sequence, so that the initialization of a later variable occurs in the lexical scope of earlier variables. \n",
    "For that, use the form `let\\*`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>2</code>"
      ],
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x 20)\n",
    "\n",
    "(let* ([x 1]\n",
    "       [y x])\n",
    "  (+ x y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With `let\\*` the `x` in `y`’s initialization refers to the `x` just above. \n",
    "\n",
    "This example is entirely equivalent to — and is in fact intended to be a convenient abbreviation for — the following program with nested lets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(let ([x 1])\n",
    "  (let ([y x])\n",
    "    (+ x y)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One more point: the values bound to lexical variables can be procedures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>3</code>"
      ],
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(let ((cons (lambda (x y) (+ x y))))\n",
    "  (cons 1 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inside this `let` body, the lexical variable `cons` adds its arguments. \n",
    "Outside this body, `cons` continues to create dotted pairs.\n",
    "Racket doesn't get confused."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## letrec\n",
    "\n",
    "For `let\\*`, we set `x` to a value and then set `y` to a value by using `x`.\n",
    "You can have that they reference each other with the form `letrec`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(letrec ([is-even? (lambda (n)\n",
    "                       (or (zero? n)\n",
    "                           (is-odd? (sub1 n))))]\n",
    "         [is-odd? (lambda (n)\n",
    "                      (and (not (zero? n))\n",
    "                           (is-even? (sub1 n))))])\n",
    "    (is-odd? 11))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more, see the introduction to Recursion."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
