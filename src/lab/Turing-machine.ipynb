{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Turing machine simulator\n",
    "\n",
    "The code here simulates a Turing machine.\n",
    "The language is Racket.\n",
    "\n",
    "## Input\n",
    "\n",
    "The machine to be simulated is in a file that this code reads in.\n",
    "That file has one line per instruction.  \n",
    "An instruction is a space-separated list of four elements:\n",
    "\n",
    "1. natural number for current state, \n",
    "2. character for what the head is point to (any characer in the alphabet; cannot be \"L\" or \"R\"), \n",
    "3. character for the action (any character in the alphabet, or \"L\" or \"R\"), \n",
    "4. natural number for the next state.\n",
    "\n",
    "Best is to use digits or lower-case letters for alphabet.\n",
    "\n",
    "## Configurations\n",
    "\n",
    "A *configuration* is a four-tuple list:\n",
    "\n",
    "1. the current state, as a natural number,\n",
    "2. the symbol being read, a character,\n",
    "3. the contents of the tape to the left of the head, as a list of characters,\n",
    "4. the contents of the tape to the right of the head, as a list of characters.\n",
    "\n",
    "\n",
    "## Output\n",
    "\n",
    "After each step, the machine prints out a picture of the tape. \n",
    "The current character (that is, the location of the tape head) is between asterisks.\n",
    "\n",
    "\n",
    "## Racket code\n",
    "\n",
    "These definitions make the code more readable.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define BLANK #\\B)  ;; Easier to read than space\n",
    "(define STROKE #\\1)  ;; \n",
    "(define LEFT #\\L) ;; Move tape pointer left\n",
    "(define RIGHT #\\R) ;; Move tape pointer right"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These routines are for working with configurations, and selecting the parts of one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; A configuration is a list of four things:\n",
    ";;  the current state, as a natural number\n",
    ";;  the symbol being read, a character\n",
    ";;  the contents of the tape to the left of the head, as a list of characters\n",
    ";;  the contents of the tape to the right of the head, as a list of characters\n",
    "(define (make-config state  char left-tape-list right-tape-list)\n",
    "  (list state char left-tape-list right-tape-list))\n",
    "\n",
    "(define (get-current-state config) (first config))\n",
    "(define (get-current-symbol config)\n",
    "  (let ([cs (second config)])  ;; make horizontal whitespace like a B\n",
    "    (if (char-blank? cs)\n",
    "        #\\B\n",
    "        cs)))\n",
    "(define (get-left-tape-list config) (third config))\n",
    "(define (get-right-tape-list config) (fourth config))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to display the configuration, so we can follow the computation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; configuration-> string  Return a string showing the tape\n",
    "(define (configuration->string config)\n",
    "  (let* ([state-number (get-current-state config)]\n",
    "         [state-string (string-append \"q\" (number->string state-number))]\n",
    "         [left-tape (list->string (get-left-tape-list config))]    \n",
    "         [current (string #\\* (get-current-symbol config) #\\*)]  ;; wrap *'s\n",
    "         [right-tape (list->string (get-right-tape-list config))])\n",
    "    (string-append state-string \": \" left-tape current right-tape)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Next-state function\n",
    "\n",
    "The *delta* function inputs the first half of an instruction, the current state and current character pair. \n",
    "It looks that up in the list of instructions and returns the second half of the relevant instruction, the next state and next action. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; delta  Find the applicable instruction\n",
    "(define (delta tm current-state tape-symbol)\n",
    "  (define (delta-test inst)\n",
    "    (and (= current-state (first inst))\n",
    "         (equal? tape-symbol (second inst))))\n",
    "  \n",
    "  (let ([inst (findf delta-test tm)])\n",
    "    (if (not inst)\n",
    "        '()\n",
    "        (list (third inst) (fourth inst)))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tape manipulation\n",
    "\n",
    "Turing machines compute in a sequence of steps, which consist of transitions from configuration to configuration.  \n",
    "So we must work with the data structure used to simulate the tape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; tape-right-char  Return the element nearest the head on the right side\n",
    "(define (tape-right-char right-tape-list)\n",
    "  (if (empty? right-tape-list)\n",
    "      BLANK\n",
    "      (car right-tape-list)))\n",
    "\n",
    ";; tape-left-char  Return the element nearest the head on the left\n",
    "(define (tape-left-char left-tape-list)\n",
    "  (tape-right-char (reverse left-tape-list)))\n",
    "\n",
    ";; tape-right-pop  Return the right tape list without char nearest the head\n",
    "(define (tape-right-pop right-tape-list)\n",
    "  (if (empty? right-tape-list)\n",
    "      '()\n",
    "      (cdr right-tape-list)))\n",
    "\n",
    ";; tape-left-pop   Return the left tape list without char nearest the head\n",
    "(define (tape-left-pop left-tape-list)\n",
    "  (reverse (tape-right-pop (reverse left-tape-list))))\n",
    "\n",
    ";; move-left  Respond to Left action\n",
    "(define (move-left config next-state)\n",
    "  (let ([left-tape-list (get-left-tape-list config)]\n",
    "        [prior-current-symbol (get-current-symbol config)]\n",
    "        [right-tape-list (get-right-tape-list config)])\n",
    "    ;; push old tape head symbol onto the right tape list \n",
    "    (make-config next-state\n",
    "                 (tape-left-char left-tape-list)    ;; new current symbol\n",
    "                 (tape-left-pop left-tape-list)       ;; strip symbol off left\n",
    "                 (cons prior-current-symbol right-tape-list)))) \n",
    "\n",
    ";; move-right Respond to Right action\n",
    "(define (move-right config next-state)\n",
    "  (let ([left-tape-list (get-left-tape-list config)]\n",
    "        [prior-current-symbol (get-current-symbol config)]\n",
    "        [right-tape-list (get-right-tape-list config)])\n",
    "    ;; push old head symbol onto the left tape list\n",
    "    (make-config next-state\n",
    "                 (tape-right-char right-tape-list) ;; new current symbol\n",
    "                 (reverse (cons prior-current-symbol (reverse left-tape-list))) \n",
    "                 (tape-right-pop right-tape-list)))) ;; strip symbol off right"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## One step\n",
    "\n",
    "We simulate a Turing machine by iterating single-steps.\n",
    "This code performs one step.\n",
    "It takes a configuration and a Turing machine, and it yields the next configuration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; step  Do one step; from a config and the tm, yield the next config\n",
    ";;   Returns null if machine halts.\n",
    "(define (step config tm)\n",
    "  (let* ([current-state (get-current-state config)]\n",
    "         [left-tape-list (get-left-tape-list config)]\n",
    "         [current-symbol (get-current-symbol config)]\n",
    "         [right-tape-list (get-right-tape-list config)]\n",
    "         [action-next-state (delta tm current-state current-symbol)])\n",
    "    (if (null? action-next-state)\n",
    "        '()                       ;; machine is halted\n",
    "        (let ([action (first action-next-state)]\n",
    "              [next-state (second action-next-state)])\n",
    "          (cond\n",
    "            [(char=? LEFT action) (move-left config next-state)]\n",
    "            [(char=? RIGHT action) (move-right config next-state)]\n",
    "            [else (make-config next-state\n",
    "                               action  ;; not L or R so it is in tape alphabet\n",
    "                               left-tape-list\n",
    "                               right-tape-list)])))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Many steps\n",
    "\n",
    "Run a Turing machine step-by-step until it halts.\n",
    "(Notice the `execute-helper` routine nested inside `execute`.\n",
    "Notice also the optional argument `verbose` that has the default value `#t`.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; execute  Run a Turing machine step-by-step until it halts\n",
    "(define (execute tm initial-config [verbose #t])\n",
    "  ;; execute-helper\n",
    "  ;;   config  4-tuple configuration or '()\n",
    "  ;;   stp  integer, step number\n",
    "  ;;   history  list of 4-tuple configurations\n",
    "  (define (execute-helper config stp history)\n",
    "    (if (or (null? config)\n",
    "            (= (get-current-state config) HALT-STATE))\n",
    "        (begin\n",
    "          (when verbose\n",
    "              (fprintf (current-output-port)\n",
    "                       \"step ~s: HALT\\n\"\n",
    "                       stp))\n",
    "          (reverse history))\n",
    "        (begin\n",
    "          (when verbose\n",
    "              (fprintf (current-output-port)\n",
    "                       \"step ~s: ~a\\n\"\n",
    "                       stp\n",
    "                       (configuration->string config)))\n",
    "          (let ([next-config (step config tm)])\n",
    "            (execute-helper next-config (add1 stp) (cons next-config history))))))\n",
    "  \n",
    "  (execute-helper initial-config 0 (list initial-config)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This routine is quite similar, but it runs only for a specified number of steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; Execute for a limited number of states\n",
    "(define (execute-guarded tm initial-config slmt [verbose #t])\n",
    "  (define (execute-helper config stp history)\n",
    "    (cond [(> stp slmt)\n",
    "           (begin\n",
    "             (when verbose\n",
    "               (fprintf (current-output-port)\n",
    "                    \"step ~s: Simulation step limit reached\\n\"\n",
    "                    stp))\n",
    "             (reverse history))]\n",
    "          [(or (null? config)\n",
    "               (= (get-current-state config) HALT-STATE))\n",
    "           (begin\n",
    "             (when verbose\n",
    "               (fprintf (current-output-port)\n",
    "                    \"step ~s: HALT\\n\"\n",
    "                    stp))\n",
    "             (reverse history))]\n",
    "          [else (begin\n",
    "                  (when verbose\n",
    "                    (fprintf (current-output-port)\n",
    "                           \"step ~s: ~a\\n\"\n",
    "                           stp\n",
    "                           (configuration->string config)))\n",
    "                  (let ([next-config (step config tm)])\n",
    "                    (execute-helper next-config (add1 stp) (cons next-config history))))]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read a machine \n",
    "\n",
    "This will read a Turing machine from a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define (current-state-string->number s)\n",
    "  (if (eq? #\\( (string-ref s 0))   ;; allow instr to start with (\n",
    "      (string->number (substring s 1))\n",
    "      (string->number s)))\n",
    "(define (current-symbol-string->char s)\n",
    "  (string-ref s 0))\n",
    "(define (action-symbol-string->char s)\n",
    "  (string-ref s 0))\n",
    "(define (next-state-string->number s)\n",
    "  (if (eq? #\\) (string-ref s (- (string-length s) 1))) ;; ends with )?\n",
    "      (string->number (substring s 0 (- (string-length s) 1)))\n",
    "      (string->number s)))\n",
    "(define (string->instruction s)\n",
    "  (let* ([instruction (string-split (string-trim s))]\n",
    "         [current-state (current-state-string->number (first instruction))]\n",
    "         [current-symbol (current-symbol-string->char (second instruction))]\n",
    "         [action (action-symbol-string->char (third instruction))]\n",
    "         [next-state (next-state-string->number (fourth instruction))])\n",
    "    (list current-state\n",
    "          current-symbol\n",
    "          action\n",
    "          next-state)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
