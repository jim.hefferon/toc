{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Recursion\n",
    "\n",
    "A procedure body can contain calls to other procedures, not least itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define factorial\n",
    "  (lambda (n)\n",
    "    (if (= n 0) 1\n",
    "        (* n (factorial (- n 1))))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This procedure calculates the factorial of a number. If the number is 0, the answer is 1. For any other number n, the procedure uses itself to calculate the factorial of n‑1, multiplies that subresult by n, and returns the product.  It is *recursive* because the procedure name recurs in its own definition.\n",
    "\n",
    "You can also have mutually recursive procedures. The following predicates for evenness and oddness use each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define is-even?\n",
    "  (lambda (n)\n",
    "    (if (= n 0) #t\n",
    "        (is-odd? (- n 1)))))\n",
    "\n",
    "(define is-odd?\n",
    "  (lambda (n)\n",
    "    (if (= n 0) #f\n",
    "        (is-even? (- n 1)))))\n",
    "\n",
    "(is-even? 13)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(These are here only as simple illustrations; Racket already provides `even?` and `odd?`.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## letrec\n",
    "\n",
    "If we wanted the above procedures as local variables, we could try to use a `let` form.\n",
    "\n",
    "```\n",
    "(let ((local-even? (lambda (n)\n",
    "                     (if (= n 0) #t\n",
    "                         (local-odd? (- n 1)))))\n",
    "      (local-odd? (lambda (n)\n",
    "                    (if (= n 0) #f\n",
    "                        (local-even? (- n 1))))))\n",
    "  (list (local-even? 23) (local-odd? 23)))\n",
    "```\n",
    "  \n",
    "This won’t quite work because the occurrences of `local‑even?` and `local‑odd?` in the initializations don’t refer to the lexical variables themselves. Changing the `let` to a `let*` won’t work either, for while the `local‑even?` inside `local‑odd?`’s body refers to the correct procedure value, the `local‑odd?` in `local‑even?`’s body still points elsewhere.\n",
    "\n",
    "To solve problems like this, Scheme provides the form `letrec` (which was briefly introduced in Lexical Variables)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(#f #t)</code>"
      ],
      "text/plain": [
       "'(#f #t)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(letrec ((local-even? (lambda (n)\n",
    "                        (if (= n 0) #t\n",
    "                            (local-odd? (- n 1)))))\n",
    "         (local-odd? (lambda (n)\n",
    "                       (if (= n 0) #f\n",
    "                           (local-even? (- n 1))))))\n",
    "  (list (local-even? 23) (local-odd? 23)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The lexical variables introduced by a `letrec` are visible not only in the `letrec`-body but also within all the initializations. Thus, `letrec` is tailor-made for defining recursive and mutually recursive local procedures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tail call elimination\n",
    "\n",
    "Recursion is an elegant way to express solutions, but can use a lot of computing resources.\n",
    "Consider the `factorial` function from above.\n",
    "\n",
    "```\n",
    "(define factorial\n",
    "  (lambda (n)\n",
    "    (if (= n 0) 1\n",
    "        (* n (factorial (- n 1))))))\n",
    "```\n",
    "\n",
    "To find `(factorial 5)` Racket will take the \"if\" statement's \"else\" branch, leading to `(* 5 (factorial 4))`.\n",
    "To proceed it must compute the `(factorial 4)` part, which leads to Racket looking to find `(* 5 (* 4 (factorial 3)))`. \n",
    "This stack of nested waiting multiplications grows until Racket eventually encounters `(factorial 0)`.\n",
    "If the nesting is quite deep, if we want to compute the factorial of a large number, then the stack can \n",
    "use a great deal of space.\n",
    "\n",
    "An important innovation of Scheme, an ancestor language of Racket, is that the computer can \n",
    "recognize certain recursions that can be done in constant space, that are *iterative*.\n",
    "\n",
    "Below, when the recursive call occurs in `fact-iter`’s body, it is the *tail call*, or the very last thing done. Each invocation of `fact-iter` either does not call itself, or when it does, it does so as its very last act."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>120</code>"
      ],
      "text/plain": [
       "120"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define (factorial n)\n",
    "  (fact-iter n 1))\n",
    "\n",
    "(define (fact-iter n accumulator)\n",
    "  (if (= n 0)\n",
    "      accumulator\n",
    "      (fact-iter (- n 1) (* n accumulator))))\n",
    "\n",
    "(factorial 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Racket recognizes the tail call and computes `(fact-iter 5 1)` not by making a stack but instead by relacing it with a computation of `(fact-iter 4 5)`.  That then gets replaced by `(fact-iter 3 20)`, which in turn is \n",
    "replaced by `(fact-iter 2 60)`, and `(fact-iter 1 120)`, and finally the first branch of \"if\" gives a result of 120.\n",
    "\n",
    "By writing procedures to take advantage of tail-call elimination, you get the expressiveness of recursion with the space-savings of looping."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mapping a procedure across a list\n",
    "\n",
    "A special kind of iteration involves repeating the same action for each element of a list. \n",
    "Racket offers two procedures for this situation: `map` and `for‑each`.\n",
    "\n",
    "The `map` procedure applies a given procedure to every element of a given list, and returns the list of the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(3 4 5)</code>"
      ],
      "text/plain": [
       "'(3 4 5)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define (add2 x) (+ x 2)) \n",
    "(map add2 '(1 2 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `for‑each` procedure also applies a procedure to each element in a list, but returns a null value. \n",
    "It is purely for any side-effects it causes. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(for-each display\n",
    "  (list \"one \"\" \"two \"\" \"buckle my shoe\"\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "has the side-effect of displaying the strings (in the order they appear) on the console.\n",
    "\n",
    "The procedures applied by `map` and `for‑each` need not be one-argument procedures. For example, given an n-argument procedure, `map` takes n lists and applies the procedure to every set of n of arguments selected from across the lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(map cons '(1 2 3) '(10 20 30))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(map + '(1 2 3) '(10 20 30))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
