{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Turing machine encoding\n",
    "\n",
    "Here we give a way to count the Turing machines.\n",
    "Under this function, $\\text{encode-TM}\\colon \\mathbb{N}\\to T$, for every natural number there is an associated Turing machine\n",
    "and for every Turing machine there is a natural number.\n",
    "Because we will describe the function with Racket code, these associations are obviously computable.\n",
    "That is, we are giving here an *acceptable numbering* of the Turing machines.\n",
    "\n",
    "We start by bringing in some definitions and functions from the Turing machine model.\n",
    "In particular, `#\\L` stands for the next action of moving the tape left, and \n",
    "`#\\R` for moving it right."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(require \"../prologue/turing-machine.rkt\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we define some constants, so when we use them the intent is clear.\n",
    "The basic idea is: a Turing machine is a set of instructions, which are 4-tuples. To encode it, we fix some ordering and encode the instructions, separated by INTER-INSTRUCTION-SEPARATOR.\n",
    "Inside each instruction, we encode the four items separated by SEPARATOR."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define SEPARATOR BLANK)            ; inside of an instruction's representation\n",
    "(define INTER-INSTRUCTION-SEPARATOR (string BLANK BLANK))  ; between instructions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To encode the instruction items, we use unary.\n",
    "Thus, the state $q_5$ will be represented by `11111`.\n",
    "(An encoding that is in some ways simpler is to represent $q_i$ with $i+1$-many strokes.\n",
    "That differs from the book's presentation so we don't do it here, but see the Racket file for\n",
    "a full implementation.)\n",
    "\n",
    "The tape symbols take a bit more care.\n",
    "Our encoding depends on the *code point* of the character.\n",
    "Every possible characters has a slot in the Unicode table, its code point.\n",
    "For instance, the code point of the lower case a character, `#\\a`, is $97$.\n",
    "We will only ever use symbols that appear in the ASCII table, which is a subset of Unicode.\n",
    "\n",
    "To help readability of the output, the encoding here does not just take the character's code point,\n",
    "its ASCII number.\n",
    "Instead, the encoding subtracts some constant.\n",
    "Some machines we may write use zero or the digits or the upper case of lower case letters, and \n",
    "so we pick a number below the code point of all of those.\n",
    "Specifically, the constant is the ASCII encoding of the character the comes before \"0\", which is a slash."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; Characters in the alphabet are represented with a unary string.  If we used their\n",
    ";; Unicode char point, that would be very hard to read.  Instead, we find the difference\n",
    ";; between character point of some lowest character and the one we want.  So basically,\n",
    ";; use a characer here that is strictly below anything you will use.\n",
    "(define CHAR-POINT-OF-LOWEST-CHAR\n",
    "  (char->integer #\\/))   ; note that / is one below #\\0, so we can use digits, upper case letters, or lower case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we are asked to decode an integer that doesn't make sense -- for instance if it contains an instruction encoding that does not include any `SEPARATOR`'s and so it doesn't break into four parts -- then we must return some default machine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define EMPTY-TURING_MACHINE '())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the following utility function.\n",
    "It takes a list and finds the `and` of the elements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; and-of-list  apply and to the list v\n",
    "(define (and-of-list v)\n",
    "  (if (null? v)\n",
    "      #t\n",
    "      (and (car v)\n",
    "           (and-of-list (cdr v)))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The parts of an instruction\n",
    "\n",
    "Next we give routines to encode and decode the four items in an instruction.\n",
    "\n",
    "First, rather than work directly with numbers, we will work mostly with strings.\n",
    "The are more convienent than natural numbers; for instance, finding the third entry in a string is easier than \n",
    "finding the third digit in a number.\n",
    "Thus the encoding routines take in either a natural number present state or next state, or a character for present input or next action, and output a string.\n",
    "The decoding routines take in a string and output something of the relevant type.\n",
    "\n",
    "Both types of routines use *unary strings*, strings from the alphabet $\\Sigma=\\{1,B\\}$.\n",
    "(We can interpret this\n",
    "as the binary representation of a number.\n",
    "To avoid the question of leading blanks being interprete as zeros, which are then not significant and so \n",
    "there is ambiguity about the number for the machine, we could prefix the string with `1` before the\n",
    "interpretation.)\n",
    "\n",
    "The `encode-xx` routines are more straightforward, because they are sure to be given known input.\n",
    "The `decode-xx` routines must fail if the input is not sensible (for instance, if they must decode to \n",
    "a natural number but the string input just doesn't decode to that), so they do a little input error checking.\n",
    "For a parse failure, the decode routines return `#f`.\n",
    "\n",
    "First comes some routines to work with unary strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; unary-string?  Test whether a string is the unary representation of a number\n",
    "(define (unary-string? s)\n",
    "  (if (not (string? s))\n",
    "      #f\n",
    "      (let ([lst (string->list s)])\n",
    "        (and-of-list (map\n",
    "                      (lambda (x) (eqv? x STROKE))\n",
    "                      lst)))))\n",
    "\n",
    "\n",
    ";; natural->unary-string  Convert a natural number to unary encoded string\n",
    ";;  n  Natural number (n=0 is OK)\n",
    "(define (natural->unary-string n)\n",
    "  (make-string n STROKE))\n",
    "\n",
    ";; unary-string->natural  Convert a unary string to the natural number it represents \n",
    "(define (unary-string->natural s)\n",
    "  (string-length s))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing parts of a machine\n",
    "\n",
    "We first have some predicates that test whether their input matches the parts of a machine.\n",
    "Note that the present symbol cannot be `L` or `R`.  \n",
    "\n",
    "Note also that the test for whether input is in the form of a Turing machine, `TM?`, has an\n",
    "optional argument `verbose` with a default value of `#f`.\n",
    "This is convenient for debugging.\n",
    "(The `when` conditional is Racket's way of having an `if` with no `then`.)\n",
    "\n",
    "In the source file, several routines have this crude debugging code, but we will omit it here because\n",
    "the `display` lines are a distraction.\n",
    "We are showing it this once, to make clear what we are not showing in other places."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; state?  Test whether the argument is a possible state\n",
    ";;  Note that it does not check membership in the actual set of states for this machine\n",
    "(define (state? v)\n",
    "  (exact-nonnegative-integer? v))\n",
    "\n",
    ";; present-symbol?  Test whether the argument is a possible present symbol\n",
    ";;  Note that it does not check whether the argument is in the alphabet, but\n",
    ";;  it does reject L or R\n",
    "(define (present-symbol? v)  \n",
    "  (if (not (char? v))\n",
    "      #f\n",
    "      (and (not (eqv? v LEFT))\n",
    "           (not (eqv? v RIGHT)))))\n",
    "\n",
    ";; next-action?  Test whether the argument is a possible next-action (a char)\n",
    "(define (next-action? v)\n",
    "  (char? v))\n",
    "\n",
    ";; instruction? Test whether the argument is a possible instruction, a four-tuple\n",
    "(define (instruction? v)\n",
    "  (and (list? v)\n",
    "       (= 4 (length v))\n",
    "       (state? (first v))\n",
    "       (present-symbol? (second v))\n",
    "       (next-action? (third v))\n",
    "       (state? (fourth v))))\n",
    "\n",
    ";; TM?  Test whether the argument is a Turing machine, a list of instructions\n",
    "(define (TM? v [verbose #f])\n",
    "  (if (not (list? v))\n",
    "      #f\n",
    "      (let ([instruction-results (map instruction? v)])\n",
    "        (when verbose  ; debugging\n",
    "            (begin\n",
    "              (display \"TM? instruction first first?\") (display (first (first v))) (display (state? (first (first v)))) (newline)\n",
    "              (display \"TM? instruction first second?\") (display (second (first v))) (display (present-symbol? (second (first v)))) (newline)\n",
    "              (display \"TM? instruction first third?\") (display (third (first v))) (display (next-action? (third (first v)))) (newline)\n",
    "              (display \"TM? instruction first fourth?\") (display (fourth (first v))) (display (state? (fourth (first v)))) (newline)\n",
    "              (display \"TM? net: instruction first?\") (display (instruction? (first v))) (newline)\n",
    "              (display \"TM? net: instruction-results\") (display instruction-results) (newline) \n",
    "              (display \"TM? result \") (display (and-of-list instruction-results)) (newline)))\n",
    "        (and-of-list instruction-results))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Present state or next state\n",
    "\n",
    "To encode or decode, we just need to convert between natural numbers and string representations. \n",
    "(Note that Racket's `string->number` routine returns `#f` if the string doesn't convert.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-present-state  input natural number, output encoding as string\n",
    "(define (encode-present-state q)\n",
    "  (natural->unary-string q))\n",
    "\n",
    ";; decode-present-state input a unary-string, output natural number\n",
    ";;  (if string is not suitable, output #f)\n",
    "(define (decode-present-state s)\n",
    "  (if (not (unary-string? s))\n",
    "      #f\n",
    "      (unary-string->natural s)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next state routines work the same way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-next-state  input positive integer, output encoding as string\n",
    "(define (encode-next-state q)\n",
    "  (natural->unary-string q))\n",
    "\n",
    ";; decode-next-state input a string, output natural number\n",
    ";;  (if string is not suitable, output #f)\n",
    "(define (decode-next-state s)\n",
    "  (if (not (unary-string? s))\n",
    "      #f\n",
    "      (unary-string->natural s)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As described above, to make the instructions easier to read we represent characters using smaller numbers.\n",
    "Specifically, we take the code point and subtract a constant.\n",
    "This is strictly a matter of convenience of reading."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-present-symbol  input a present-symbol, output encoding as unary-string\n",
    "(define (encode-present-symbol ch)\n",
    "  (natural->unary-string (- (char->integer ch)\n",
    "                            CHAR-POINT-OF-LOWEST-CHAR)))\n",
    "\n",
    ";; decode-present-symbol input a unary-string, output a present symbol\n",
    ";;  (if string is not suitable, output #f)\n",
    "(define (decode-present-symbol s)\n",
    "  (if (not (unary-string? s))\n",
    "      #f\n",
    "      (let* ([char-point (+ (unary-string->natural s)\n",
    "                            CHAR-POINT-OF-LOWEST-CHAR)]\n",
    "             [ch (integer->char char-point)])\n",
    "        (if (not (present-symbol? ch))\n",
    "            #f\n",
    "            ch))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `encode-next-action` and `decode-next-action` routines are similar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-next-action  Input a character, output encoding as unary string\n",
    "(define (encode-next-action s)\n",
    "  (natural->unary-string (- (char->integer s)\n",
    "                            CHAR-POINT-OF-LOWEST-CHAR)))\n",
    "\n",
    ";; decode-next-action  input a unary string, output next-action\n",
    ";;   If input string not suitable, return #f\n",
    "(define (decode-next-action s)\n",
    "  (if (not (unary-string? s))\n",
    "      #f\n",
    "      (let* ([char-point (+ (unary-string->natural s)\n",
    "                            CHAR-POINT-OF-LOWEST-CHAR)]\n",
    "             [ch (integer->char char-point)])\n",
    "        (if (not (next-action? ch))\n",
    "            #f\n",
    "            ch))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instructions\n",
    "\n",
    "Next we encode and decode entire 4-tuple instructions.\n",
    "\n",
    "The `encode-TM-instruction` routine takes in a list of 4 items, \n",
    "$(\\text{present-state}\\quad \\text{present-character}\\quad\\text{next-action}\\quad\\text{next-state})$.\n",
    "It uses the routines above to encode these items. \n",
    "It then makes a string by separating them with the string consisting only of the\n",
    "single character `B` (called `SEPARATOR`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-TM-instruction  Input instruction, output encoding as string\n",
    "(define (encode-TM-instruction inst)\n",
    "  (let([present-state (first inst)]\n",
    "       [present-symbol (second inst)]\n",
    "       [next-action (third inst)]\n",
    "       [next-state (fourth inst)]\n",
    "       [separator-string (string SEPARATOR)])\n",
    "    (string-append (encode-present-state present-state)\n",
    "                   separator-string (encode-present-symbol present-symbol)\n",
    "                   separator-string (encode-next-action next-action)\n",
    "                   separator-string (encode-next-state next-state))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual the decoding routine is a little more involved than encoding.\n",
    "For one thing, it uses a regular expression.\n",
    "We will cover these in a later lab, but basically it pulls the input string apart.\n",
    "(It is here that the other encoding mentioned above is easier.)\n",
    "\n",
    "Note also that `decode-TM-instruction` has an optional argument of `verbose`; we have omitted \n",
    "the relevant code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; INSTRUCTION-REGEX  String that will be used to pull apart the encoded instructions\n",
    "(define INSTRUCTION-REGEX (string-append \"(\" INTER-INSTRUCTION-SEPARATOR \")?\"\n",
    "                                         \"(([^\" (string SEPARATOR) \"]*)\"\n",
    "                                         (string SEPARATOR) \"([^\" (string SEPARATOR) \"]*)\"\n",
    "                                         (string SEPARATOR) \"([^\" (string SEPARATOR) \"]*)\"\n",
    "                                         (string SEPARATOR) \"([^\" (string SEPARATOR) \"]*))\"\n",
    "                                         \"(.*)\"))\n",
    "\n",
    ";; decode-TM-instruction  input string encoding instruction, return instruction\n",
    ";;   Returns #f if the parsing does not work.\n",
    "(define (decode-TM-instruction instruction-str [verbose #f])  \n",
    "  (let ([m (regexp-match (pregexp INSTRUCTION-REGEX) instruction-str)])\n",
    "    (if (not m)\n",
    "        #f\n",
    "        (let ([inter-instruction-separator (second m)]\n",
    "              [new-encoded-instruction (third m)]\n",
    "              [new-encoded-present-state (fourth m)]  ; part of new-encoded-instruction\n",
    "              [new-encoded-present-symbol (fifth m)]\n",
    "              [new-encoded-next-action (sixth m)]\n",
    "              [new-encoded-next-state (seventh m)]\n",
    "              [new-str (eighth m)])\n",
    "          (let ([instruction (list (decode-present-state new-encoded-present-state)\n",
    "                                   (decode-present-symbol new-encoded-present-symbol)\n",
    "                                   (decode-next-action new-encoded-next-action)\n",
    "                                   (decode-next-state new-encoded-next-state))])\n",
    "            (if (not (and-of-list instruction))\n",
    "                #f\n",
    "                instruction))))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Whole machines\n",
    "\n",
    "Most of the work is done.\n",
    "The routine `encode-TM` just takes the strings generated from encoding the instructions, \n",
    "concatenates them, with a suitable separator, and returns the resulting string. \n",
    "\n",
    "The routine `decode-TM` inputs a string, breaks it into instructions, breaks each instruction into\n",
    "four parts, and decodes all the parts.\n",
    "If the input string doesn't divide up correctly, for instance as happens with the input $`s`=0$, then this routine returns the default `EMPTY-TURING_MACHINE`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ";; encode-TM  input list of instructions, return string encoding\n",
    "(define (encode-TM tm)\n",
    "  (if (null? tm)\n",
    "      (natural->unary-string 0)\n",
    "      (string-join (map encode-TM-instruction tm) INTER-INSTRUCTION-SEPARATOR)))\n",
    "\n",
    "\n",
    ";; decode-TM  input string, representing binary number, output a machine\n",
    "(define (decode-TM TM-str [verbose #f])\n",
    "  (define (decode-TM-helper str instr-list)\n",
    "    (if (= 0 (string-length str))\n",
    "        instr-list\n",
    "        (let ([m (regexp-match (pregexp INSTRUCTION-REGEX) str)])\n",
    "          (if (not m)\n",
    "              #f      ; failed to parse\n",
    "              (let ([inter-instruction-separator (second m)]\n",
    "                    [new-encoded-instruction (third m)]\n",
    "                    [new-encoded-present-state (fourth m)] \n",
    "                    [new-encoded-present-symbol (fifth m)]\n",
    "                    [new-encoded-next-action (sixth m)]\n",
    "                    [new-encoded-next-state (seventh m)]\n",
    "                    [new-str (eighth m)])\n",
    "                (decode-TM-helper new-str (cons (decode-TM-instruction new-encoded-instruction) instr-list)))))\n",
    "        ))\n",
    "  \n",
    "  (if (not (string? TM-str))\n",
    "      #f\n",
    "      (let ([TM (decode-TM-helper TM-str '())])\n",
    "        (if (not (and-of-list TM))\n",
    "            #f\n",
    "            (reverse TM)))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `decode-TM` routine relies on the tail-recursive `decode-TM-helper`, which is defined inside of it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "Use the cell below to do these exercises\n",
    "\n",
    "1. What number encodes the symbol lower case a in the above routine?\n",
    "\n",
    "2. What is the lowest-numbered non-trivial Turing machine?\n",
    "\n",
    "3. Find an index of the successor function.\n",
    "\n",
    "4. Recall that every computable function has infinitely many indices.  Give a different index for the successor function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
