{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Forms\n",
    "\n",
    "All of the Racket example programs provided thus far are also s-expressions. This is true of all Racket programs: Programs are data.\n",
    "\n",
    "Thus, the character datum `#\\c` is a program, or a *form*. We will use the more general term form instead of program, so that we can deal with program fragments too.\n",
    "\n",
    "Racket evaluates the form `#\\c` to the value `#\\c`; that's what we mean when we say `#\\c` is self-evaluating. But not all s-expressions are self-evaluating. For instance the symbol s-expression `xyz` evaluates to the value held by the variable `xyz`. The list s-expression `(string‑>number \"16\")` evaluates to the number 16.\n",
    "\n",
    "Also, not all s-expressions are valid programs. If you typed the dotted-pair s-expression `(1 . 2)` at the Racket listener then you will get an error.\n",
    "\n",
    "The basic way that Racket evaluates a list form is by examining the first element, or head, of the form. If the head evaluates to a procedure then the rest of the form is evaluated to get the procedure’s arguments, and the procedure is applied to the arguments. The exception is if the head of the form is a *special form*, then the evaluation proceeds in a manner idiosyncratic to that form. Some special forms we have already seen are `begin`, `define`, and `set!`. The special form `begin` causes its subforms to be evaluated in order, the result of the entire form being the result of the last subform. And, `define` introduces and initializes a variable, while `set!` changes the binding of a variable.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Procedures\n",
    "\n",
    "We have seen quite a few primitive Scheme procedures, e.g., `cons`, `string‑>list`, and the like. Users can create their own procedures using the special form `lambda`. For example, the following defines a procedure that adds 2 to its argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#&lt;procedure&gt;</code>"
      ],
      "text/plain": [
       "#<procedure>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(lambda (x) (+ x 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first subform, `(x)`, is the list of parameters. The remaining subform(s) constitute the procedure’s body. This procedure can be called on an argument, just like a primitive procedure. \n",
    "Here we call it on argument 5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "((lambda (x) (+ x 2)) 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we wanted to call this same procedure many times, we could create a replica using lambda each time. A more practical approach is to name it, to use a variable to hold the procedure value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define add2\n",
    "  (lambda (x) (+ x 2)))\n",
    "\n",
    "(add2 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Procedure parameters\n",
    "\n",
    "The parameters of a lambda-procedure are specified by its first subform (the form immediately following the head, the symbol lambda). For instance, `add2` is a single-argument — or unary — procedure, and so its parameter list is the singleton list `(x)`. The symbol `x` acts as a variable holding the procedure’s argument. Each occurrence of `x` in the procedure’s body refers to the procedure’s argument. The variable `x` is said to be *local* to the procedure’s body.\n",
    "\n",
    "We can use 2-element lists for 2-argument procedures, and in general, n-element lists for n-argument procedures. The following is a 2-argument procedure that calculates the area of a rectangle. Its two arguments are the length and breadth of the rectangle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define area\n",
    "  (lambda (length breadth)\n",
    "    (* length breadth)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Notice that area multiplies its arguments, and so does the primitive procedure \\*. We could have simply said:\n",
    "`(define area \\*)`.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variable number of arguments\n",
    "\n",
    "Some procedures can be called at different times with different numbers of arguments. For instance, we can call `(+ 1 2)` or `(+ 1 2 3)`.\n",
    "\n",
    "To do this, we replace the lambda parameter list with a single symbol. This symbol acts as a variable that is bound to the list of the arguments.\n",
    "\n",
    "In general, the lambda parameter list can be a list of the form `(x ...)`, a symbol, or a dotted pair of the form `(x ... . z)`. In the dotted-pair case, all the variables before the dot are bound to the corresponding arguments in the procedure call, with the single variable after the dot picking up all the remaining arguments as one list."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## apply\n",
    "\n",
    "The Scheme procedure `apply` lets us call a procedure on a list of its arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define x '(1 2 3))\n",
    "\n",
    "(apply + x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, `apply` takes a procedure, followed by a variable number of other arguments, the last of which must be a list. It constructs the argument list by prefixing the last argument with all the other (intervening) arguments. It then returns the result of calling the procedure on this argument list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>21</code>"
      ],
      "text/plain": [
       "21"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x '(1 2 3))\n",
    "\n",
    "(apply + 4 5 6 x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sequencing\n",
    "\n",
    "We used the `begin` special form to bunch together a group of subforms that need to be evaluated in sequence. Many Scheme forms have implicit `begin`s. For example, here is a 3-argument procedure that displays its three arguments, with spaces between them. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define display3\n",
    "  (lambda (arg1 arg2 arg3)\n",
    "    (begin\n",
    "      (display arg1)\n",
    "      (display \" \")\n",
    "      (display arg2)\n",
    "      (display \" \")\n",
    "      (display arg3)\n",
    "      (newline))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Racket, lambda-bodies are implicit begins. Thus, the `begin` in `display3`’s body isn’t needed, although it doesn’t hurt. This definition does the same and is simpler."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define display3\n",
    "  (lambda (arg1 arg2 arg3)\n",
    "    (display arg1)\n",
    "    (display \" \"\")\n",
    "    (display arg2)\n",
    "    (display \" \"\")\n",
    "    (display arg3)\n",
    "    (newline)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
