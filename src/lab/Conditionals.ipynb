{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conditionals\n",
    "\n",
    "The most basic conditional is the form `if`.\n",
    "\n",
    "```\n",
    "(if test-expression\n",
    "    then-branch\n",
    "    else-branch)\n",
    "```\n",
    "\n",
    "If `test‑expression` evaluates to true (i.e., any value other than `#f`), the “then” branch is evaluated. If not, the “else” branch is evaluated. The “else” branch is optional.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'OK</code>"
      ],
      "text/plain": [
       "'OK"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define t 98.6)\n",
    "\n",
    "(if (> t 100) \n",
    "    'unwell\n",
    "    'OK)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(if (< t 100)\n",
    "    'unwell)    ;no “else” branch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## when and unless\n",
    "\n",
    "The forms `when` and `unless` are convenient conditionals to use when you need only one branch (the “then” or the “else” branch) of the basic conditional.\n",
    "\n",
    "```\n",
    "(when (< (pressure tube) 60)\n",
    "   (open-valve tube)\n",
    "   (attach floor-pump tube)\n",
    "   (depress floor-pump 5)\n",
    "   (detach floor-pump tube)\n",
    "   (close-valve tube))\n",
    "```\n",
    "\n",
    "Assuming pressure of tube is less than 60, this conditional will attach floor‑pump to tube and depress it 5 times. (`attach` and `depress` are some suitable procedures.)\n",
    "\n",
    "The same program using `if` would be:\n",
    "\n",
    "```\n",
    "(if (< (pressure tube) 60)\n",
    "    (begin\n",
    "      (open-valve tube)\n",
    "      (attach floor-pump tube)\n",
    "      (depress floor-pump 5)\n",
    "      (detach floor-pump tube)\n",
    "      (close-valve tube)))\n",
    "```\n",
    "\n",
    "Note that `when`’s branch is an implicit `begin`, whereas `if` requires an explicit `begin` if either of its branches has more than one form.\n",
    "\n",
    "The same behavior can be written using `unless`.\n",
    "\n",
    "```\n",
    "(unless (>= (pressure tube) 60)\n",
    "   (open-valve tube)\n",
    "   (attach floor-pump tube)\n",
    "   (depress floor-pump 5)\n",
    "   (detach floor-pump tube)\n",
    "   (close-valve tube))\n",
    "```\n",
    "   \n",
    "The `unless` form is equivalent to `when not`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## cond\n",
    "\n",
    "A very common thing in programs is a sequence of nested `if` statements, as here.  \n",
    "\n",
    "```\n",
    "(if (char<? c #\\c) -1\n",
    "    (if (char=? c #\\c) 0\n",
    "        1))\n",
    "```        \n",
    "\n",
    "Racket provides the more readable `cond` form as a multi-branch conditional.  The `cond` is thus a multi-branch conditional. Each clause has a *test* and an associated *action*. The first test that succeeds triggers its associated action. \n",
    "\n",
    "```\n",
    "(cond ((char<? c #\\c) -1)\n",
    "      ((char=? c #\\c) 0)\n",
    "      (else 1))\n",
    "```      \n",
    "      \n",
    "The final `else` clause is chosen if no other test succeeded.\n",
    "\n",
    "The `cond` actions are implicit begins."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## case\n",
    "\n",
    "A kind of multi-branch conditional that occurs often is where every test is a membership test.\n",
    "\n",
    "```\n",
    "(cond ((equal? ch #\\a) 1)\n",
    "       (equal? ch #\\b) 2)\n",
    "       (equal? ch #\\c) 3)\n",
    "       (else 4))\n",
    "```\n",
    "\n",
    "You can compressed this into a `case` expression.\n",
    "\n",
    "```\n",
    "(case c\n",
    "  ((#\\a) 1)\n",
    "  ((#\\b) 2)\n",
    "  ((#\\c) 3)\n",
    "  (else 4))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## and and or\n",
    "\n",
    "Racket provides special forms for boolean conjunction (“and”) and disjunction (“or”). (We have already seen the  boolean negation `not`, which is a procedure.) The special form `and` returns a true value if all its subforms are true (that is, none is `#f`). The actual value returned is the value of the final subform."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>#f</code>"
      ],
      "text/plain": [
       "#f"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x #t)\n",
    "(define y #f)\n",
    "\n",
    "(and x y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>7</code>"
      ],
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define x #t)\n",
    "(define y 7)\n",
    "\n",
    "(and x y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The special form `or` returns the value of its first true subform. \n",
    "If all the subforms are false then `or` returns `#f`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define x #f)\n",
    "(define y #f)\n",
    "\n",
    "(or x y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define x #f)\n",
    "(define y 7)\n",
    "\n",
    "(and x y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both `and` and `or` evaluate their subforms left-to-right. As soon as the result can be determined, `and` and `or` will ignore the remaining subforms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(and 1 #f expression-guaranteed-to-cause-error)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
